import React from "react";
// import Modal from "react-responsive-modal";

class Api extends React.Component {
  state = {
    open: false
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div>
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>API Keys</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-8">
            <div className="widget-box">
              <div className="widget-header widget-header-flat">
                <h4 className="smaller">
                  <i className="ace-icon fa fa-lock"></i>
                  {"  "}
                  Key
                </h4>
              </div>

              <div className="widget-body ">
                <div className="widget-main ">
                  <div
                    className="widget-header "
                    style={{
                      paddingBottom: "15px",
                      marginBottom: "15px",
                      color: "#585858",
                      fontSize: "12px"
                    }}
                  >
                    <div className="col-xs-8">
                      <i className="ace-icon fa fa-lightbulb-o"></i>
                      {"   "}
                      API Keys are used for authentication to Chargebee API
                      every time an API request is made. Like passwords, API
                      keys should remain a secret.{" "}
                      <a href="#">
                        <i className="ace-icon fa fa-external-link"></i> Learn
                        More...
                      </a>{" "}
                    </div>
                    <div
                      className="pull-right"
                      style={{ marginRight: "20px", marginTop: "10px" }}
                    >
                      <span
                        className="btn btn-info btn-sm tooltip-info "
                        data-rel="tooltip"
                        data-placement="bottom"
                        title="Request an API Key"
                        data-original-title="Bottm Info"
                        style={{ marginRight: "25px" }}
                      >
                        <i className="ace-icon glyphicon glyphicon-plus"></i>
                        Generate API Key
                      </span>
                    </div>
                  </div>
                  <div
                    className="widget-body"
                    style={{
                      backgroundColor: "#f5f5f5",
                      border: "1px solid #ccc",
                      padding: "10px",
                      borderRadius: "4px"
                    }}
                  >
                    <ul>
                      <li>
                        <div className="" style={{ padding: "4px" }}>
                          <b>Full_Acess_Key 1</b>
                          <i
                            className="fa fa-paste blue "
                            style={{ paddingLeft: "10px", cursor: "pointer" }}
                          ></i>
                          <i
                            className="ace-icon glyphicon glyphicon-remove red pull-right"
                            style={{ paddingLeft: "10px", cursor: "pointer" }}
                          ></i>
                          <span
                            style={{
                              color: "#adadad",
                              display: "block",
                              fontSize: "9px",
                              marginTop: "-2px"
                            }}
                          >
                            Created on Mar 3, 2020, 10:11 AM
                          </span>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Api;
