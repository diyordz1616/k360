import React from "react";
import { Link } from "react-router-dom";

function Sidebar() {
  let isPage = "Dashboard";
  return (
    <div className="App">
      <div id="sidebar" className="sidebar responsive ace-save-state">
        <div className="sidebar-shortcuts" id="sidebar-shortcuts"></div>
        <ul id="sidebar_menu_navigation" className="nav nav-list">
          <li
            id="sidebar_menu_dashboard"
            className="dropdown-toggle {{isPage=='Dashboard'? 'active':null }}"
          >
            <Link to="/dashboard">
              <i className="menu-icon fa fa-tachometer"></i>
              <span className="menu-text"> Dashboard </span>
            </Link>
          </li>

          <li id="sidebar_menu_reports" className="dropdown-toggle">
            <Link to="/reports">
              <i className="menu-icon fa fa-bar-chart-o"></i>
              <span className="menu-text"> Reports </span>
            </Link>
          </li>
          {/* 
          <li id="sidebar_menu_routes" className="dropdown-toggle">
            <Link to="/about">
              <i className="menu-icon fa fa-map-marker"></i>
              <span className="menu-text"> About </span>
            </Link>
          </li> */}

          <li id="sidebar_menu_announcements" className="dropdown-toggle">
            <Link to="/announcements">
              <i className="menu-icon fa fa-bullhorn"></i>
              <span className="menu-text"> Announcements </span>
            </Link>
          </li>

          <li id="sidebar_menu_roles" className="dropdown-toggle">
            <Link to="/roleslist">
              <i className="menu-icon fa fa-child"></i>
              <span className="menu-text"> Roles </span>
            </Link>
          </li>

          <li id="sidebar_menu_groups" className="dropdown-toggle">
            <Link to="/groupslist">
              {" "}
              <i className="menu-icon fa fa-users"></i>{" "}
              <span className="menu-text"> Groups </span>
            </Link>
          </li>

          <li id="sidebar_menu_users" className="dropdown-toggle">
            <a href="/users">
              {" "}
              <i className="menu-icon fa fa-user"></i>{" "}
              <span className="menu-text"> Users </span>
            </a>
          </li>

          <li id="sidebar_menu_brands" className="dropdown-toggle">
            <Link to="/brandslist">
              {" "}
              <i className="menu-icon fa fa-tags"></i>
              <span className="menu-text"> Brands </span>
            </Link>
          </li>

          <li id="sidebar_menu_categories" className="dropdown-toggle">
            <Link to="/categorieslist">
              {" "}
              <i className="menu-icon fa fa-sitemap"></i>
              <span className="menu-text"> Categories </span>
            </Link>
          </li>

          <li id="sidebar_menu_uom" className="dropdown-toggle">
            <Link to="/uomlist">
              {" "}
              <i className="menu-icon fa fa-balance-scale"></i>
              <span className="menu-text"> UOM </span>
            </Link>
          </li>

          <li id="sidebar_menu_products" className="dropdown-toggle">
            <a href="/productlist">
              {" "}
              <i className="menu-icon fa fa-shopping-cart"></i>
              <span className="menu-text"> Products </span>
            </a>
          </li>

          <li id="sidebar_menu_pricezone" className="dropdown-toggle">
            <a href="/pricezonelist">
              {" "}
              <i className="menu-icon fa fa-globe"></i>{" "}
              <span className="menu-text"> Price Zones </span>
            </a>
          </li>

          <li id="sidebar_menu_accounts" className="dropdown-toggle">
            <Link to="/accountslist">
              {" "}
              <i className="menu-icon fa fa-briefcase"></i>{" "}
              <span className="menu-text"> Accounts </span>
            </Link>
          </li>
          <li id="sidebar_menu_branches" className="dropdown-toggle">
            <Link to="/brancheslist">
              <i className="menu-icon fa fa-home"></i>
              <span className="menu-text"> Branches </span>
            </Link>
          </li>

          <li id="sidebar_menu_itinerary" className="dropdown-toggle">
            <Link to="/itineraries">
              {" "}
              <i className="menu-icon fa fa-calendar"></i>
              <span className="menu-text"> Itineraries </span>
            </Link>
          </li>

          <li id="sidebar_menu_forms" className="dropdown-toggle">
            <Link to="/forms">
              <i className="menu-icon fa fa-tasks"></i>
              <span className="menu-text"> Forms </span>
            </Link>
          </li>

          <li id="sidebar_menu_settings" className="dropdown-toggle">
            <Link to="/api">
              <i className="menu-icon fa fa-tasks"></i>
              <span className="menu-text"> Settings </span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Sidebar;
