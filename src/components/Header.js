import React from "react";

function Header() {
  const navBar = {
    marginBottom: "0px",
    borderRadius: "0px",
    minHeight: "0px"
  };

  return (
    <div className="App">
      {" "}
      {/* <Link to="/dashboard">Dashboard</Link> |{" "}
      <Link to="/reports">Reports</Link> | <Link to="/about">About</Link> */}
      <div
        id="navbar"
        className="navbar  navbar-collapse       ace-save-state"
        style={navBar}
      >
        <div className="navbar-container ace-save-state" id="navbar-container">
          <button
            type="button"
            className="navbar-toggle menu-toggler pull-left"
            id="menu-toggler"
            data-target="#sidebar"
          >
            <span>Toggle sidebar</span>

            <span className="icon-bar"></span>

            <span className="icon-bar"></span>

            <span className="icon-bar"></span>
          </button>
          <div className="navbar-header pull-left">
            <a className="navbar-brand">
              <small>Kustom360</small>
            </a>

            <button
              className="pull-right navbar-toggle navbar-toggle-img collapsed"
              type="button"
              data-toggle="collapse"
              data-target=".navbar-buttons"
            >
              <span className="sr-only">Toggle user menu</span>

              {/* <img src="/assets/avatars/default-male.png" alt="Photo"> */}
            </button>
          </div>
          <div
            className="navbar-buttons navbar-header pull-right  collapse navbar-collapse"
            role="navigation"
          >
            <ul className="nav ace-nav">
              <li className="kustom360-blue dropdown-modal">
                <a
                  id="a_notification_badge"
                  data-toggle="dropdown"
                  className="dropdown-toggle"
                >
                  <i className="ace-icon fa fa-bell"></i>
                </a>
                <ul className="dropdown-menu-right dropdown-navbar navbar-notification dropdown-menu dropdown-caret dropdown-close navbar-notification-right">
                  <li className="dropdown-header">
                    <i className="ace-icon fa fa-exclamation-triangle kustom360-font-color-secondary"></i>
                    0 Notification
                  </li>
                  <li className="dropdown-footer">
                    <a>Notification not available</a>
                  </li>
                </ul>
              </li>

              <li className="grey">
                <a target="_blank">
                  <i className="ace-icon fa fa-question-circle"></i> Support
                </a>
              </li>
              <li className="kustom360-orange dropdown-modal">
                <a data-toggle="dropdown" className="dropdown-toggle">
                  {/* 						
						<img className="nav-user-photo" src="/assets/avatars/default-male.png" alt="Photo"> */}
                  <span className="user-info">
                    <small>Welcome,</small>
                    Jordan
                  </span>

                  <i className="ace-icon fa fa-caret-down"></i>
                </a>

                <ul className="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                  <li>
                    <a>
                      <i className="ace-icon fa fa-user"></i>
                      Profile
                    </a>
                  </li>

                  <li className="divider"></li>

                  <li>
                    <a>
                      <i className="ace-icon fa fa-lock"></i>
                      Change Password
                    </a>
                  </li>
                  <li className="divider"></li>

                  <li>
                    <a>
                      <i className="ace-icon fa fa-power-off"></i>
                      Logout
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
