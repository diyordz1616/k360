import React, { Component } from "react";
import { Link } from "react-router-dom";
import { EditorState } from "draft-js";
// import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SimpleReactValidator from "simple-react-validator";
// import draftToHtml from "draftjs-to-html";
// import SelectUsers from "./SelectUsers";
import { ButtonToolbar } from "react-bootstrap";

class NewRoles extends Component {
  constructor() {
    super();
    this.validator = new SimpleReactValidator();
  }
  state = {
    editorState: EditorState.createEmpty(),
    addModalshow: false,
    roles_name: "",
    web_access: "",
    data_restriction: ""
  };
  onChangeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };
  roles_nameHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  submitHandler = () => {
    this.setState({ addModalShow: true });
    console.log(this.state.addModalshow);
    // console.log(
    //   draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
    // );
  };
  submitForm = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      const formData = new FormData(e.target);
      let formSend = Object.fromEntries(formData);
      console.log(formSend);
      alert("You submitted the form and stuff!");
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      // you can use the autoForceUpdate option to do this automatically`
      this.forceUpdate();
    }
  };
  render() {
    const { editorState } = this.state;
    let addModalClose = () => this.setState({ addModalshow: false });

    return (
      <div class="row">
        <div class="page-header col-xs-12">
          <div class="pull-left">
            <h1>New Role </h1>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="row">
            <div class="col-sm-9">
              <div class="space-2"></div>
              <form
                // context={this.state.context}
                onSubmit={this.submitForm}
              >
                {/* <div class="form-group col-sm-12"> */}
                <div
                  class={
                    this.validator.message(
                      "roles_name",
                      this.state.roles_name,
                      "required"
                    ) !== undefined
                      ? "form-group col-sm-9 has-error"
                      : "form-group col-sm-9"
                  }
                >
                  <label class="control-label">
                    Role Name: <span class="required">*</span>
                  </label>
                  <div class="col-xs-12 no-padding">
                    <div class="clearfix">
                      <input
                        id="roles_name"
                        name="roles_name"
                        type="text"
                        class="form-control"
                        maxlength="50"
                        onChange={this.roles_nameHandler}
                      />
                      {this.validator.message(
                        "roles_name",
                        this.state.roles_name,
                        "required"
                      )}
                    </div>
                  </div>
                </div>

                <div
                  className={
                    this.validator.message(
                      "web_access",
                      this.state.web_access,
                      "required"
                    ) !== undefined
                      ? "form-group col-sm-4 has-error"
                      : "form-group col-sm-4"
                  }
                  id=""
                >
                  <label class="control-label" for="web_acccess">
                    Web Access: <span class="required">*</span>
                  </label>

                  <div class="col-xs-12 no-padding">
                    <div class="clearfix">
                      <select
                        class="select2 form-control"
                        id="web_access"
                        name="web_access"
                        data-placeholder="Click to Choose..."
                        aria-describedby="web_access-error"
                        onChange={this.onChangeHandler}
                      >
                        <option value="" selected="" disabled="">
                          Click to Choose...
                        </option>
                        <option value="true">YES</option>
                        <option value="false">NO</option>
                      </select>
                      {this.validator.message(
                        "web_access",
                        this.state.web_access,
                        "required"
                      )}
                    </div>
                  </div>
                </div>
                <div
                  className={
                    this.validator.message(
                      "data_restriction",
                      this.state.data_restriction,
                      "required"
                    ) !== undefined
                      ? "form-group col-sm-4 has-error"
                      : "form-group col-sm-4"
                  }
                  id=""
                >
                  <label class="control-label" for="data_restriction">
                    Data Restriction: <span class="required">*</span>
                  </label>
                  <div class="col-xs-12 no-padding">
                    <div class="clearfix"> </div>
                    <select
                      class="select2 form-control"
                      id="data_restriction"
                      name="data_restriction"
                      data-placeholder="Click to Choose..."
                      aria-describedby="data_restriction-error"
                      aria-invalid="true"
                      onChange={this.onChangeHandler}
                    >
                      <option value="" selected="" disabled="">
                        Click to Choose...
                      </option>
                      <option value="ALL">No Restriction</option>
                      <option value="GROUP">Group</option>
                      <option value="User">User</option>
                      <option value="test_data_restriction">
                        Test Data Restriction
                      </option>
                    </select>
                    {this.validator.message(
                      "data_restriction",
                      this.state.data_restriction,
                      "required"
                    )}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-9 widget-container-col">
                  <div class="hr hr-dotted"></div>
                  <div class="form-group">
                    <div class="page-header">
                      <h1>Permissions</h1>
                    </div>
                    <div
                      class=""
                      style={{ marginLeft: "10%", marginBottom: "1%" }}
                    >
                      <div class="inline">
                        <h2>
                          <b>Modules</b>
                        </h2>
                      </div>
                    </div>

                    <div class="" style={{ marginLeft: "10%" }}>
                      <div class="inline">
                        <table
                          class="table table-stripped sorted_table"
                          id="table_dashboard"
                        >
                          <thead>
                            <tr>
                              <th>
                                <b>
                                  <a id="sort"></a>
                                </b>
                              </th>
                              <th style={{ textAlign: "center" }} width="80">
                                <b>View</b>
                              </th>
                              <th style={{ textAlign: "center" }} width="80">
                                <b>Add</b>
                              </th>
                              <th style={{ textAlign: "center" }} width="80">
                                <b>Edit</b>
                              </th>
                              <th style={{ textAlign: "center" }} width="80">
                                <b>Delete</b>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="50%">
                                <span id="module_accounts">ACCOUNTS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="accounts_view"
                                  name="accounts_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="accounts_add"
                                  name="accounts_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="accounts_edit"
                                  name="accounts_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="accounts_delete"
                                  name="accounts_delete"
                                />
                              </td>
                            </tr>
                            <tr>
                              <td width="50%">
                                <span id="module_announcements">
                                  ANNOUNCEMENTS
                                </span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="announcements_view"
                                  name="announcements_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="announcements_add"
                                  name="announcements_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="announcements_edit"
                                  name="announcements_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="announcements_delete"
                                  name="announcements_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_billing">BILLING</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="billing_view"
                                  name="billing_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="billing_add"
                                  name="billing_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="billing_edit"
                                  name="billing_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="billing_delete"
                                  name="billing_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_branches">BRANCHES</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="branches_view"
                                  name="branches_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="branches_add"
                                  name="branches_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="branches_edit"
                                  name="branches_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="branches_delete"
                                  name="branches_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_brands">BRANDS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="brands_view"
                                  name="brands_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="brands_add"
                                  name="brands_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="brands_edit"
                                  name="brands_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="brands_delete"
                                  name="brands_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_categories">CATEGORIES</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="categories_view"
                                  name="categories_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="categories_add"
                                  name="categories_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="categories_edit"
                                  name="categories_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="categories_delete"
                                  name="categories_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_dashboard">DASHBOARD</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="dashboard_view"
                                  name="dashboard_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </td>
                              <td valign="middle" align="center">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </td>
                              <td valign="middle" align="center">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_forms">FORMS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="forms_view"
                                  name="forms_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="forms_add"
                                  name="forms_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="forms_edit"
                                  name="forms_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="forms_delete"
                                  name="forms_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_groups">GROUPS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="groups_view"
                                  name="groups_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="groups_add"
                                  name="groups_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="groups_edit"
                                  name="groups_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="groups_delete"
                                  name="groups_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_itineraries">ITINERARIES</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="itineraries_view"
                                  name="itineraries_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="itineraries_add"
                                  name="itineraries_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="itineraries_edit"
                                  name="itineraries_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="itineraries_delete"
                                  name="itineraries_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_pricezones">PRICEZONES</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="pricezones_view"
                                  name="pricezones_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="pricezones_add"
                                  name="pricezones_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="pricezones_edit"
                                  name="pricezones_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="pricezones_delete"
                                  name="pricezones_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_products">PRODUCTS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="products_view"
                                  name="products_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="products_add"
                                  name="products_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="products_edit"
                                  name="products_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="products_delete"
                                  name="products_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_reports">REPORTS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="reports_view"
                                  name="reports_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </td>
                              <td valign="middle" align="center">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </td>
                              <td valign="middle" align="center">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_roles">ROLES</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="roles_view"
                                  name="roles_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="roles_add"
                                  name="roles_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="roles_edit"
                                  name="roles_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="roles_delete"
                                  name="roles_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_uoms">UOMS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="uoms_view"
                                  name="uoms_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="uoms_add"
                                  name="uoms_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="uoms_edit"
                                  name="uoms_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="uoms_delete"
                                  name="uoms_delete"
                                />
                              </td>
                            </tr>

                            <tr>
                              <td width="50%">
                                <span id="module_users">USERS</span>
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="users_view"
                                  name="users_view"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="users_add"
                                  name="users_add"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="users_edit"
                                  name="users_edit"
                                />
                              </td>
                              <td valign="middle" align="center">
                                <input
                                  type="checkbox"
                                  id="users_delete"
                                  name="users_delete"
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <ButtonToolbar>
                    <button
                      variant="primary"
                      // onClick={() => this.setState({ addModalShow: true })}
                      type="submit"
                      class="btn btn-info margin-bottom-10"
                    >
                      <i class="ace-icon fa fa-floppy-o bigger-110"> </i> Submit
                    </button>{" "}
                    {/* <SelectUsers
                      show={this.state.addModalshow}
                      onHide={addModalClose}
                    />{" "} */}
                    <Link to="/roleslist" class="btn margin-bottom-10">
                      <i class="ace-icon fa fa-times"></i> Cancel
                    </Link>
                  </ButtonToolbar>
                  {"  "}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewRoles;
