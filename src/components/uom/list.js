import React, { Component } from "react";
import { Link } from "react-router-dom";
import DataTable from "react-data-table-component";
import axios from "axios";

class UomList extends Component {
  componentDidMount() {
    axios.get("https://reqres.in/api/users?page=2").then(response => {
      this.setState({ uomPromise: response.data });
    });
  }
  state = {
    uomPromise: []
  };
  datatableHandler = key => {
    this.props.history.push("/uomdetails");
  };
  render() {
    const columns = [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "First Name",
        selector: "first_name",
        sortable: true
      },
      {
        name: "Last_name",
        selector: "last_name",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      }
    ];
    const uomData = this.state.uomPromise.data;

    return (
      <div class="row">
        <div class="page-header col-xs-12">
          <div class="pull-left">
            <h1>Unit of Measurements</h1>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <div id="task-tab" class="tab-pane active">
                    <div class="row">
                      <div id="div_filter" class="col-md-12"></div>
                      <div class="col-xs-12 col-md-12 col-sm-12 align-right">
                        <div class="btn-group">
                          <Link
                            to="/newuom"
                            class="btn btn-white btn-sm btn-info btn-round"
                          >
                            <span class="ace-icon fa fa-plus "></span>
                            {"   "}
                            Add New UOM
                          </Link>
                        </div>
                      </div>

                      <div class="space-20"></div>
                      <div class="col-xs-12 content">
                        <div class="space-6"></div>
                        <div class="table-responsive">
                          <DataTable
                            columns={columns}
                            data={uomData}
                            onRowClicked={this.datatableHandler}
                            pointerOnHover
                            striped
                            highlightOnHover
                            pagination
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default UomList;
