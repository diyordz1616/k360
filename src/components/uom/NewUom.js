import React, { Component } from "react";
import { Link } from "react-router-dom";
import { EditorState } from "draft-js";
// import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SimpleReactValidator from "simple-react-validator";
// import draftToHtml from "draftjs-to-html";
// import SelectUsers from "./SelectUsers";
import { ButtonToolbar } from "react-bootstrap";

class NewUom extends Component {
  constructor() {
    super();
    this.validator = new SimpleReactValidator();
  }
  state = {
    editorState: EditorState.createEmpty(),
    addModalshow: false,
    uom_name: ""
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };
  uom_nameHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  submitHandler = () => {
    this.setState({ addModalShow: true });
    console.log(this.state.addModalshow);
    // console.log(
    //   draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
    // );
  };
  submitForm = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      alert("You submitted the form and stuff!");
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      // you can use the autoForceUpdate option to do this automatically`
      this.forceUpdate();
    }
  };
  render() {
    const { editorState } = this.state;
    let addModalClose = () => this.setState({ addModalshow: false });

    return (
      <div class="row">
        <div class="page-header col-xs-12">
          <div class="pull-left">
            <h1>New Unit of Measurement </h1>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="row">
            <div class="col-sm-12">
              <div class="space-2"></div>
              <form
                // context={this.state.context}
                onSubmit={this.submitForm}
              >
                {/* <div class="form-group col-sm-12"> */}
                <div
                  class={
                    this.validator.message(
                      "uom_name",
                      this.state.uom_name,
                      "required"
                    ) !== undefined
                      ? "form-group col-sm-12 has-error"
                      : "form-group col-sm-12"
                  }
                >
                  <label class="control-label">
                    UOM Name: <span class="required">*</span>
                  </label>
                  <div class="col-xs-12 no-padding">
                    <div class="clearfix">
                      <input
                        id="uom_name"
                        name="uom_name"
                        type="text"
                        class="form-control"
                        maxlength="50"
                        onChange={this.uom_nameHandler}
                      />
                      {this.validator.message(
                        "uom_name",
                        this.state.uom_name,
                        "required"
                      )}
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <ButtonToolbar>
                    <button
                      variant="primary"
                      // onClick={() => this.setState({ addModalShow: true })}
                      type="submit"
                      class="btn btn-info margin-bottom-10"
                    >
                      <i class="ace-icon fa fa-floppy-o bigger-110"> </i> Submit
                    </button>{" "}
                    {/* <SelectUsers
                      show={this.state.addModalshow}
                      onHide={addModalClose}
                    />{" "} */}
                    <Link to="/uomlist" class="btn margin-bottom-10">
                      <i class="ace-icon fa fa-times"></i> Cancel
                    </Link>
                  </ButtonToolbar>
                  {"  "}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewUom;
