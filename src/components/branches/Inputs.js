// src/components/CatInputs.js
import React from "react";
const Inputs = props => {
  return props.phone_number.map((val, idx) => {
    let preFix = `pre-${idx}`,
      input_number = `number-${idx}`,
      is_primary = `primary-${idx}`;
    return (
      <div key={idx} className="row" style={{ paddingBottom: "2px" }}>
        <label
          htmlFor={preFix}
          className="col-sm-3 control-label no-padding-right"
        >
          {`Contact No(s)`}
        </label>
        <div className="col-sm-9">
          {/* <input
            type="text"
            name={preFix}
            data-id={idx}
            id={preFix}
            value={props.phone_number[idx].name}
            className="name"
          /> */}
          <select
            name={preFix}
            id={preFix}
            data-id={idx}
            value={props.phone_number[idx].prefix}
            onChange={event => props.handleInputChange(idx, event)}
          >
            <option value="Mobile">Mobile</option>
            <option value="Phone">Phone</option>
            <option value="Other">Other</option>
          </select>{" "}
          <input
            type="text"
            name={input_number}
            data-id={idx}
            id={input_number}
            value={props.phone_number[idx].input_number}
            onChange={event => props.handleInputChange(idx, event)}
            className="conNum"
          />
          <label>
            <input
              name="radio"
              data-id={idx}
              id={is_primary}
              defaultChecked={props.phone_number[idx].is_primary}
              type="radio"
              onChange={event => props.handleInputChange(idx, event)}
              className="ace isPrimaryContact"
            />
            &nbsp;<span className="lbl"> Primary</span>{" "}
          </label>
        </div>
      </div>
    );
  });
};
export default Inputs;
