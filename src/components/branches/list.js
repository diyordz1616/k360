import React, { Component } from "react";
import "bootstrap3/dist/css/bootstrap.css";
import "bootstrap-daterangepicker/daterangepicker.css";
import DataTable from "react-data-table-component";
import moment from "moment";
import axios from "axios";
import { Dropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

class Branches extends Component {
  componentDidMount() {}

  state = {
    toDashboard: false,
    filterText: "",
    date: "Jan 2019",
    showReports: false,
    reportPromise: [],
    reportType: "Sales Entry",
    ranges: {
      Today: [moment(), moment()],
      Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
      "This Month": [moment().startOf("month"), moment().endOf("month")],
      "Last Month": [
        moment()
          .subtract(1, "month")
          .startOf("month"),
        moment()
          .subtract(1, "month")
          .endOf("month")
      ]
    },
    totalUsers: 1,
    totalVisits: 4
  };

  onChange = date => this.setState({ date: date });
  reportHandler = event => {
    this.setState({ reportType: event.target.value });
    this.retrieveData();
  };
  handlePickerSubmit = (event, picker) => {
    const momentString =
      picker.startDate.format("MMM D, YYYY") +
      " to " +
      picker.endDate.format("MMM D, YYYY");
    this.setState({ date: momentString });
    this.retrieveData();
  };

  datatableHandler = key => {
    // console.log(key.id);
    this.props.history.push("/branchdetails");
  };
  retrieveData = () => {
    this.setState({ showReports: true });
    let tU = this.generateRand() + 2;
    this.setState({ totalUsers: tU });
    let tV = this.generateRand() + 4;
    this.setState({ totalVisits: tV });
    let num = this.generateRand();
    axios.get("https://reqres.in/api/users?page=" + num).then(response => {
      this.setState({ reportPromise: response.data });
    });
  };
  generateRand = () => {
    return Math.floor(Math.random() * (3 - 1 + 1)) + 1;
  };
  render() {
    const reportData = this.state.reportPromise.data;
    const columns = [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "First Name",
        selector: "first_name",
        sortable: true
      },
      {
        name: "Last_name",
        selector: "last_name",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      }
    ];
    let reportsDiv = null;

    if (this.state.showReports) {
      reportsDiv = (
        <div>
          <DataTable
            title="Branches"
            columns={columns}
            data={reportData}
            onRowClicked={this.datatableHandler}
            pointerOnHover
            striped
            highlightOnHover
            pagination
          />
        </div>
      );
    }

    return (
      <div className="row">
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>Branches</h1>
          </div>
          <div class="pull-right">
            <div class="btn-group">
              <Link
                to="/branchsettings"
                id="btn_settings"
                class="btn btn-white btn-sm btn-info btn-round"
              >
                <i class="ace-icon fa fa-cog icon-only"></i> Settings
              </Link>
            </div>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="widget-box transparent" id="recent-box">
            <div className="widget-header">
              <div className="col-sm-4">
                Report :{" "}
                <select
                  value={this.state.reportType}
                  onChange={this.reportHandler}
                >
                  <option>Account One</option>
                  <option>Account Three</option>
                </select>
              </div>
              <div className="col-sm-7 pull-right">
                <Dropdown>
                  <Dropdown.Toggle
                    id="dropdown-basic"
                    bsPrefix="btn btn-white btn-sm btn-info btn-round dropdown-toggle"
                  >
                    <span className="ace-icon fa fa-plus icon-on-right"></span>{" "}
                    Add Branch
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Link to="/newBranch">Add a Branch manually</Link>
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-3">
                      <Link to="/multiplebranches">
                        Add several Branches at once (CSV file upload)
                      </Link>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                  {"  "}
                  <Link
                    to="/multiplebranches"
                    className="btn btn-white btn-sm btn-info btn-round dropdown-toggle"
                  >
                    <i className="ace-icon fa fa-pencil bigger-110"></i> Update
                    several Branches (CSV file upload)
                  </Link>{" "}
                  <button
                    className="btn btn-white btn-sm btn-default btn-round"
                    id="btn-multiple-deactivate"
                    disabled=""
                  >
                    <span className="glyphicon glyphicon-trash"></span>{" "}
                    Deactivate
                  </button>
                  {"  "}
                  <a
                    href="/branches/csv/download/data"
                    className="btn btn-white btn-sm btn-info btn-round"
                    role="button"
                  >
                    <span className="glyphicon glyphicon-download"></span>{" "}
                    Export
                  </a>
                </Dropdown>
              </div>
            </div>
          </div>
        </div>
        {reportsDiv}
      </div>
    );
  }
}
export default Branches;
