import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import Geocode from "react-geocode";

const apiKey = "AIzaSyChqGi4UW00p6WMP7Pa6mcmdGf7xfnar3c";
class Contents extends Component {
  state = {
    data: [],
    marker: [{ title: "hello", lat: 0, lng: 0 }],
    loc_x: 0,
    loc_y: 0,
    locRendered: false,
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
    redirect: false,
    redirectId: 0,
    geocoder: [],
    infowindow: [],
    position: null,
    errorMsg: false
  };

  componentDidMount() {
    this.renderAutoComplete();
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps.map) this.renderAutoComplete();
  }

  onSubmit(e) {
    e.preventDefault();
  }
  centerMoved = (mapProps, map) => {
    this.setState({
      loc_x: map.center.lat(),
      loc_y: map.center.lng()
    });
  };
  saveCoords = (mapProps, map, clickEven) => {
    console.log("maps:", mapProps);
    console.log("map:", map);
    console.log("clickeven:", clickEven);
  };
  onMarkerClick = (props, marker, e) => {
    //console.log(marker);
    let geo = this.geocodeLatLng(props);
    let theMarker = Object.fromEntries(
      Object.entries(this.state.marker).filter(
        ([key, val]) => !key.includes("title")
      )
    );

    theMarker["title"] = geo === undefined ? "API Error" : geo;
    // console.log(theMarker);
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: !this.state.showingInfoWindow,
      marker: theMarker
    });
  };
  geocodeLatLng = map => {
    const loc = { lat: this.state.marker.lat, lng: this.state.marker.lng };
    // const { google } = this.props;
    Geocode.setApiKey(apiKey);
    Geocode.fromLatLng(this.state.marker.lat, this.state.marker.lng).then(
      response => {
        const address = response.results[0].formatted_address;
        return address;
      },
      error => {
        console.error(error);
        return "API error.";
      }
    );

    // geocoder({ location: loc }, function(results, status) {
    //   if (status === "OK") {
    //     if (results[0]) {
    //       map.setZoom(11);
    //       // var marker = new google.maps.Marker({
    //       //   position: latlng,
    //       //   map: map
    //       // });
    //       // infowindow.setContent(results[0].formatted_address);
    //       console.log(results[0].formatted_address);
    //       // infowindow.open(map, marker);
    //     } else {
    //       console.log("No results found");
    //     }
    //   } else {
    //     console.log("Geocoder failed due to: " + status);
    //   }
    // });
  };
  onInfoWindowClose = () =>
    this.setState({
      activeMarker: null,
      showingInfoWindow: false
    });
  onDragstartHandler = () => {
    console.log("eeeeee");
  };

  moveMarker = (l, e) => {
    // console.log(l.position);

    this.setState({
      showingInfoWindow: false,
      marker: {
        title: "haller wolrd2",
        lat: e.position.lat(),
        lng: e.position.lng()
      }
    });
  };

  onMapClicked = (p, m, e) => {
    // console.log(p);
    // console.log(e.latLng.lat());
    // console.log(e.latLng.lng());
    let theMarker = this.state.marker;
    theMarker["lat"] = e.latLng.lat();
    theMarker["lng"] = e.latLng.lng();
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    } else {
      this.setState({
        marker: theMarker,
        errorMsg: false
      });
    }
  };
  onSubmit = e => {
    e.preventDefault();
    if (this.state.marker.lat === undefined) {
      this.setState({ errorMsg: true });
    } else {
      const staticMapHtml = "https://maps.googleapis.com/maps/api/staticmap?";
      const coordinatesVal =
        this.state.marker.lat + ", " + this.state.marker.lng;
      let parameter =
        staticMapHtml +
        "center=" +
        coordinatesVal +
        "&" +
        "zoom=16" +
        "&size=300x200" +
        "&markers=color:red|" +
        coordinatesVal +
        "&key=" +
        apiKey;
      // $("#staticMapContainer").html('<img src="' + parameter + '" alt="Maps">');
      this.props.setMapImage(parameter);
      this.props.setLocation(coordinatesVal);
      this.props.onCloseModal();
    }
    // console.log(this.state.marker.lat);
  };
  renderAutoComplete() {
    const { google, map } = this.props;

    if (!google || !map) return;

    const autocomplete = new google.maps.places.Autocomplete(this.autocomplete);
    // this.setState({ geocoder: new google.maps.Geocoder() });
    // this.setState({ infowindow: new google.maps.InfoWindow() });

    autocomplete.bindTo("bounds", map);

    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();

      if (!place.geometry) return;

      if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
      else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }

      this.setState({ position: place.geometry.location });
    });
  }

  render() {
    const { position } = this.state;
    const mapStyles = {
      width: "100%",
      height: "100%"
    };
    return (
      <div style={{ width: "97%" }}>
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            Pin Location
          </h5>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <label for="searchLocation">Search Location:</label>
                  <div class="input-group">
                    <form onSubmit={this.onSubmit}>
                      <div class="input-group">
                        <input
                          type="text"
                          class="form-control search-query"
                          placeholder="Search"
                          id="searchLocation"
                          ref={ref => (this.autocomplete = ref)}
                        />
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-default btn-sm">
                            <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                            Search
                          </button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="alert alert-info alert-dismissible">
                  <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                  >
                    &times;
                  </a>
                  Pin location by <strong>Dragging the red marker</strong> or by{" "}
                  <strong>Clicking the places</strong> on the map.
                </div>
                <div
                  class={
                    this.state.errorMsg
                      ? "alert alert-danger"
                      : "alert alert-danger hide"
                  }
                  id="errorMessageMap"
                >
                  Please pin a location on the map.
                </div>
                <div class="form-group" id="detailsContainer">
                  <h2>Details:</h2>
                  <h5>
                    Address:<span id="lblFormattedAddress"></span>
                  </h5>
                  <h5>
                    Coordinates:
                    <span id="lblCoordinates">
                      <small>
                        {" "}
                        {this.state.marker.lat}, {this.state.marker.lng}
                      </small>
                    </span>
                  </h5>
                </div>
              </div>
              <div class="col-sm-8" style={{ minHeight: "450px" }}>
                <Map
                  // google={this.props.google}
                  {...this.props}
                  zoom={14}
                  onClick={this.onMapClicked}
                  initialCenter={{
                    lat: "14.560867",
                    lng: "121.0271431"
                  }}
                >
                  <Marker
                    onClick={this.onMarkerClick}
                    title={"Marker1"}
                    name={this.state.marker.title}
                    draggable={true}
                    onDragend={this.moveMarker.bind(this)}
                    onDragStart={this.onDragstartHandler.bind(this)}
                    position={{
                      lat: this.state.marker.lat,
                      lng: this.state.marker.lng
                    }}
                  />
                  <InfoWindow
                    draggable={true}
                    position={{
                      lat: this.state.marker.lat,
                      lng: this.state.marker.lng
                    }}
                    marker={this.state.activeMarker}
                    onClose={this.onInfoWindowClose}
                    visible={this.state.showingInfoWindow}
                  >
                    <div>
                      <small>{this.state.marker.title}</small>
                    </div>
                  </InfoWindow>
                </Map>
                {/* <Map
                  {...this.props}
                  center={position}
                  centerAroundCurrentLocation={false}
                  containerStyle={{
                    height: "100vh",
                    position: "relative",
                    width: "100%"
                  }}
                >
                  <Marker position={position} />
                </Map> */}
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button
            type="button"
            class="btn btn-secondary"
            data-dismiss="modal"
            onClick={() => this.props.onCloseModal()}
          >
            Cancel
          </button>
          <button
            type="button"
            class="btn btn-primary"
            id="btnSaveMaps"
            onClick={this.onSubmit}
          >
            Save
          </button>
        </div>
      </div>
    );
  }
}

// export default GoogleApiWrapper({
//   apiKey: "AIzaSyChqGi4UW00p6WMP7Pa6mcmdGf7xfnar3c"
// })(PinMap);

const PinMap = props => (
  <Map
    // className="map"
    google={props.google}
    visible={false}
  >
    <Contents {...props} />
  </Map>
);

export default GoogleApiWrapper({
  apiKey: apiKey
})(PinMap);
