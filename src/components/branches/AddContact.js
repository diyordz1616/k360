import React, { Component } from "react";
import Inputs from "./Inputs";

class Contacts extends Component {
  state = {
    phone_number: [],
    owner: "",
    description: ""
  };

  handleInputChange = (index, event) => {
    const values = [...this.state.phone_number];
    if (event.target.name.includes("number")) {
      values[index].input_number = event.target.value;
    } else if (event.target.name.includes("pre")) {
      values.forEach(e => (e.is_primary = false));
      values[index].prefix =
        event.target.value === "" ? "Mobile" : event.target.value;
    } else {
      values[index].is_primary = event.target.value;
    }

    this.setState({ values });
  };
  addContact = e => {
    this.setState(prevState => ({
      phone_number: [
        ...prevState.phone_number,
        { prefix: "Phone", input_number: "", is_primary: false }
      ]
    }));
    console.log(this.state.phone_number);
  };
  handleSubmit = e => {
    e.preventDefault();
  };
  render() {
    let { phone_number } = this.state;
    return (
      <div onChange={this.handleChange} className="form-group contacts-group">
        <div className="row">
          <div className="col-sm-4"></div>
          <div className="col-sm-8">
            <a onClick={this.addContact} style={{ cursor: "pointer" }}>
              Add Contact No.
            </a>
          </div>
        </div>
        <Inputs
          phone_number={phone_number}
          handleInputChange={this.handleInputChange}
        />
      </div>
    );
  }
}
export default Contacts;
