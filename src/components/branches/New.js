import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { Link } from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
import PinMap from "./PinMap";
import DatePicker from "react-datepicker";
import Contacts from "./AddContact";

import "react-datepicker/dist/react-datepicker.css";
const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

class NewBranch extends Component {
  constructor() {
    super();
    this.validator = new SimpleReactValidator();
  }
  state = {
    open: false,
    branch_name: "",
    account_name: "",
    address1_name: "",
    addresss2_name: "",
    city_name: "",
    contact_person: "",
    contact_email: "",
    price_zone_name: "",
    location: "",
    date_today: "",
    ref_number: 0,
    imageMap: ""
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  setMapImage = q => {
    this.setState({ imageMap: q });
  };
  setLocation = l => {
    this.setState({ location: l });
  };
  onChangeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleDateChange = date => {
    this.setState({
      date_today: date
    });
  };

  submitForm = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      alert("You submitted the form and stuff!");
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      // you can use the autoForceUpdate option to do this automatically`
      this.forceUpdate();
    }
  };
  render() {
    const { open } = this.state;
    const ExampleCustomInput = ({ value, onClick }) => (
      <button className="example-custom-input" onClick={onClick}>
        {value}
      </button>
    );
    return (
      <div className="row">
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>New Branch</h1>
          </div>
        </div>
        <div className="col-xs-12">
          <form
            className="form-horizontal"
            id="form_info"
            onSubmit={this.submitForm}
          >
            <h3 className="lighter block green">
              Enter the following information
            </h3>

            <div className="space-2"></div>
            <div
              className={
                this.validator.message(
                  "account_name",
                  this.state.account_name,
                  "required"
                ) !== undefined
                  ? "form-group has-error"
                  : "form-group"
              }
            >
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="account_name"
              >
                Account: <span className="required">*</span>
              </label>

              <div className="col-sm-3">
                <select
                  className="chosen-select form-control"
                  id="account_name"
                  name="account_name"
                  data-placeholder="Click to choose..."
                  onChange={this.onChangeHandler}
                >
                  <option></option>
                  <option>6566422691971072 </option>
                  <option>Carl Account</option>
                  <option>Mobilemo Inc.</option>
                </select>
                {this.validator.message(
                  "account_name",
                  this.state.account_name,
                  "required"
                )}
              </div>
            </div>

            <div
              className={
                this.validator.message(
                  "branch_name",
                  this.state.branch_name,
                  "required|alpha|min:2"
                ) !== undefined
                  ? "form-group has-error"
                  : "form-group"
              }
            >
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="branch_name"
              >
                Branch: <span className="required">*</span>
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="branch_name"
                    id="branch_name"
                    className="col-xs-12 col-sm-6"
                    maxlength="50"
                    onChange={this.onChangeHandler}
                  />
                </div>
                {this.validator.message(
                  "branch_name",
                  this.state.branch_name,
                  "required|alpha|min:2"
                )}
              </div>
            </div>

            <div className="hr hr-dotted"></div>

            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="address1_name"
              >
                Address 1:{" "}
              </label>

              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="address1_name"
                    id="address1_name"
                    className="col-xs-12 col-sm-6 valid"
                    maxlength="500"
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="address2_name"
              >
                Address 2:{" "}
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="address2_name"
                    id="address2_name"
                    className="col-xs-12 col-sm-6 valid"
                    maxlength="500"
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="city_name"
              >
                City:
              </label>

              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="city_name"
                    id="city_name"
                    className="col-xs-12 col-sm-3 valid"
                    maxlength="50"
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="zipCode_name"
              >
                Zip Code:{" "}
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="zipCode_name"
                    id="zipCode_name"
                    className="col-xs-12 col-sm-3 valid"
                    maxlength="50"
                  />
                </div>
              </div>
            </div>
            <div className="hr hr-dotted"></div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="contact_person"
              >
                Contact Person:
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="contact_person"
                    id="contact_person"
                    className="col-xs-12 col-sm-6 valid"
                    maxlength="50"
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="contact_email"
              >
                Contact Email:
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="text"
                    name="contact_email"
                    id="contact_email"
                    className="col-xs-12 col-sm-6 valid"
                    value={this.state.contact_email}
                    onChange={this.onChangeHandler}
                  />
                </div>
                {this.validator.message(
                  "contact_email",
                  this.state.contact_email,
                  "email"
                )}
              </div>
            </div>
            <div id="div_contacts">
              <div className="form-group contacts-group" id="div_contact">
                {" "}
                <Contacts />
              </div>
            </div>

            <div className="hr hr-dotted"></div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="user_role"
              >
                Price Zone:{" "}
              </label>

              <div className="col-sm-3">
                <select
                  className="chosen-select form-control"
                  id="price_zone_name"
                  name="price_zone_name"
                  data-placeholder="Click to choose..."
                  value={this.state.price_zone_name}
                  onChange={this.onChangeHandler}
                >
                  <option value="4640539223785472---6566422691971072">
                    fpf11{" "}
                  </option>
                  <option>All Products</option>
                </select>
              </div>
            </div>
            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="coordinates"
              >
                Location:
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <button
                    type="button"
                    data-toggle="modal"
                    data-target="#mapsModal"
                    className="btn btn-primary btn-link"
                    onClick={this.onOpenModal}
                  >
                    Pin Location
                  </button>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="control-label col-xs-12 col-sm-3 no-padding-right"></div>
              <div className="col-xs-12 col-sm-9">
                <div className="row">
                  <div
                    className={
                      this.state.imageMap === ""
                        ? "col-sm-2 col-xs-6 hide"
                        : "col-sm-2 col-xs-6"
                    }
                    id="staticMapContainer"
                  >
                    <img src={this.state.imageMap} alt="Maps" />
                  </div>
                </div>
              </div>
            </div>
            <h3 className="lighter block green">Custom Fields:</h3>
            <div className="space-2"></div>

            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="5140133502255104"
              >
                Date Today:
              </label>
              <div className="col-xs-12 col-sm-9">
                <div
                  className="input-group col-sm-3"
                  style={{ cursor: "pointer" }}
                >
                  <DatePicker
                    id="date_today"
                    readonly=""
                    className="form-control date-picker hasDatepicker valid"
                    name="date_today"
                    selected={this.state.date_today}
                    onChange={date => this.handleDateChange(date)}
                    // onChange={this.onChangeHandler}
                  />
                  <span className="input-group-addon">
                    <i className="fa fa-calendar bigger-110"></i>
                  </span>
                </div>
              </div>
            </div>

            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="5692552476884992"
              >
                Reference Number:
              </label>

              <div className="col-xs-12 col-sm-9">
                <div className="clearfix">
                  <input
                    type="number"
                    id="ref_number"
                    name="ref_number"
                    className="col-xs-12 col-sm-5 valid"
                    maxlength="50"
                    value={this.state.ref_number}
                    aria-describedby="5692552476884992-error"
                    onChange={this.onChangeHandler}
                  />
                </div>
              </div>
            </div>

            <div className="space-4"></div>
            <div className="clearfix form-actions">
              <div className="col-md-offset-3 col-md-9">
                <button
                  id="btn_save"
                  className="btn btn-info margin-bottom-10"
                  type="submit"
                >
                  <i className="ace-icon fa fa-check bigger-110"></i> Submit
                </button>{" "}
                <Link
                  id="reset"
                  to="/brancheslist"
                  className="btn margin-bottom-10"
                >
                  {" "}
                  <i className="ace-icon fa fa-times bigger-110"></i> Cancel
                </Link>
              </div>
            </div>
          </form>
        </div>

        <Modal
          open={open}
          onClose={this.onCloseModal}
          closeOnOverlayClick={false}
        >
          <PinMap
            onCloseModal={this.onCloseModal}
            setMapImage={this.setMapImage}
            setLocation={this.setLocation}
          />
        </Modal>
      </div>
    );
  }
}

export default NewBranch;
