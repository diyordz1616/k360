import React, { Component } from "react";
// import ReactDOM from "react-dom";

// import "./style.css";

class Choices extends Component {
  state = {
    name: "",
    chosenname: [{ name: "" }]
  };

  handleShareholderNameChange = idx => evt => {
    const newchosenname = this.state.chosenname.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      return { ...shareholder, name: evt.target.value };
    });

    this.setState({ chosenname: newchosenname });
  };

  handleAddShareholder = () => {
    const lastItem = document.getElementById("multiple_choice_choices")
      .lastChild.firstElementChild.value; //get value of last item
    if (lastItem !== "") {
      this.setState({
        chosenname: this.state.chosenname.concat([{ name: "" }])
      });
    }
  };

  handleRemoveShareholder = idx => () => {
    this.setState({
      chosenname: this.state.chosenname.filter((s, sidx) => idx !== sidx)
    });
  };

  render() {
    return (
      <div className="control-group" id="multiple_choice_div">
        <label className="control-label">Choices</label>
        <div className="controls form-group">
          <ol
            id="multiple_choice_choices"
            className="list-type-alpha question-choices"
          >
            {this.props.chosenname.map((shareholder, idx) => (
              <li
                key={idx}
                className="question-choice-list"
                style={{ marginBottom: "5px" }}
              >
                <input
                  className="question-choice"
                  type="text"
                  value={shareholder.name}
                  onChange={() => this.props.handleShareholderNameChange(idx)}
                />{" "}
                <button
                  type="button"
                  className="btn btn-danger btn-sm remove-choice"
                  //onClick={this.props.handleRemoveShareholder(idx)}
                >
                  <i className="fa fa-times"></i>
                </button>
              </li>
            ))}
          </ol>

          <button
            type="button"
            onClick={this.props.handleAddShareholder}
            className="btn btn-success btn-sm add-choice"
            style={{
              marginLeft: "25px",
              paddingLeft: "78px",
              paddingRight: "78px"
            }}
          >
            Add
          </button>
        </div>
      </div>
    );
  }
}

export default Choices;
