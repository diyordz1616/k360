import React, { createRef } from "react";
import $ from "jquery";
// import ReactDOM from "react-dom";
import "jquery-ui-sortable";
import "formBuilder/dist/starRating";

window.jQuery = $;
window.$ = $;

require("jquery-ui-sortable");
require("formBuilder");

const formData = [
  {
    type: "header",
    subtype: "h1",
    label: "Form Items"
  },
  {
    type: "paragraph",
    label: "(Drop form tools here)"
  },
  { stickyControls: { enable: true } }
];

class newBuilder extends React.Component {
  fb = createRef();
  componentDidMount() {
    $(this.fb.current).formBuilder({ formData });
  }
  render() {
    return (
      <div className="row">
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>Forms</h1>
          </div>
        </div>
        <div className="col-md-8">
          <label className="col-sm-9">
            Form Name:<span className="required">*</span>
          </label>
          <input type="text" className="form-control" />
          <br />
          <label>
            Roles<span className="required">* </span>
          </label>
          <button className="btn btn-info">Add </button>

          <div className="control-group">
            <div className="checkbox">
              <label className="col-sm-9">Settings:</label>
              <br />
              <label>
                <input
                  name="form-field-checkbox"
                  type="checkbox"
                  className="ace"
                />
                <span className="lbl"> Send Result to Sender's Email</span>
              </label>
            </div>

            <div className="checkbox">
              <label>
                <input
                  name="form-field-checkbox"
                  type="checkbox"
                  className="ace"
                />
                <span className="lbl"> Enable Photo Taking</span>
              </label>
            </div>
            <div className="checkbox">
              <label>
                <input
                  name="form-field-checkbox"
                  type="checkbox"
                  className="ace"
                />
                <span className="lbl"> Enable Signature</span>
              </label>
            </div>
            <div className="checkbox">
              <label>
                <input
                  name="form-field-checkbox"
                  type="checkbox"
                  className="ace"
                />
                <span className="lbl"> Required</span>
              </label>
            </div>
          </div>
        </div>
        <div id="fb-editor" ref={this.fb} />
      </div>
    );
  }
}
export default newBuilder;
