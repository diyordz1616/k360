import React, { Component } from "react";
// import { Link } from "react-router-dom";
// import DataTable from "react-data-table-component";
// import axios from "axios";

class Forms extends Component {
  state = {
    groupName: "",
    toolType: "",
    modalContent: "",
    modalContentSettings: ""
  };
  componentDidMount() {
    // this.editMe(val, num);
  }
  editMe = (val, num) => {
    // let sets = [];
    // let contents = [];
    // switch (val.value) {
    //   case "Question Group":
    //     this.setState({ toolType: "Question Group" });
    //     contents.push([
    //       <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
    //     ]);
    //     contents.push(this.formGroupContent);
    //     this.setState({ modalContent: contents });
    //     this.setState({ modalContentSettings: "" });
    //     break;
    //   default:
    //     this.setState({ modalContent: <p>Form not set.</p> });
    //     this.setState({ modalContentSettings: "" });
    // }
    // contents.push([<input type="hidden" name="group" value={group} />]);
  };
  onChange = e => {
    if (e) {
      this.setState({
        [e.target.name]: e.target.value
      });
    }
  };
  formSubmit = ev => {
    ev.preventDefault();
    const formData = new FormData(ev.target);
    let formSend = Object.fromEntries(formData);
    console.log(formSend);
    this.props.setTool(formSend); //send to state
    this.props.onCloseModal(1);
  };
  render() {
    let formGroupContent = (
      <div id="div_question_details" className="control-group">
        <label id="question_label" className="control-label">
          {/* {this.state.groupname} */}
          Form Group Name
        </label>
        <span id="question_count" className="pull-right">
          0 of 500
        </span>
        <div className="controls form-group">
          <textarea
            id="question_g"
            className="form-control"
            name="groupName"
            rows="3"
            required="required"
            maxLength="500"
            value={this.state.groupName}
            onChange={e => this.onChange(e)}
          ></textarea>
        </div>{" "}
      </div>
    );
    return (
      <div className="modal-dialog">
        <form
          id="add_survey_form"
          method="get"
          data-validate="parsley"
          style={{ minWidth: "350px" }}
          onSubmit={this.formSubmit}
        >
          <fieldset>
            <div className="modal-header">
              <span>
                <h4 className="modal-title" id="myModalLabel">
                  Form Item Details
                </h4>
                (
                <label id="question_type_label">
                  Tool Type : {this.props.toolType}
                </label>
                )
              </span>
            </div>
            <div className="modal-body">{this.state.modalContent}</div>
          </fieldset>
          {this.props.modalContentSettings !== "" ? (
            <div className="control-group" id="settings_div">
              <label className="control-label">Settings</label>
              <div className="controls form-group">
                {this.props.toolType === "Question Group"
                  ? formGroupContent
                  : null}
              </div>
            </div>
          ) : (
            " "
          )}
          <div className="modal-footer">
            <button
              id="close_question_dialog"
              type="button"
              className="btn btn-default"
              data-dismiss="modal"
              onClick={() => this.props.onCloseModal(1)}
            >
              Cancel
            </button>
            <button
              id="submit_question_dialog"
              type="submit"
              className="btn btn-primary"
            >
              Done
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Forms;
