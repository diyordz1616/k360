// import random from "lodash/random";
import uniqueId from "lodash/uniqueId";
// import uniq from "lodash/uniq";
import React, { Component } from "react";
import { Link } from "react-router-dom";
// import ReactDOM from "react-dom";
import Modal from "react-responsive-modal";
import Choices from "./Choices";
import { ReactSortable } from "react-sortablejs";
// import Sortable from "react-sortablejs";
import "../../assets/css/lists.css";

class FormSorter extends Component {
  state = {
    cloneControlledSource: [
      { id: 2, value: "Text" }
      // { id: 3, value: "Multiline" },
      // { id: 4, value: "Yes No" },
      // { id: 5, value: "Rating" },
      // { id: 6, value: "Drop Down" },
      // { id: 7, value: "Multiple Choice" },
      // { id: 8, value: "Number" },
      // { id: 9, value: "Date" },
      // { id: 10, value: "Email" },
      // { id: 11, value: "Time" },
      // { id: 12, value: "Product" },
      // { id: 13, value: "Formula" }
    ],
    cloneControlledTarget1: [],
    cloneControlledSource1: [
      "Question Group",
      "Text",
      "Multiline"
      // "Yes No",
      // "Rating",
      // "Drop Down",
      // "Multiple Choice",
      // "Number",
      // "Date",
      // "Email",
      // "Time",
      // "Product",
      // "Formula"
    ],

    cloneControlledTarget: [],
    modalContent: "",
    modalContentSettings: "",
    open: false,
    name: "", //for choices
    chosenname: [{ name: "" }], //for choices
    list: [
      { id: 2, value: "Text" },
      { id: 3, value: "Multiline" },
      { id: 4, value: "Yes No" },
      { id: 5, value: "Rating" },
      { id: 6, value: "Drop Down" },
      { id: 7, value: "Multiple Choice" },
      { id: 8, value: "Number" },
      { id: 9, value: "Date" },
      { id: 10, value: "Email" },
      { id: 11, value: "Time" },
      { id: 12, value: "Product" },
      { id: 13, value: "Formula" }
    ],
    list1: []
  };

  //   onOpenModal = () => {
  //     this.setState({ open: true });
  //   };

  //   onCloseModal = () => {
  //     this.setState({ open: false });
  //   };
  //   formSubmit = ev => {
  //     ev.preventDefault();
  //     const formData = new FormData(ev.target);
  //     let formSend = Object.fromEntries(formData);
  //     this.setTool(formSend); //send to state
  //     this.onCloseModal();
  //     console.log(this.state.cloneControlledTarget);
  //   };
  //   setTool = formdata => {
  //     //set tool according to value
  //     //console.log(this.state.cloneControlledTarget);
  //     let cloneTarget = this.state.cloneControlledTarget;
  //     cloneTarget.forEach(target => {
  //       if (target.id === formdata.id) {
  //         target["tool"] = formdata;
  //       }
  //     });
  //     document
  //       .getElementById(formdata.id)
  //       .setAttribute("data-tool", JSON.stringify(formdata)); //print to LI

  //     this.setState({ cloneControlledTarget: cloneTarget });
  //     //console.log(this.state.cloneControlledTarget);
  //   };
  //   deleteMe = val => {
  //     this.setState({
  //       cloneControlledTarget: this.state.cloneControlledTarget.filter(function(
  //         tool
  //       ) {
  //         return tool.id !== val.id;
  //       })
  //     });
  //     var elem = document.getElementById(val.id);
  //     elem.parentNode.removeChild(elem);
  //     console.log(this.state.cloneControlledTarget);
  //   };
  //   editMe = val => {
  //     let sets = [];
  //     let contents = [];
  //     switch (val.value) {
  //       case "Question Group":
  //         this.setState({ toolType: "Question Group" });
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         contents.push(this.formGroupContent);
  //         this.setState({ modalContent: contents });
  //         this.setState({ modalContentSettings: "" });
  //         break;
  //       case "Text":
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         this.setState({ toolType: "Text" });
  //         contents.push(this.formItemContent);
  //         this.setState({ modalContent: contents });
  //         sets.push(this.numericContent);
  //         sets.push(this.requiredContent);
  //         sets.push(this.barcodeContent);
  //         this.setState({
  //           modalContentSettings: sets
  //         });
  //         break;
  //       case "Multiline":
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         this.setState({ toolType: "Multiline" });
  //         contents.push(this.formItemContent);
  //         this.setState({ modalContent: contents });
  //         sets.push(this.requiredContent);
  //         this.setState({
  //           modalContentSettings: sets
  //         });
  //         break;
  //       case "Yes No":
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         this.setState({ toolType: "Yes/No" });
  //         contents.push(this.formItemContent);
  //         this.setState({ modalContent: contents });
  //         this.setState({ modalContentSettings: "" });
  //         break;
  //       case "Rating":
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         this.setState({ toolType: "Rating" });
  //         contents.push(this.formItemContent);
  //         this.setState({ modalContent: contents });
  //         this.setState({ modalContentSettings: "" });
  //         break;
  //       case "Drop Down":
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         this.setState({ toolType: "Drop Down" });
  //         contents.push(this.formItemContent);
  //         contents.push(this.choicesContent);
  //         this.setState({ modalContent: contents });
  //         sets.push(this.requiredContent);
  //         this.setState({
  //           modalContentSettings: sets
  //         });
  //         break;
  //       case "Multiple Choice":
  //         contents.push([<input type="hidden" name="id" value={val.id} />]);
  //         this.setState({ toolType: "Multiple Choic" });
  //         contents.push(this.formItemContent);
  //         contents.push(this.multiplechoicesContent);
  //         this.setState({ modalContent: contents });
  //         sets.push(this.requiredContent);
  //         sets.push(this.allMultipleContent);
  //         this.setState({
  //           modalContentSettings: sets
  //         });
  //         break;
  //       default:
  //         this.setState({ modalContent: <p>Form not set.</p> });
  //         this.setState({ modalContentSettings: "" });
  //     }
  //     //console.log(this.state.modalContentSettings);
  //     this.onOpenModal();
  //     //print the data to modal
  //     this.getModalData(val.id);
  //   };
  //   getModalData = id => {
  //     const toolData = this.state.cloneControlledTarget.filter(
  //       data => data.id === id
  //     );
  //     if (toolData[0].hasOwnProperty("tool") === true) {
  //       //if there's value
  //       toolData[0].tool.hasOwnProperty("question")
  //         ? (document.getElementById("question").value =
  //             toolData[0].tool.question)
  //         : (document.getElementById("question").value = "");
  //       toolData[0].tool.hasOwnProperty("is_numeric_only_text_settings")
  //         ? (document.getElementById(
  //             "is_numeric_only_text_settings"
  //           ).checked = true)
  //         : console.clear();
  //       toolData[0].tool.hasOwnProperty("required")
  //         ? (document.getElementById("required_settings").checked = true)
  //         : console.clear();
  //       toolData[0].tool.hasOwnProperty("enable_barcode_settings")
  //         ? (document.getElementById("enable_barcode_settings").checked = true)
  //         : console.clear();
  //       toolData[0].tool.hasOwnProperty("choice")
  //         ? (document.getElementById("drop_down_choice_textarea").value =
  //             toolData[0].tool.choice)
  //         : console.clear();
  //     }
  //   };
  //   addState = evt => {
  //     //let myUnique = uniqueId();
  //     let toolz = [];
  //     let myId = {
  //       value: evt.item.getAttribute("data-id"),
  //       id: evt.item.getAttribute("data-key"),
  //       tool: evt.item.getAttribute("data-tool")
  //     };
  //     //evt.item.setAttribute("data-key", myUnique); //add a unique ID on add to LI
  //     toolz.push(myId);
  //     this.setState({
  //       cloneControlledTarget: [...this.state.cloneControlledTarget, ...toolz]
  //     });
  //     this.syncState(evt.to);
  //   };
  //   syncState = evt => {
  //     let toolz = [];
  //     let lis = evt.getElementsByTagName("li");
  //     for (var i = 0; i < lis.length; i++) {
  //       toolz.push({
  //         value: lis[i].getAttribute("data-id"),
  //         id: lis[i].getAttribute("data-key"),
  //         tool: lis[i].getAttribute("data-tool")
  //       });
  //     }
  //     // evt.item.setAttribute("data-id", JSON.stringify(myId)); //add a unique ID on add to LI
  //     this.setState({ cloneControlledTarget: toolz });
  //   };
  //   clearForm = () => {
  //     this.setState({ cloneControlledTarget: [] });
  //     let ul = document.querySelector("ul.A");
  //     var listLength = ul.children.length;

  //     for (let i = 0; i < listLength; i++) {
  //       ul.removeChild(ul.children[0]);
  //     }
  //   };
  //   handleShareholderNameChange = idx => evt => {
  //     const newchosenname = this.state.chosenname.map((name, sidx) => {
  //       if (idx !== sidx) return name;
  //       return { ...name, name: evt.target.value };
  //     });

  //     this.setState({ chosenname: newchosenname });
  //   };

  //   handleAddShareholder = () => {
  //     const lastItem = document.getElementById("multiple_choice_choices")
  //       .lastChild.firstElementChild.value; //get value of last item

  //     if (lastItem !== "") {
  //       this.setState({
  //         chosenname: this.state.chosenname.concat([{ name: "" }])
  //       });
  //     }
  //     //console.log(this.state.chosenname);
  //   };

  //   handleRemoveShareholder = idx => () => {
  //     this.setState({
  //       chosenname: this.state.chosenname.filter((s, sidx) => idx !== sidx)
  //     });
  //   };

  render() {
    const items = this.state.list.map((val, key) => (
      <li key={key} data-id={val}>
        {val}
      </li>
    ));
    // const cloneControlledSource = this.state.cloneControlledSource.map(
    //   (val, key) => (
    //     <li key={uniqueId()}>
    //       <i
    //         className={this.getIcon(val.value)}
    //         style={{ cursor: "pointer" }}
    //       ></i>{" "}
    //       {"  "}
    //       {val.value}
    //     </li>
    //   )
    // );
    // const cloneControlledTarget = this.state.cloneControlledTarget.map(
    //   (val, key) => (
    //     <li key={val.id} data-id={val.value}>
    //       <i
    //         className={this.getIcon(val.value)}
    //         style={{ cursor: "pointer" }}
    //       ></i>{" "}
    //       {"  "}
    //       {val.value}
    //       <i
    //         className="ace-icon fa fa-trash-o bigger-130 pull-right"
    //         style={{ cursor: "pointer", color: "#DD5A43" }}
    //         onClick={() => {
    //           this.deleteMe(val);
    //         }}
    //       ></i>
    //       <i
    //         className="ace-icon fa fa-pencil bigger-130 pull-right"
    //         style={{ cursor: "pointer", color: "#f89406" }}
    //         onClick={() => {
    //           this.editMe(val);
    //         }}
    //       ></i>
    //     </li>
    //   )
    // );
    const { open } = this.state;

    return (
      <div>
        <div>
          <div className="page-header col-xs-12">
            <div className="pull-left">
              <h1>New Form</h1>
            </div>
          </div>

          <div className="col-sm-6">
            <div className="form-group">
              <label>
                Form Name: <span class="required">*</span>
              </label>{" "}
              <input
                id="form_name"
                name="form_name"
                type="text"
                className="form-control"
                maxLength="50"
              />
            </div>
            <div className="form-group">
              <label className="pull-left">
                Roles:<span className="required">*</span>
              </label>

              <span
                type="button"
                id="addRoles"
                className="btn btn-sm btn-success margin-bottom-10 pull-left"
              >
                Add Role
              </span>

              <div id="add_roles_target" className="list-group col-sm-6"></div>
            </div>
            <div
              className="hr hr-double dotted"
              style={{ clear: "both" }}
            ></div>

            <label>
              Settings: <span className="required"></span>
            </label>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="enable_send_to_email"
                  name="enable_send_to_email"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />
                <span className="lbl bigger-120">
                  {" "}
                  Send Result to Sender's Email
                </span>
              </label>
            </div>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="enable_photo"
                  name="enable_photo"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />{" "}
                <span className="lbl bigger-120"> Enable Photo Taking</span>
              </label>
            </div>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="enable_signature"
                  name="enable_signature"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />
                <span className="lbl bigger-120"> Enable Signature</span>
              </label>
            </div>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="is_form_required"
                  name="is_form_required"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />
                <span className="lbl bigger-120"> Required</span>
              </label>
            </div>
            <p>(Drop form tools here.) </p>
            <ReactSortable
              className="block-list A"
              list={this.state.list1}
              setList={newState => this.setState({ list1: newState })}
              animation={150}
              group={{ name: "cloning-group-name", pull: "clone" }}
              clone={item => ({ ...item, id: item.id })}
            >
              {this.state.list1.map(item => (
                <div key={item.id}>{item.value}</div>
              ))}
            </ReactSortable>
          </div>

          <div className="row">
            <div className="col-sm-6">
              {" "}
              <ReactSortable
                animation={150}
                group={{ name: "cloning-group-name", pull: "clone" }}
                className="block-list"
                sort={false}
                //   tag="ul"
                list={this.state.list}
                setList={newState => this.setState({ list: newState })}
              >
                {this.state.list.map(item => (
                  <div key={item.id}>{item.value}</div>
                ))}
              </ReactSortable>
            </div>
          </div>
          <div className="col-md-9">
            <button
              id="btn_save"
              className="btn btn-info margin-bottom-10"
              type="button"
            >
              <i className="ace-icon fa fa-floppy-o bigger-110"></i>Save
            </button>
            &nbsp; &nbsp; &nbsp;
            <button
              id="btn_save_and_activate"
              className="btn btn-success margin-bottom-10"
              type="button"
            >
              <i className="ace-icon fa fa-check-o bigger-110"></i>Save and
              Activate
            </button>
            &nbsp; &nbsp; &nbsp;
            <button
              id="btn_clear"
              className="btn btn-secondary margin-bottom-10"
              type="button"
              onClick={() => {
                this.clearForm();
              }}
            >
              <i className="ace-icon fa fa-floppy-o bigger-110"></i>Clear
            </button>
            &nbsp; &nbsp; &nbsp;
            <Link
              to="/forms"
              id="reset"
              className="btn btn-danger margin-bottom-10"
            >
              <i className="ace-icon fa fa-times"></i> Cancel
            </Link>
          </div>
        </div>
        {/* <Modal
          open={open}
          onClose={this.onCloseModal}
          closeOnOverlayClick={false}
        >
          <div className="modal-dialog">
            <form
              id="add_survey_form"
              method="get"
              data-validate="parsley"
              style={{ minWidth: "350px" }}
              onSubmit={this.formSubmit}
            >
              <fieldset>
                <div className="modal-header">
                  <span>
                    <h4 className="modal-title" id="myModalLabel">
                      Form Item Details
                    </h4>
                    (
                    <label id="question_type_label">
                      Tool Type : {this.state.toolType}
                    </label>
                    )
                  </span>
                  <br />
                </div>
                <div className="modal-body">{this.state.modalContent}</div>
              </fieldset>

              {this.state.modalContentSettings !== "" ? (
                <div className="control-group" id="settings_div">
                  <label className="control-label">Settings</label>
                  <div className="controls form-group">
                    {this.state.modalContentSettings}
                  </div>
                </div>
              ) : (
                " "
              )}
              <div className="modal-footer">
                <button
                  id="close_question_dialog"
                  type="button"
                  className="btn btn-default"
                  data-dismiss="modal"
                  onClick={this.onCloseModal}
                >
                  Cancel
                </button>
                <button
                  id="submit_question_dialog"
                  type="submit"
                  className="btn btn-primary"
                >
                  Done
                </button>
              </div>
            </form>
          </div>
        </Modal> */}
      </div>
    );
  }
  //   formGroupContent = (
  //     <div id="div_question_details" className="control-group">
  //       <label id="question_label" className="control-label">
  //         Form Group Name
  //       </label>
  //       <span id="question_count" className="pull-right">
  //         0 of 500
  //       </span>
  //       <div className="controls form-group">
  //         <textarea
  //           id="question"
  //           className="form-control"
  //           name="question"
  //           rows="3"
  //           required="required"
  //           maxLength="500"
  //         ></textarea>
  //       </div>
  //     </div>
  //   );
  //   formItemContent = (
  //     <div id="div_question_details" className="control-group">
  //       <label id="question_label" className="control-label">
  //         Form Item
  //       </label>
  //       <span id="question_count" className="pull-right">
  //         0 of 500
  //       </span>
  //       <div className="controls form-group">
  //         <textarea
  //           id="question"
  //           className="form-control"
  //           name="question"
  //           rows="3"
  //           required="required"
  //           maxLength="500"
  //         ></textarea>
  //       </div>
  //     </div>
  //   );
  //   choicesContent = (
  //     <div id="div_question_details" className="control-group">
  //       <label id="question_label" className="control-label">
  //         Choices
  //       </label>
  //       <label style={{ fontStyle: "italic" }}>(Add one choice per line)</label>
  //       <div className="controls form-group">
  //         <textarea
  //           id="drop_down_choice_textarea"
  //           className="form-control"
  //           name="choice"
  //           rows="5"
  //         ></textarea>
  //       </div>
  //     </div>
  //   );
  //   multiplechoicesContent = (
  //     <div className="control-group" id="multiple_choice_div">
  //       <Choices
  //         handleAddShareholder={this.handleAddShareholder}
  //         handleShareholderNameChange={this.handleShareholderNameChange.bind(
  //           this
  //         )}
  //         name={this.state.name}
  //         chosenname={this.state.chosenname}
  //       />
  //     </div>
  //   );
  //   numericContent = (
  //     <div className="checkbox">
  //       <label>
  //         <input
  //           id="is_numeric_only_text_settings"
  //           name="is_numeric_only_text_settings"
  //           className="ace ace-checkbox-2"
  //           type="checkbox"
  //         />
  //         <span className="lbl"> Numeric Input Only </span>
  //       </label>
  //     </div>
  //   );
  //   barcodeContent = (
  //     <div className="checkbox">
  //       <label>
  //         <input
  //           id="enable_barcode_settings"
  //           name="enable_barcode_settings"
  //           className="ace ace-checkbox-2"
  //           type="checkbox"
  //         />
  //         <span className="lbl"> Enable Barcode </span>
  //       </label>
  //     </div>
  //   );
  //   requiredContent = (
  //     <div className="checkbox">
  //       <label>
  //         <input
  //           id="required_settings"
  //           name="required"
  //           className="ace ace-checkbox-2"
  //           type="checkbox"
  //         />
  //         <span className="lbl"> Required </span>
  //       </label>
  //     </div>
  //   );
  //   allMultipleContent = (
  //     <div className="checkbox">
  //       <label>
  //         <input
  //           id="allow_multiple_selection_settings"
  //           name="allow_multiple_selection_settings"
  //           className="ace ace-checkbox-2"
  //           type="checkbox"
  //         />
  //         <span className="lbl"> Allow multiple selection </span>
  //       </label>
  //     </div>
  //   );
  //   getIcon = value => {
  //     switch (value) {
  //       case "Question Group":
  //         return "fa fa-question-circle";
  //         break;
  //       case "Text":
  //         return "fa fa-font";
  //         break;
  //       case "Multiline":
  //         return "fa fa-align-justify";
  //         break;
  //       case "Yes No":
  //         return "fa fa-dot-circle-o";
  //         break;
  //       case "Rating":
  //         return "fa fa-star-o";
  //         break;
  //       case "Drop Down":
  //         return "fa fa-caret-down";
  //         break;
  //       case "Multiple Choice":
  //         return "fa fa-th-large";
  //         break;
  //       case "Number":
  //         return "fa fa-hashtag";
  //         break;
  //       case "Date":
  //         return "fa fa-calendar";
  //         break;
  //       case "Email":
  //         return "fa fa-envelope-o";
  //         break;
  //       case "Time":
  //         return "fa fa-clock-o";
  //         break;
  //       case "Product":
  //         return "fa fa-product-hunt";
  //         break;
  //       case "Formula":
  //         return "fa fa-calculator";
  //         break;
  //       default:
  //         return "fa fa-tasks";
  //     }
  //   };
}

export default FormSorter;
