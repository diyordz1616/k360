import React, { Component } from "react";
import { Link } from "react-router-dom";
import DataTable from "react-data-table-component";
import axios from "axios";

class FormList extends Component {
  componentDidMount() {
    axios.get("https://reqres.in/api/users?page=1").then(response => {
      this.setState({ formListPromise: response.data });
    });
  }
  state = {
    formListPromise: []
  };
  datatableHandler = key => {
    //this.props.history.push("/announcementdetails");
  };
  render() {
    const columns = [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "First Name",
        selector: "first_name",
        sortable: true
      },
      {
        name: "Last_name",
        selector: "last_name",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      }
    ];
    const formListData = this.state.formListPromise.data;

    return (
      <div className="row">
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>Forms</h1>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="widget-box transparent" id="recent-box">
            <div className="widget-body">
              <div className="widget-main padding-4">
                <div className="tab-content padding-8">
                  <div id="task-tab" className="tab-pane active">
                    <div className="row">
                      <div id="div_filter" className="col-md-12"></div>
                      <div className="col-xs-12 col-md-12 col-sm-12 align-right">
                        <div className="btn-group">
                          <Link
                            to="/formsort"
                            className="btn btn-white btn-sm btn-info btn-round"
                          >
                            <span className="ace-icon fa fa-plus "></span>
                            {"   "}
                            Add New Form
                          </Link>
                        </div>
                        <div className="btn-group">
                          <Link
                            to="/DeleteForm"
                            className="btn btn-white btn-sm btn-info btn-round btn-danger"
                          >
                            <span className="glyphicon glyphicon-trash "></span>
                            {"   "}
                            Delete Form
                          </Link>
                        </div>
                      </div>

                      <div className="space-20"></div>
                      <div className="col-xs-12 content">
                        <div className="space-6"></div>
                        <div className="table-responsive">
                          <DataTable
                            columns={columns}
                            data={formListData}
                            onRowClicked={this.datatableHandler}
                            pointerOnHover
                            striped
                            highlightOnHover
                            pagination
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default FormList;
