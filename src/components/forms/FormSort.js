import nextId from "react-id-generator";
import React, { Component } from "react";
import { Link } from "react-router-dom";
// import ReactDOM from "react-dom";
import Modal from "react-responsive-modal";
// import Choices from "./Choices";
import { ReactSortable } from "react-sortablejs";
import StickyBox from "react-sticky-box";
// import Sortable from "react-sortablejs";
import "../../assets/css/lists.css";

class FormSort extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  state = {
    cloneControlledSource: [
      { id: 1, value: "Question Group" },
      { id: 2, value: "Text" },
      { id: 3, value: "Multiline" },
      { id: 4, value: "Yes No" },
      { id: 5, value: "Rating" },
      { id: 6, value: "Drop Down" },
      { id: 7, value: "Multiple Choice" },
      { id: 8, value: "Number" },
      { id: 9, value: "Date" },
      { id: 10, value: "Email" },
      { id: 11, value: "Time" },
      { id: 12, value: "Product" },
      { id: 13, value: "Formula" }
    ],
    cloneControlledTarget: [], //default state
    cloneControlledTarget0: [], //state for PRODUCT
    cloneControlledTarget1: [], //destination for Question group
    cloneControlledTarget2: [], //destination for Question group
    cloneControlledTarget3: [], //destination for Question group
    cloneControlledTarget4: [], //destination for Question group
    roles: [
      { text: "Administrator", checked: false },
      { text: "New Role", checked: false },
      { text: "Owner", checked: false }
    ],
    isChecked: false,
    modalContent: "",
    modalContentSettings: "",
    open: false,
    openb: false,
    name: "", //for choices
    inputValueLimit: 1000,
    chosenname: [{ name: "" }], //for choices
    groupname: "",
    isDraggable: true,
    toolType: ""
  };

  onOpenModal = e => {
    if (e === 1) {
      this.setState({ open: true });
    } else {
      this.setState({ openb: true });
    }
  };
  onCloseModal = e => {
    if (e === 1) {
      this.setState({ open: false });
    } else {
      this.setState({ openb: false });
    }
  };
  selectRoles = ev => {
    //ev.preventDefault();
    // if (this.state.isChecked) {
    //   let w = document.getElementsByClassName("getRoles");
    //   w.forEach(e => (e.checked = true));
    // } else {
    //   document.getElementsByClassName("getRoles").checked = true;
    // }
    this.setState({
      isChecked: !this.state.isChecked
    });
  };
  onToggle = (index, e) => {
    let newItems = this.state.roles.slice();
    newItems[index].checked = !newItems[index].checked;
    this.setState({
      roles: newItems
    });
  };
  formSubmit = ev => {
    ev.preventDefault();
    const formData = new FormData(ev.target);
    // console.log(ev);
    let formSend = Object.fromEntries(formData);
    // console.log(formSend);
    this.setTool(formSend); //send to state
    this.onCloseModal(1);
  };
  setTool = formdata => {
    //console.log(formdata);
    //set tool according to value
    let group = formdata.group === NaN ? 2 : formdata.group;

    let cloneTarget =
      formdata.group == 1
        ? this.state.cloneControlledTarget
        : group == 0
        ? this.getChosenState(0)
        : this.getChosenState(group - 1);

    cloneTarget.forEach(target => {
      if (target.id == formdata.id) {
        target["tool"] = formdata;
      }
    });
    if (formdata.type === "Product") this.setProduct(formdata);
    //console.log(this.state.cloneControlledTarget);
  };
  setProduct = formdata => {
    //sets or deletes PRODUCT tools
    let target = this.state.cloneControlledTarget0.filter(function(tool) {
      return (
        tool.value !== "QUANTITY" &&
        tool.value !== "UOM" &&
        tool.value !== "DISCOUNT"
      );
    }); //rinse data first
    if (formdata.hasOwnProperty("enable_oum_settings")) {
      target.push({
        id: nextId(),
        value: "UOM",
        chosen: false,
        selected: false
      });
    }
    if (formdata.hasOwnProperty("enable_product_quantity_selection_settings")) {
      target.push({
        id: nextId(),
        value: "QUANTITY",
        chosen: false,
        selected: false
      });
    }
    if (formdata.hasOwnProperty("enable_product_discount_selection_settings")) {
      target.push({
        id: nextId(),
        value: "DISCOUNT",
        chosen: false,
        selected: false
      });
    }
    this.setState({
      cloneControlledTarget0: target
    });
  };
  deleteMe = (val, group = 1) => {
    //delete state or children state
    if (Number.isNaN(group)) {
      group = 2;
    }
    // console.log(group);
    if (group == 2) {
      this.setState({
        cloneControlledTarget1: this.state.cloneControlledTarget1.filter(
          function(tool) {
            return tool.id.toString() !== val.id.toString();
          }
        )
      });
    } else if (group == 3) {
      this.setState({
        cloneControlledTarget2: this.state.cloneControlledTarget2.filter(
          function(tool) {
            return tool.id.toString() !== val.id.toString();
          }
        )
      });
    } else if (group == 4) {
      this.setState({
        cloneControlledTarget3: this.state.cloneControlledTarget3.filter(
          function(tool) {
            return tool.id.toString() !== val.id.toString();
          }
        )
      });
    } else if (group == 0) {
      this.setState({
        cloneControlledTarget0: this.state.cloneControlledTarget0.filter(
          function(tool) {
            return tool.id.toString() !== val.id.toString();
          }
        )
      });
    } else {
      this.setState({
        cloneControlledTarget: this.state.cloneControlledTarget.filter(function(
          tool
        ) {
          return tool.id.toString() !== val.id.toString();
        })
      });
    }
    if (val.value === "Question Group") this.setThisState(val.targetId, []); //remove children in the other state
    if (val.value === "Product") this.setThisState(0, []); //remove children in the other state
    if (val.value === "Product") this.setState({ isDraggable: true }); //activate the tool again

    // var elem = document.getElementById(val.id);
    // elem.parentNode.removeChild(elem);
    // console.log(this.state.cloneControlledTarget);
  };
  editMe = (val, group = 1) => {
    //PUT CONTENTS TO THE MODAL
    // console.log(val);
    let sets = [];
    let contents = [];
    let c = val.hasOwnProperty("tool") ? val.tool : [];
    let rq = val.hasOwnProperty("tool")
      ? val.tool.hasOwnProperty("required_settings")
        ? val.tool.required_settings
        : ""
      : "";
    let mc = val.hasOwnProperty("tool")
      ? val.tool.hasOwnProperty("allow_multiple_selection_settings")
        ? val.tool.allow_multiple_selection_settings
        : ""
      : "";
    let bc = val.hasOwnProperty("tool")
      ? val.tool.hasOwnProperty("enable_barcode_settings")
        ? val.tool.enable_barcode_settings
        : ""
      : "";
    let ib = val.hasOwnProperty("tool")
      ? val.tool.hasOwnProperty("enable_incremental_buttons_settings")
        ? {
            ib: val.tool.enable_incremental_buttons_settings,
            iv: val.tool.inputValueLimit
          }
        : { iv: 1000 }
      : { iv: 1000 };

    switch (val.value) {
      case "Question Group":
        this.setState({ toolType: "Question Group" });
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        contents.push(this.formGroupContent(c));
        this.setState({ modalContent: contents });
        this.setState({ modalContentSettings: "" });
        break;
      case "Text":
        contents.push([<input type="hidden" name="id" value={val.id} />]);
        this.setState({ toolType: "Text" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        let nc = val.hasOwnProperty("tool")
          ? val.tool.hasOwnProperty("is_numeric_only_text_settings")
            ? val.tool.is_numeric_only_text_settings
            : ""
          : "";
        sets.push(this.numericContent(nc));
        sets.push(this.requiredContent(rq));
        sets.push(this.barcodeContent(bc));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Multiline":
        contents.push([<input type="hidden" name="id" value={val.id} />]);
        this.setState({ toolType: "Multiline" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Yes No":
        contents.push([<input type="hidden" name="id" value={val.id} />]);
        this.setState({ toolType: "Yes/No" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        this.setState({ modalContentSettings: "" });
        break;
      case "Rating":
        contents.push([<input type="hidden" name="id" value={val.id} />]);
        this.setState({ toolType: "Rating" });
        sets.push(this.requiredContent(rq));
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        this.setState({ modalContentSettings: sets });
        break;
      case "Drop Down":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Drop Down" });
        contents.push(this.formItemContent(c));
        contents.push(this.choicesContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Multiple Choice":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Multiple Choice" });
        contents.push(this.formItemContent(c));
        contents.push(this.multiplechoicesContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        sets.push(this.allMultipleContent(mc));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Number":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Number" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        sets.push(this.allMultipleContent(mc));
        sets.push(this.showIncrementalButtonsContent(ib));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Date":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Date" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Email":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Email" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Time":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Time" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.requiredContent(rq));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Product":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Product" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.productsContent(c));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "Formula":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "Formula" });
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        sets.push(this.productsContent(c));
        this.setState({
          modalContentSettings: sets
        });
        break;
      case "UOM":
        contents.push([
          <input type="hidden" name="id" value={val.id} defaultValue={val.id} />
        ]);
        this.setState({ toolType: "UOM" });
        sets.push(this.requiredContent(rq));
        contents.push(this.formItemContent(c));
        this.setState({ modalContent: contents });
        this.setState({ modalContentSettings: sets });
        break;
      default:
        this.setState({ modalContent: <p>Form not set.</p> });
        this.setState({ modalContentSettings: "" });
    }
    //console.log(this.state.modalContentSettings);
    contents.push([<input type="hidden" name="value" value={val.value} />]);
    contents.push([<input type="hidden" name="type" value={val.value} />]);
    contents.push([<input type="hidden" name="group" value={group} />]);
    this.onOpenModal(1);
    //print the data to modal
  };
  addRoleHandler = () => {
    this.setState({ modalContent: <p>Form not set.</p> });
    this.onOpenModal(2);
    //print the data to modal
  };
  delRole = u => {
    this.setState({
      roles: this.state.roles.map(s =>
        s.text === u.text ? { text: s.text, checked: false } : s
      )
    });
  };
  groupHandler = event => {
    this.setState({ groupname: event.target.value });
  };
  addState = evt => {
    //let myUnique = uniqueId();
    let toolz = [];
    let myId = {
      value: evt.item.getAttribute("data-id"),
      id: evt.item.getAttribute("data-key"),
      tool: evt.item.getAttribute("data-tool")
    };
    //evt.item.setAttribute("data-key", myUnique); //add a unique ID on add to LI
    toolz.push(myId);
    this.setState({
      cloneControlledTarget: [...this.state.cloneControlledTarget, ...toolz]
    });
    this.syncState(evt.to);
  };

  syncState = evt => {
    let toolz = [];
    let lis = evt.getElementsByTagName("li");
    for (var i = 0; i < lis.length; i++) {
      toolz.push({
        value: lis[i].getAttribute("data-id"),
        id: lis[i].getAttribute("data-key"),
        tool: lis[i].getAttribute("data-tool")
      });
    }
    // evt.item.setAttribute("data-id", JSON.stringify(myId)); //add a unique ID on add to LI
    this.setState({ cloneControlledTarget: toolz });
  };
  clearForm = () => {
    this.setState({ cloneControlledTarget: [], cloneControlledTarget0: [] });
    this.setState({ cloneControlledTarget1: [] });
    this.setState({ cloneControlledTarget2: [] });
    this.setState({ cloneControlledTarget3: [] });
  };
  handleShareholderNameChange = idx => evt => {
    const newchosenname = this.state.chosenname.map((name, sidx) => {
      if (idx !== sidx) return name;
      return { ...name, name: evt.target.value };
    });

    this.setState({ chosenname: newchosenname });
  };

  handleAddShareholder = () => {
    const lastItem = document.getElementById("multiple_choice_choices")
      .lastChild.firstElementChild.value; //get value of last item

    if (lastItem !== "") {
      this.setState({
        chosenname: this.state.chosenname.concat([{ name: "" }])
      });
    }
    //console.log(this.state.chosenname);
  };

  handleRemoveShareholder = idx => () => {
    this.setState({
      chosenname: this.state.chosenname.filter((s, sidx) => idx !== sidx)
    });
  };
  closeEdit = () => {
    //when cancel is clicked on modal, delete from state if empty
    const formData = new FormData(document.querySelector("form"));
    let formSend = Object.fromEntries(formData);
    formSend["value"] = formSend.question;
    let groupId = formSend.group;
    if (formSend.question.isNullOrWhiteSpace()) {
      //if form empty
      this.deleteMe(formSend, groupId);
      if (formSend.type === "Product") this.setState({ isDraggable: true });
    }
    // console.log(formSend);
    this.onCloseModal(1);
  };
  submitForm = () => {
    console.log(this.state.cloneControlledTarget);
    console.log(this.state.cloneControlledTarget1);
    console.log(this.state.cloneControlledTarget2);
    console.log(this.state.cloneControlledTarget3);
    console.log(this.state.cloneControlledTarget0);
  };
  render() {
    const { open } = this.state;
    const { openb } = this.state;

    return (
      <div>
        <div>
          <div className="page-header col-xs-12">
            <div className="pull-left">
              <h1>New Form</h1>
            </div>
          </div>

          <div className="col-sm-9">
            <div className="form-group">
              <label>
                Form Name: <span className="required">*</span>
              </label>{" "}
              <input
                id="form_name"
                name="form_name"
                type="text"
                className="form-control"
                maxLength="50"
              />
            </div>
            <div className="form-group col-sm-12">
              <label className="pull-left" style={{ paddingRight: "5px" }}>
                Roles:<span className="required">*</span>
              </label>
              <button
                type="button"
                id="addRoles"
                className="btn btn-sm btn-success margin-bottom-10 pull-left"
                onClick={this.addRoleHandler}
              >
                Add Role
              </button>
            </div>

            <div id="add_roles_target" className="list-group col-sm-6">
              <ul>
                {this.state.roles.map(e => {
                  return e.checked ? (
                    <a
                      className="list-group-item"
                      style={{
                        borderBottom: "none",
                        borderLeft: "none",
                        borderRight: "none"
                      }}
                    >
                      <label>
                        <small>{e.text}</small>
                      </label>
                      <i
                        class="ace-icon fa fa-times bigger-150 text-danger pull-right pointer"
                        aria-hidden="true"
                        onClick={() => this.delRole(e)}
                      ></i>
                    </a>
                  ) : null;
                })}
              </ul>
            </div>
            <div
              className="hr hr-double dotted"
              style={{ clear: "both" }}
            ></div>

            <label>
              Settings: <span className="required"></span>
            </label>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="enable_send_to_email"
                  name="enable_send_to_email"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />
                <span className="lbl bigger-120">
                  {" "}
                  Send Result to Sender's Email
                </span>
              </label>
            </div>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="enable_photo"
                  name="enable_photo"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />{" "}
                <span className="lbl bigger-120"> Enable Photo Taking</span>
              </label>
            </div>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="enable_signature"
                  name="enable_signature"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />
                <span className="lbl bigger-120"> Enable Signature</span>
              </label>
            </div>
            <div className="checkbox">
              <label className="block" style={{ paddingLeft: "10px" }}>
                {" "}
                <input
                  id="is_form_required"
                  name="is_form_required"
                  type="checkbox"
                  className="ace input-lg ace-checkbox-2"
                />
                <span className="lbl bigger-120"> Required</span>
              </label>
            </div>
            <div className="widget-box">
              <div className="widget-header">
                <h4>Form Items</h4>
              </div>
              <div className="widget-body" style={{ padding: "5px" }}>
                <i>(Drop form tools here.) </i>
                <ReactSortable
                  className="block-list"
                  // draggable=".draggables"
                  list={this.state.cloneControlledTarget}
                  setList={newState => {
                    this.setState({
                      cloneControlledTarget: newState
                    });
                  }}
                  animation={150}
                  group={{ name: "clone2", pull: "clone" }}
                  clone={item => ({
                    ...item,
                    id: item.id++
                  })}
                  onAdd={e => {
                    //   //   let oldId = e.to.firstChild.getAttribute("data-id");
                    if (e.item.innerText === "Product") {
                      //disabled Product if used
                      this.setState({ isDraggable: false });
                    }
                    this.editMe(e.item.dataset); //show modal on Add
                  }}
                  // onEnd={e => console.log(e)}
                  // setData={e => console.log(e)}
                >
                  {this.state.cloneControlledTarget.map(item => {
                    //IF TOOL IS GROUP, SORTABLE DIV AGAIN
                    if (item.value === "Question Group") {
                      return (
                        <div
                          key={item.id}
                          data-value={item.value}
                          data-all={JSON.stringify(item)}
                        >
                          <i className="fa fa-question-circle"></i>
                          {"   "}
                          {item.hasOwnProperty("tool")
                            ? item.tool.question
                            : item.value}
                          <i
                            className="ace-icon red fa fa-trash-o bigger-130 pull-right pointer"
                            onClick={() => this.deleteMe(item)}
                          ></i>
                          <i
                            className="ace-icon blue fa fa-pencil bigger-130 pull-right editMe pointer"
                            onClick={() => this.editMe(item)}
                          ></i>{" "}
                          <ReactSortable
                            style={{
                              marginTop: "5px",
                              minHeight: "10px",
                              border: "0px dashed #EAEAEA"
                            }}
                            className="block-list"
                            list={this.getChosenState(item.targetId)}
                            setList={newState => {
                              //ADD A NEW PRARENT ID
                              // newState.forEach(e => {
                              //   if (!e.hasOwnProperty("parent"))
                              //     e["parent"] = item.id;
                              // });
                              this.setThisState(item.targetId, newState);
                            }}
                            animation={150}
                            group={{ name: "clone2", pull: false }}
                            clone={item => ({
                              ...item,
                              id: nextId()
                            })}
                            onAdd={e => {
                              if (e.item.innerText === "Product") {
                                //disabled product if used
                                this.setState({ isDraggable: false });
                              }
                              this.editMe(e.item.dataset, item.targetId + 1); //show modal on Add
                            }}
                          >
                            {this.getGroup(item.targetId)}
                          </ReactSortable>
                        </div>
                      );
                    }
                    if (item.value === "Product") {
                      return (
                        <div
                          key={item.id}
                          data-value={item.value}
                          data-all={JSON.stringify(item)}
                        >
                          <i className="fa fa-question-circle"></i>
                          {"   "}
                          {item.hasOwnProperty("tool")
                            ? item.tool.question
                            : item.value}
                          <i
                            className="ace-icon red fa fa-trash-o bigger-130 pull-right pointer"
                            onClick={() => this.deleteMe(item)}
                          ></i>
                          <i
                            className="ace-icon blue fa fa-pencil bigger-130 pull-right editMe pointer"
                            onClick={() => this.editMe(item)}
                          ></i>{" "}
                          <ReactSortable
                            style={{
                              marginTop: "5px",
                              minHeight: "10px",
                              border: "0px dashed #EAEAEA"
                            }}
                            className="block-list"
                            list={this.getChosenState(0)}
                            setList={newState => {
                              //ADD A NEW PRARENT ID
                              // newState.forEach(e => {
                              //   if (!e.hasOwnProperty("parent"))
                              //     e["parent"] = item.id;
                              // });
                              this.setThisState(0, newState);
                            }}
                            animation={150}
                            group={{ name: "clone2", pull: false }}
                            clone={item => ({
                              ...item,
                              id: nextId()
                            })}
                            onAdd={e => {
                              if (e.item.innerText === "Product") {
                                //disabled product if used
                                this.setState({ isDraggable: false });
                              }
                              this.editMe(e.item.dataset, 0); //show modal on Add
                            }}
                          >
                            {this.getGroup(0)}
                          </ReactSortable>
                        </div>
                      );
                    } else {
                      return (
                        //IF NOT QUESTION GROUP/PRODUCT PRINT REGULAR TOOL
                        <div
                          key={item.id}
                          data-value={item.value}
                          data-all={JSON.stringify(item)}
                        >
                          <i
                            className={this.getIcon(item.value)}
                            style={{ paddingRight: "10px" }}
                          ></i>
                          {item.hasOwnProperty("tool")
                            ? item.tool.question
                            : item.value}{" "}
                          <i
                            className="ace-icon red fa fa-trash-o bigger-130 pull-right pointer"
                            onClick={() => this.deleteMe(item)}
                          ></i>
                          <i
                            className="ace-icon blue fa fa-pencil bigger-130 pull-right editMe pointer"
                            onClick={() => this.editMe(item)}
                          ></i>
                          {/* {
                            <div
                              style={{ display: "block", paddingLeft: "10px" }}
                            >
                              test
                            </div>
                          } */}
                          {item.hasOwnProperty("tool") ? (
                            <div
                              style={{
                                display: "block",
                                paddingLeft: "10px",
                                fontSize: "10px"
                              }}
                            >
                              <ol type="A">
                                {Object.keys(item.tool).map((k, i) => {
                                  if (k.includes("choices_")) {
                                    return <li>{item.tool[k]}</li>;
                                  } else if (
                                    k.includes("drop_down_choice_textarea")
                                  ) {
                                    let ch = item.tool[k].split("\r\n");
                                    Object.keys(ch).map(key => {
                                      return ch[key];
                                    });
                                    // for (i = 0; i < ch.length; i++) {
                                    //   return <li>{ch[i]}</li>;
                                    // }
                                  }
                                })}
                              </ol>
                            </div>
                          ) : (
                            " "
                          )}
                        </div>
                      );
                    }
                  })}
                </ReactSortable>
              </div>
            </div>
          </div>

          <div className="row">
            <StickyBox offsetTop={10} offsetBottom={10}>
              <div className="col-sm-3">
                <div className="widget-box" id="tools_stick">
                  <div className="widget-header">
                    <h5 className="widget-title smaller">Tools</h5>
                  </div>
                  <div className="widget-body">
                    <ReactSortable
                      animation={150}
                      group={{ name: "clone2", pull: "clone", put: false }}
                      sort={false}
                      draggable=".draggables"
                      className="block-list"
                      style={{ border: "none", fontSize: "13px" }}
                      list={this.state.cloneControlledSource}
                      setList={newState =>
                        this.setState({ cloneControlledSource: newState })
                      }
                      clone={item => ({
                        ...item,
                        id: nextId(),
                        targetId: this.stateChooser(item)
                      })}
                    >
                      {this.state.cloneControlledSource.map(item => (
                        <div
                          key={item.id}
                          data-value={item.value}
                          data-all={JSON.stringify(item)}
                          className={
                            item.value === "Product" && !this.state.isDraggable
                              ? "disabled"
                              : "draggables"
                          }
                        >
                          <i
                            className={this.getIcon(item.value)}
                            style={{ paddingRight: "10px" }}
                          ></i>
                          {item.value.length > 7 ? (
                            <small>{item.value}</small>
                          ) : (
                            item.value
                          )}
                        </div>
                      ))}
                    </ReactSortable>
                  </div>
                </div>
              </div>
            </StickyBox>
          </div>
          <div id="actions_btns" className="clearfix form-actions">
            <div className="col-md-9">
              <button
                id="btn_save"
                className="btn btn-info margin-bottom-10"
                type="button"
                onClick={this.submitForm}
              >
                <i className="ace-icon fa fa-floppy-o bigger-110"></i>Save
              </button>
              &nbsp; &nbsp; &nbsp;
              <button
                id="btn_save_and_activate"
                className="btn btn-success margin-bottom-10"
                type="button"
              >
                <i className="ace-icon fa fa-check-o bigger-110"></i>Save and
                Activate
              </button>
              &nbsp; &nbsp; &nbsp;
              <button
                id="btn_clear"
                className="btn btn-secondary margin-bottom-10"
                type="button"
                onClick={() => {
                  this.clearForm();
                }}
              >
                <i className="ace-icon fa fa-floppy-o bigger-110"></i>Clear
              </button>
              &nbsp; &nbsp; &nbsp;
              <Link
                to="/forms"
                id="reset"
                className="btn btn-danger margin-bottom-10"
              >
                <i className="ace-icon fa fa-times"></i> Cancel
              </Link>
            </div>
          </div>
        </div>
        <Modal
          open={open}
          closeOnEsc={false}
          closeOnOverlayClick={true}
          showCloseIcon={false}
          onClose={this.closeEdit}
          closeOnOverlayClick={false}
          key={nextId()}
        >
          {/* <Forms
            onCloseModal={this.onCloseModal}
            modalContentSettings={this.modalContentSettings}
            toolType={this.state.toolType}
            setTool={this.setTool}
            groupName={this.state.toolType}
          /> */}
          <div className="modal-dialog">
            <form
              id="add_survey_form"
              method="get"
              data-validate="parsley"
              style={{ minWidth: "350px" }}
              onSubmit={this.formSubmit}
            >
              <fieldset>
                <div className="modal-header">
                  <span>
                    <h4 className="modal-title" id="myModalLabel">
                      Form Item Details
                    </h4>
                    (
                    <label id="question_type_label">
                      Tool Type : {this.state.toolType}
                    </label>
                    )
                  </span>
                  <br />
                </div>
                <div className="modal-body" key={nextId()}>
                  {this.state.modalContent}
                </div>
              </fieldset>

              {this.state.modalContentSettings !== "" ? (
                <div className="control-group" id="settings_div">
                  <label className="control-label">Settings</label>
                  <div className="controls form-group">
                    {this.state.modalContentSettings}
                  </div>
                </div>
              ) : (
                " "
              )}
              <div className="modal-footer">
                <button
                  id="close_question_dialog"
                  type="button"
                  name="close"
                  value="close"
                  className="btn btn-default"
                  data-dismiss="modal"
                  onClick={this.closeEdit}
                >
                  Cancel
                </button>
                <button
                  id="submit_question_dialog"
                  type="submit"
                  name="edit"
                  value="edit"
                  className="btn btn-primary"
                >
                  Done
                </button>
              </div>
            </form>
          </div>
        </Modal>
        <Modal
          open={openb}
          closeOnEsc={false}
          closeOnOverlayClick={true}
          showCloseIcon={false}
          onClose={() => this.onCloseModal()}
          className="modal-dialog"
          key={nextId()}
        >
          <div className="modal-header">
            <h4 className="blue bigger">Choose Roles</h4>
          </div>

          <div className="modal-body" style={{ minWidth: "400px" }}>
            <div className="row">
              <div className="dropdown-container">
                <div className="dropdown-button noselect">
                  <div className="dropdown-label">Role</div>
                  <i className="fa fa-filter"></i>
                </div>
                <div className="dropdown-list">
                  <ul>
                    {this.state.roles.map((item, i) => (
                      <li key={i}>
                        <label>
                          <input
                            className="list-item getRoles"
                            type="checkbox"
                            onClick={this.onToggle.bind(this, i)}
                            // checked={item.checked}
                          />
                          {item.text}
                        </label>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
            <div className="modal-footer" style={{ marginTop: "10px" }}>
              <div>
                <div className="space-10"></div>
                <a
                  onClick={this.onCloseModal}
                  id="submit_roles"
                  className="btn btn-sm btn-success"
                >
                  <i className="ace-icon fa fa-check"></i> Done
                </a>
              </div>{" "}
            </div>
          </div>
        </Modal>
      </div>
    );
  }

  formGroupContent = e => (
    <div id="div_question_details" className="control-group" key={nextId()}>
      <label id="question_label" className="control-label">
        Form Group Name
      </label>
      <span id="question_count" className="pull-right">
        0 of 500
      </span>
      <div className="controls form-group">
        <textarea
          id="question_g"
          className="form-control"
          name="question"
          rows="3"
          required="required"
          maxLength="500"
          // onBlur={e => this.groupHandler(e)}
          defaultValue={e.question}
        ></textarea>
      </div>{" "}
    </div>
  );
  formItemContent = e => (
    <div id="div_items_details" className="control-group" key={nextId()}>
      <label id="question_label" className="control-label">
        Form Item
      </label>
      <span id="question_count" className="pull-right">
        0 of 500
      </span>
      <div className="controls form-group">
        <textarea
          id="question"
          className="form-control"
          name="question"
          rows="3"
          required="required"
          maxLength="500"
          defaultValue={e.question}
        ></textarea>
      </div>
    </div>
  );
  choicesContent = w => (
    <div id="div_question_details" className="control-group" key={nextId()}>
      <label id="question_label" className="control-label">
        Choices
      </label>
      <label>
        <i>(Add one choice per line)</i>
      </label>
      <div className="controls form-group">
        <textarea
          id="drop_down_choice_textarea"
          className="form-control"
          name="drop_down_choice_textarea"
          rows="5"
          defaultValue={
            w.hasOwnProperty("drop_down_choice_textarea")
              ? w.drop_down_choice_textarea
              : ""
          }
        ></textarea>
      </div>
    </div>
  );

  multiplechoicesContent = w => (
    <div className="control-group" id="multiple_choice_div" key={nextId()}>
      <div className="controls form-group">
        <ol
          id="multiple_choice_choices"
          className="list-type-alpha question-choices"
        >
          <li className="question-choice-list">
            <input
              className="question-choice"
              type="text"
              maxlength="500"
              required="true"
              name="choices_0"
              defaultValue={w.hasOwnProperty("choices_0") ? w.choices_0 : ""}
            />
            <button
              style={{ margin: "2px" }}
              className="btn btn-success btn-sm add-choice"
              onClick={this.addChoice}
            >
              <i className="fa fa-plus"></i>
            </button>
            <button
              style={{ margin: "2px" }}
              className="btn btn-danger btn-sm remove-choice"
              // onClick={this.subChoice.bind(this)}
            >
              <i className="fa fa-times"></i>
            </button>
          </li>
          {Object.keys(w).map((k, i) => {
            if (k.includes("choices_") && i !== 2) {
              return (
                <li className="question-choice-list">
                  <input
                    className="question-choice"
                    type="text"
                    maxlength="500"
                    required="true"
                    name={k}
                    value={w[k]}
                    defaultValue={w[k]}
                  />
                  <button
                    style={{ margin: "2px" }}
                    className="btn btn-success btn-sm add-choice"
                    onClick={this.addChoice}
                  >
                    <i className="fa fa-plus"></i>
                  </button>
                  <button
                    style={{ margin: "2px" }}
                    className="btn btn-danger btn-sm remove-choice"
                    onClick={this.subChoice.bind(this)}
                  >
                    <i className="fa fa-times"></i>
                  </button>
                </li>
              );
            }
          })}
        </ol>
      </div>
    </div>
  );
  runChoices = w => {
    let scripts = "";
    Object.keys(w).forEach((k, i) => {
      if (k.includes("choices_")) {
        scripts +=
          '<li class="question-choice-list"><input class="question-choice" type="text" maxlength="500" required="true" name=' +
          k +
          " value=" +
          w[k] +
          " defaultValue=" +
          w[k] +
          " />" +
          '<button style="margin:2px" class="btn btn-success btn-sm add-choice" onClick=' +
          this.addChoice +
          " >" +
          '<i class="fa fa-plus"></i></button>' +
          "</li>";
      }
    });
    return scripts;
  };
  numericContent = w => (
    <div className="checkbox" key={nextId()}>
      <label>
        <input
          id="is_numeric_only_text_settings"
          name="is_numeric_only_text_settings"
          className="ace ace-checkbox-2"
          type="checkbox"
          defaultChecked={w}
        />
        <span className="lbl"> Numeric Input Only </span>
      </label>
    </div>
  );
  barcodeContent = w => (
    <div className="checkbox" key={nextId()}>
      <label>
        <input
          id="enable_barcode_settings"
          name="enable_barcode_settings"
          className="ace ace-checkbox-2"
          type="checkbox"
          defaultChecked={w}
        />
        <span className="lbl"> Enable Barcode </span>
      </label>
    </div>
  );
  requiredContent = w => (
    <div className="checkbox" key={nextId()}>
      <label>
        <input
          id="required_settings"
          name="required_settings"
          className="ace ace-checkbox-2"
          type="checkbox"
          defaultChecked={w}
        />
        <span className="lbl"> Required </span>
      </label>
    </div>
  );
  allMultipleContent = w => (
    <div className="checkbox" key={nextId()}>
      <label>
        <input
          id="allow_multiple_selection_settings"
          name="allow_multiple_selection_settings"
          className="ace ace-checkbox-2"
          type="checkbox"
          defaultChecked={w}
        />
        <span className="lbl"> Allow multiple selection </span>
      </label>
    </div>
  );
  productsContent = w => (
    <div key={nextId()}>
      <div className="checkbox">
        <label>
          <input
            id="list_carried_products_settings"
            name="list_carried_products_settings"
            className="ace ace-checkbox-2"
            type="checkbox"
            defaultChecked={
              w.hasOwnProperty("list_carried_products_settings") ? "on" : ""
            }
          />
          <span className="lbl"> List Carried Products </span>
        </label>
      </div>
      <div className="checkbox">
        <label>
          <input
            id="enable_add_product_settings"
            name="enable_add_product_settings"
            className="ace ace-checkbox-2"
            type="checkbox"
            defaultChecked={
              w.hasOwnProperty("enable_add_product_settings") ? "on" : ""
            }
            onClick={e => this.productSettings(e)}
          />
          <span className="lbl"> Enable Add Product </span>
        </label>
      </div>
      <div
        id="div_enable_product_barcode"
        class="checkbox"
        style={{ paddingLeft: "2%" }}
      >
        <label>
          <input
            id="enable_product_barcode_settings"
            name="enable_product_barcode_settings"
            class="ace ace-checkbox-2 hide"
            type="checkbox"
            defaultChecked={
              w.hasOwnProperty("enable_product_barcode_settings") ? "on" : ""
            }
            onClick={e => this.productSettings(e)}
          />
          <span class="lbl"> Enable Barcode </span>
        </label>
      </div>
      <div className="checkbox">
        <label>
          <input
            id="enable_oum_settings"
            name="enable_oum_settings"
            className="ace ace-checkbox-2"
            type="checkbox"
            defaultChecked={w.hasOwnProperty("enable_oum_settings") ? "on" : ""}
            onClick={e => this.productSettings(e)}
          />
          <span className="lbl"> Enable OUM </span>
        </label>
      </div>
      <div id="div_show_price" class="checkbox" style={{ paddingLeft: "2%" }}>
        <label>
          <input
            id="show_price_settings"
            name="show_price_settings"
            class="ace ace-checkbox-2"
            type="checkbox"
            defaultChecked={w.hasOwnProperty("show_price_settings") ? "on" : ""}
            onClick={e => this.productSettings(e)}
          />
          <span class="lbl"> Display Price </span>
        </label>
      </div>
      <div
        id="div_enable_product_quantity"
        class="checkbox"
        style={{ paddingLeft: "3%" }}
      >
        <label>
          <input
            id="enable_product_quantity_selection_settings"
            name="enable_product_quantity_selection_settings"
            class="ace ace-checkbox-2"
            type="checkbox"
            defaultChecked={
              w.hasOwnProperty("enable_product_quantity_selection_settings")
                ? "on"
                : ""
            }
            onClick={e => this.productSettings(e)}
          />
          <span class="lbl"> Enable Quantity </span>
        </label>
      </div>
      <div
        id="div_enable_discount"
        class="checkbox"
        style={{ paddingLeft: "3%" }}
      >
        <label>
          <input
            id="enable_product_discount_selection_settings"
            name="enable_product_discount_selection_settings"
            class="ace ace-checkbox-2"
            type="checkbox"
            defaultChecked={
              w.hasOwnProperty("enable_product_discount_selection_settings")
                ? "on"
                : ""
            }
            onClick={e => this.productSettings(e)}
          />
          <span class="lbl"> Enable Discount </span>
        </label>
      </div>
    </div>
  );
  showIncrementalButtonsContent = w => (
    <div className="checkbox" key={nextId()}>
      <label>
        <input
          id="enable_incremental_buttons_settings"
          name="enable_incremental_buttons_settings"
          className="ace ace-checkbox-2"
          type="checkbox"
          defaultChecked={w.ib}
        />
        <span className="lbl"> Show Incremental Buttons </span>
        <div className="checkbox disabled" id="divRange">
          <label>Input Value Limit: </label>
        </div>
        <div className="ace-spinner middle touch-spinner">
          <div className="input-group">
            <input
              type="text"
              id="inputValueLimit"
              name="inputValueLimit"
              className="spinbox-input form-control text-center"
              // value="1000"
              defaultValue={w.iv}
              // onChange={event =>
              //   this.setState({
              //     inputValueLimit: event.target.value.replace(/\D/, "")
              //   })
              // }
            />
            <div className="spinbox-buttons input-group-btn">
              <button
                type="button"
                className="btn spinbox-up btn-sm btn-success"
                onClick={() => this.addHandler(1)}
              >
                <i className="ace-icon ace-icon fa fa-plus"></i>
              </button>
              <button
                type="button"
                className="btn spinbox-down btn-sm btn-danger"
                onClick={() => this.subHandler(1)}
              >
                <i className="ace-icon ace-icon fa fa-minus"></i>
              </button>
            </div>
          </div>
        </div>
      </label>
    </div>
  );
  productSettings = label => {
    let inputs = label.currentTarget;
    let cbox = inputs.getAttribute("id");
    if (cbox === "enable_oum_settings") {
      //make all of them OUM  settings false
      if (inputs.checked !== true) {
        document.getElementById("show_price_settings").checked = false;
        document.getElementById(
          "enable_product_quantity_selection_settings"
        ).checked = false;
        document.getElementById(
          "enable_product_discount_selection_settings"
        ).checked = false;
      }
    } else if (cbox === "show_price_settings") {
      if (inputs.checked === true) {
        document.getElementById("enable_oum_settings").checked = true;
      } else {
        document.getElementById(
          "enable_product_quantity_selection_settings"
        ).checked = false;
        document.getElementById(
          "enable_product_discount_selection_settings"
        ).checked = false;
      }
    } else if (cbox === "enable_product_quantity_selection_settings") {
      if (inputs.checked === true) {
        document.getElementById("show_price_settings").checked = true;
        document.getElementById("enable_oum_settings").checked = true;
      }
    } else if (cbox === "enable_product_discount_selection_settings") {
      if (inputs.checked === true) {
        document.getElementById("show_price_settings").checked = true;
        document.getElementById("enable_oum_settings").checked = true;
      }
    } else if (cbox === "enable_add_product_settings") {
      if (inputs.checked === false) {
        document.getElementById(
          "enable_product_barcode_settings"
        ).checked = false;
      }
    } else if (cbox === "enable_product_barcode_settings") {
      if (inputs.checked === true) {
        document.getElementById("enable_add_product_settings").checked = true;
      }
    }
  };
  addChoice = () => {
    let DOM = document.getElementById("multiple_choice_choices");
    let DOMcount = DOM.childElementCount;
    let DOMlastChild = DOM.lastChild;
    if (DOMlastChild.firstChild.value !== "") {
      let li = document.createElement("li");
      li.setAttribute("class", "question-choice-list");
      li.style.cssText = "padding-top: 3px;";
      let input = document.createElement("input");
      input.setAttribute("class", "question-choice");
      input.setAttribute("type", "text");
      input.setAttribute("required", "true");
      input.setAttribute("maxLength", "500");
      input.setAttribute("name", "choices_" + DOMcount);
      let btnAdd = document.createElement("button");
      btnAdd.setAttribute("class", "btn btn-success btn-sm add-choice");
      btnAdd.style.cssText = "margin: 2px;";
      let iconAdd = document.createElement("i");
      iconAdd.setAttribute("class", "fa fa-plus");
      btnAdd.appendChild(iconAdd);
      btnAdd.onclick = () => this.addChoice();
      let btnSub = document.createElement("button");
      btnSub.setAttribute("class", "btn btn-danger btn-sm add-choice");
      btnSub.style.cssText = "margin: 2px;";
      btnSub.onclick = e => this.subChoice(e);
      let iconSub = document.createElement("i");
      iconSub.setAttribute("class", "fa fa-times");
      btnSub.appendChild(iconSub);
      // btnSub.onclick = () => this.subChoice();
      li.appendChild(input);
      li.appendChild(btnAdd);
      li.appendChild(btnSub);
      DOM.appendChild(li);
    }
    // console.log(DOMlastChild.firstChild.value);
  };
  subChoice = f => {
    f.preventDefault();
    let OL = document.getElementById("multiple_choice_choices");
    let DOMcount = OL.childElementCount;
    let DOM = f.currentTarget.closest("li");
    // let DOM = e.srcElement.closest("li");
    if (DOMcount > 1) DOM.remove();
  };
  assignIdToTarget = (itemId, newId) => {
    let cleanState = this.state.cloneControlledTarget.filter(
      e => e.id !== itemId
    );
    let newState = this.state.cloneControlledTarget.filter(
      e => e.id === itemId
    );
    newState.push({ targetId: newId });
    cleanState.push(newState);
    this.setState({ cloneControlledTarget: cleanState });
  };
  stateChooser = item => {
    //this chooses the correct state target number
    var s = 2; //start at 2, as 1 is already default
    var marker = 2;
    if (item.value === "Product") {
      //set target ID
      marker = 0;
      s = 5;
    }
    while (s <= 4) {
      //search for existing ID
      if (
        this.state.cloneControlledTarget.filter(x => x.targetId === s).length >
        0
      ) {
        s++; //may laman
      } else {
        //not found
        marker = s;
        s = 5;
      }
    }
    return marker;
  };
  setThisState = (val, newState) => {
    //sets apropriate state
    if (val === 2) {
      this.setState({
        cloneControlledTarget2: newState
      });
    } else if (val === 3) {
      this.setState({
        cloneControlledTarget3: newState
      });
    } else if (val === 4) {
      this.setState({
        cloneControlledTarget4: newState
      });
    } else if (val === 0) {
      this.setState({
        cloneControlledTarget0: newState
      });
    } else {
      this.setState({
        cloneControlledTarget1: newState
      });
    }
  };
  getChosenState = val => {
    //return asked state
    let chosenState = [];
    if (val === 2) {
      chosenState = this.state.cloneControlledTarget2;
    } else if (val === 3) {
      chosenState = this.state.cloneControlledTarget3;
    } else if (val === 4) {
      chosenState = this.state.cloneControlledTarget4;
    } else if (val === 0) {
      chosenState = this.state.cloneControlledTarget0;
    } else {
      chosenState = this.state.cloneControlledTarget1;
    }
    return chosenState;
  };
  getGroup = val => {
    //Shows the correct Question Group/product group
    let chosenState = this.getChosenState(val);
    if (val == 0) val = -1; //for PRODUCT
    return chosenState.map(items => {
      return (
        <div key={items.id} data-all={JSON.stringify(items)}>
          <i
            className={this.getIcon(items.value)}
            style={{ paddingRight: "10px" }}
          ></i>
          {items.hasOwnProperty("tool") ? items.tool.question : items.value}
          {items.value !== "UOM" &&
          items.value !== "QUANTITY" &&
          items.value !== "DISCOUNT" ? (
            <i
              className="ace-icon red fa fa-trash-o bigger-130 pull-right pointer"
              onClick={() => this.deleteMe(items, val + 1)}
            ></i>
          ) : (
            " "
          )}

          <i
            className="ace-icon blue fa fa-pencil bigger-130 pull-right editMe pointer"
            onClick={() => this.editMe(items, val + 1)}
          ></i>
        </div>
      );
    });
  };
  addHandler = val => {
    document.getElementById("inputValueLimit").value++;
    // let a = parseInt(this.state.inputValueLimit) + val;
    // this.setState({ inputValueLimit: a });
  };
  subHandler = val => {
    // let a = this.state.inputValueLimit - val;
    // if (!(a < 1)) this.setState({ inputValueLimit: a });
    document.getElementById("inputValueLimit").value--;
  };

  getIcon = value => {
    switch (value) {
      case "Question Group":
        return "fa fa-question-circle";
        break;
      case "Text":
        return "fa fa-font";
        break;
      case "Multiline":
        return "fa fa-align-justify";
        break;
      case "Yes No":
        return "fa fa-dot-circle-o";
        break;
      case "Rating":
        return "fa fa-star-o";
        break;
      case "Drop Down":
        return "fa fa-caret-down";
        break;
      case "Multiple Choice":
        return "fa fa-th-large";
        break;
      case "Number":
        return "fa fa-hashtag";
        break;
      case "Date":
        return "fa fa-calendar";
        break;
      case "Email":
        return "fa fa-envelope-o";
        break;
      case "Time":
        return "fa fa-clock-o";
        break;
      case "Product":
        return "fa fa-product-hunt";
        break;
      case "Formula":
        return "fa fa-calculator";
        break;
      case "UOM":
        return "fa fa-caret-down";
        break;
      case "QUANTITY":
        return "fa fa-hashtag";
        break;
      case "DISCOUNT":
        return "fa fa-th-large";
        break;
      default:
        return "fa fa-tasks";
    }
  };
}
String.prototype.isNullOrWhiteSpace = function() {
  return !this || this.length === 0 || /^\s*$/.test(this);
};
export default FormSort;
