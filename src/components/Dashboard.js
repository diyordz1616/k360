import React, { Component } from "react";
import DateRangePicker from "react-bootstrap-daterangepicker";
import "bootstrap3/dist/css/bootstrap.css";
import "bootstrap-daterangepicker/daterangepicker.css";
import moment from "moment";
import PieChart from "react-minimal-pie-chart";
import axios from "axios";
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

class Dashboard extends Component {
  state = {
    date: "Jan 2019",
    activeUsers: 2,
    accountVisited: 4,
    ranges: {
      Today: [moment(), moment()],
      Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
      "This Month": [moment().startOf("month"), moment().endOf("month")],
      "Last Month": [
        moment()
          .subtract(1, "month")
          .startOf("month"),
        moment()
          .subtract(1, "month")
          .endOf("month")
      ]
    }
  };

  onChange = date => this.setState({ date: date });
  handlePickerSubmit = (event, picker) => {
    const momentString =
      picker.startDate.format("MMM D, YYYY") ===
      picker.endDate.format("MMM D, YYYY")
        ? picker.startDate.format("MMM D, YYYY")
        : picker.startDate.format("MMM D, YYYY") +
          " to " +
          picker.endDate.format("MMM D, YYYY");
    this.setState({ date: momentString });
    this.retrieveData();
  };
  retrieveData = () => {
    let aU = this.generateRand() + 1;
    let aV = this.generateRand() + 4;

    this.setState({ activeUsers: aU });
    this.setState({ accountVisited: aV });
    let num = this.generateRand();
    axios.get("https://reqres.in/api/users?page=" + num).then(response => {
      this.setState({ reportPromise: response.data });
    });
  };
  generateRand = () => {
    return Math.floor(Math.random() * (3 - 1 + 1)) + 1;
  };
  per = (num, amount) => {
    return (100 * num) / amount;
  };
  render() {
    return (
      <div className="row">
        <div className="col-xs-12">
          <div className="col-xs-12 col-md-12 col-sm-9 align-right">
            <div>
              <div className="row">
                <div className="col-xs-12">
                  <div className="col-sm-5 pull-right">
                    <DateRangePicker
                      ranges={this.state.ranges}
                      startDate="1/1/2014"
                      endDate="3/1/2014"
                      onApply={this.handlePickerSubmit}
                    >
                      <div
                        id="reportrange"
                        className="pull-right"
                        style={{
                          color: "black",
                          backGround: " #fff",
                          cursor: "pointer",
                          padding: " 4px 10px",
                          border: " 1px solid #ccc",
                          width: "100%"
                        }}
                      >
                        <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span> {this.state.date}</span>{" "}
                        <b className="caret"></b>
                      </div>
                    </DateRangePicker>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 infobox-container">
              <div className="space-12"></div>
              <div className="infobox infobox-blue2">
                <div className="infobox-progress">
                  <PieChart
                    animate={true}
                    animationDuration={500}
                    animationEasing="ease-out"
                    data={[
                      {
                        color: "#3983C2",
                        value: Math.round(this.per(this.state.activeUsers, 15))
                      }
                    ]}
                    label
                    labelPosition={0}
                    labelStyle={{
                      fontFamily: "sans-serif",
                      fontSize: "17px"
                    }}
                    lengthAngle={360}
                    lineWidth={17}
                    onClick={undefined}
                    onMouseOut={undefined}
                    onMouseOver={undefined}
                    paddingAngle={0}
                    radius={45}
                    ratio={1}
                    rounded={false}
                    startAngle={0}
                    totalValue={100}
                    style={{
                      height: "75px"
                    }}
                    background="#dddddd"
                  />
                  <span className="percent">0</span>%
                  <canvas height="70" width="70"></canvas>
                </div>
                <div className="infobox-data">
                  <span className="infobox-text"> Active Users</span>

                  <div className="infobox-content">
                    {this.state.activeUsers} of 15
                  </div>
                </div>
              </div>
              <div className="infobox infobox-blue2">
                <div className="infobox-progress">
                  <PieChart
                    animate={true}
                    animationDuration={1000}
                    animationEasing="ease-out"
                    data={[
                      {
                        color: "#3983C2",
                        value: Math.round(this.per(this.state.activeUsers, 7))
                      }
                    ]}
                    label
                    labelPosition={0}
                    labelStyle={{
                      fontFamily: "sans-serif",
                      fontSize: "17px"
                    }}
                    lengthAngle={360}
                    lineWidth={17}
                    onClick={undefined}
                    onMouseOut={undefined}
                    onMouseOver={undefined}
                    paddingAngle={0}
                    radius={45}
                    ratio={1}
                    rounded={false}
                    startAngle={0}
                    totalValue={100}
                    style={{
                      height: "75px"
                    }}
                    background="#dddddd"
                  />
                  <span className="percent">0</span>%
                </div>
                <div className="infobox-data">
                  <span className="infobox-text">Accounts Visited</span>
                  <div className="infobox-content">
                    {this.state.accountVisited} of 7
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Dashboard;
