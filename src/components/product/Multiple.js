import React from "react";
import Modal from "react-responsive-modal";
import { Dropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

const styles = {
  fontFamily: "sans-serif"
};

class MultipleProducts extends React.Component {
  state = {
    open: false
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div style={styles}>
        <div class="row">
          <div class="page-header col-xs-12">
            <div class="pull-left">
              <h1>New Products : Multiple Insert</h1>
            </div>
          </div>
          <div id="div_main" class="col-xs-12">
            <div id="error_messages" class="well well-lg hide"></div>
            <form
              id="form_info"
              class="form-horizontal"
              action="/products/new/multiple/insert"
              method="post"
              enctype="multipart/form-data"
              novalidate="novalidate"
            >
              <div id="div_upload">
                <h3 class="lighter block green">
                  Create many products at once.{" "}
                </h3>
                <p>
                  You can choose to download the template in either Excel or csv
                  file format. You can easily fill in or edit the template using
                  Microsoft Excel, Google Sheets or other spreadsheet software.
                  <br />
                  <br />
                  <b>
                    If you are using the template in Excel file format, you need
                    to export it to csv before uploading the file.
                  </b>
                  <br />
                  <br />
                </p>
                <div style={{ fontStyle: "italic" }}>
                  Notes:
                  <ul>
                    <li>
                      Columns <b>Account</b>, and <b>Products</b> should not be
                      empty.
                    </li>
                    <li id="li_accounts">Account - is case sensitive</li>
                    <li id="li_branches">Branch - is case sensitive</li>
                    <li>
                      Contact Type - is case sensitive and only one should be
                      selected from&nbsp;
                      <b>Mobile</b>&nbsp;,&nbsp;
                      <b>Phone</b>&nbsp;and&nbsp;
                      <b>Other</b>
                      <br />
                    </li>
                    <li> This action will not delete any data.</li>
                  </ul>
                </div>
                <div class="space-20"></div>
                <div class="dropdown">
                  <Dropdown>
                    <Dropdown.Toggle
                      id="dropdown-basic"
                      bsPrefix="btn btn-link btn-sm dropdown-toggle"
                    >
                      Download Template <span class="caret"></span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item>
                        <Link to="/csv/download">CSV</Link>
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        <Link to="/excel/download">EXCEL</Link>
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                <div class="space-32"></div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="widget-box no-border">
                      <div class="widget-header">
                        <h4 class="widget-title">Custom File Input</h4>
                      </div>
                      <div class="widget-body">
                        <div class="widget-main">
                          <div class="form-group">
                            <div class="col-xs-12">
                              <label class="ace-file-input">
                                <input
                                  type="file"
                                  id="custom_file"
                                  name="custom_file"
                                />
                                <span
                                  class="ace-file-container"
                                  data-title="Choose"
                                >
                                  <span
                                    class="ace-file-name"
                                    data-title="No File ..."
                                  >
                                    <i class=" ace-icon fa fa-upload"></i>
                                  </span>
                                </span>
                                <a class="remove" href="#">
                                  <i class=" ace-icon fa fa-times"></i>
                                </a>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div
                      class="alert alert-danger"
                      id="warning_message"
                      style={{
                        padding: "3px",
                        wordBreak: "break-all",
                        overflowWrap: "break-word",
                        display: "none"
                      }}
                    ></div>
                  </div>
                </div>
              </div>
              <div class="space-4"></div>
              <div id="actions_btns" class="clearfix form-actions">
                <div class="col-md-9">
                  <button
                    id="btn_upload"
                    name="btn_upload"
                    class="btn btn-info margin-bottom-10"
                    type="button"
                  >
                    <i class="ace-icon fa fa-upload bigger-110"></i>{" "}
                    &nbsp;&nbsp;&nbsp;Upload&nbsp;&nbsp;&nbsp;
                  </button>
                  &nbsp; &nbsp; &nbsp;
                  <a
                    id="reset"
                    href="/productlist"
                    class="btn margin-bottom-10"
                  >
                    <i class="ace-icon fa fa-times"></i> Cancel
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <Modal
          open={open}
          onClose={this.onCloseModal}
          closeOnOverlayClick={false}
        >
          <h2>Simple centered modal</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
            pulvinar risus non risus hendrerit venenatis. Pellentesque sit amet
            hendrerit risus, sed porttitor quam.
          </p>
        </Modal>
      </div>
    );
  }
}

export default MultipleProducts;
