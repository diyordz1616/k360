import React, { Component } from "react";
import { Link } from "react-router-dom";
import DataTable from "react-data-table-component";
import axios from "axios";
import { Dropdown } from "react-bootstrap";

class ProductList extends Component {
  componentDidMount() {
    axios.get("https://reqres.in/api/users?page=1").then(response => {
      this.setState({ productPromise: response.data });
    });
  }
  state = {
    productPromise: []
  };
  datatableHandler = key => {
    this.props.history.push("/productdetails");
  };
  render() {
    const columns = [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "First Name",
        selector: "first_name",
        sortable: true
      },
      {
        name: "Last_name",
        selector: "last_name",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      }
    ];
    const productData = this.state.productPromise.data;

    return (
      <div class="row">
        <div class="page-header col-xs-12">
          <div class="pull-left">
            <h1>Product</h1>
          </div>
          <div class="pull-right">
            <div class="btn-group">
              <Link
                to="/productsettings"
                id="btn_settings"
                class="btn btn-white btn-sm btn-info btn-round"
              >
                <i class="ace-icon fa fa-cog icon-only"></i> Settings
              </Link>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-body">
              <div class="widget-main padding-4">
                <div class="tab-content padding-8">
                  <div id="task-tab" class="tab-pane active">
                    <div class="row">
                      <div className="col-sm-7 pull-right">
                        <Dropdown>
                          <Dropdown.Toggle
                            id="dropdown-basic"
                            bsPrefix="btn btn-white btn-sm btn-info btn-round dropdown-toggle"
                          >
                            <span className="ace-icon fa fa-plus icon-on-right"></span>{" "}
                            Add Product
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            <Dropdown.Item>
                              <Link to="/newproduct">
                                Add a Product manually
                              </Link>
                            </Dropdown.Item>
                            <Dropdown.Item href="#/action-3">
                              <Link to="/multipleproducts">
                                Add several Products at once (CSV file upload)
                              </Link>
                            </Dropdown.Item>
                          </Dropdown.Menu>
                          {"  "}
                          <Link
                            to="/multipleproducts"
                            className="btn btn-white btn-sm btn-info btn-round dropdown-toggle"
                          >
                            <i className="ace-icon fa fa-pencil bigger-110"></i>{" "}
                            Update several products (CSV file upload)
                          </Link>{" "}
                          <button
                            className="btn btn-white btn-sm btn-default btn-round"
                            id="btn-multiple-deactivate"
                            disabled=""
                          >
                            <span className="glyphicon glyphicon-trash"></span>{" "}
                            Deactivate
                          </button>
                          {"  "}
                          <a
                            href="/branches/csv/download/data"
                            className="btn btn-white btn-sm btn-info btn-round"
                            role="button"
                          >
                            <span className="glyphicon glyphicon-download"></span>{" "}
                            Export
                          </a>
                        </Dropdown>
                      </div>

                      <div class="space-20"></div>
                      <div class="col-xs-12 content">
                        <div class="space-6"></div>
                        <div class="table-responsive">
                          <DataTable
                            columns={columns}
                            data={productData}
                            onRowClicked={this.datatableHandler}
                            pointerOnHover
                            striped
                            highlightOnHover
                            pagination
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ProductList;
