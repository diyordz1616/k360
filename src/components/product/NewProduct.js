import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { Link } from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
// import PinMap from "./PinMap";
import DatePicker from "react-datepicker";
// import Contacts from "./AddContact";

import "react-datepicker/dist/react-datepicker.css";
const styles = {
  fontFamily: "sans-serif"
};

class NewProduct extends Component {
  constructor() {
    super();
    this.validator = new SimpleReactValidator();
  }
  state = {
    open: false,
    product_name: "",
    brand_name: "",
    category_name: "",
    manufacturer: "",
    customfield: "",
    units: [
      { text: "18", checked: false },
      { text: "35", checked: false },
      { text: "Big", checked: false },
      { text: "PC", checked: false },
      { text: "56", checked: false },
      { text: "9", checked: false },
      { text: "test", checked: false },
      { text: "4", checked: false }
    ]
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  onChangeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  submitForm = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      alert("You submitted the form and stuff!");
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      // you can use the autoForceUpdate option to do this automatically`
      this.forceUpdate();
    }
  };
  addUnit = () => {
    this.onOpenModal();
  };
  selectAll = e => {
    let sa = e.currentTarget.checked;
    this.setState({
      units: this.state.units.map(e => ({ text: e.text, checked: sa }))
    });
  };
  addUnitSubmit = e => {
    e.preventDefault();
    const formData = new FormData(e.target);
    let formSend = Object.fromEntries(formData);
    console.log(formSend);
    // this.onCloseModal();
  };
  onToggle = (index, e) => {
    let newItems = this.state.units.slice();
    newItems[index].checked = !newItems[index].checked;
    this.setState({
      units: newItems
    });
  };
  delUnit = u => {
    this.setState({
      units: this.state.units.map(s =>
        s.text === u.text ? { text: s.text, checked: false } : s
      )
    });
  };
  render() {
    const { open } = this.state;
    return (
      <div className="row">
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>New Product</h1>
          </div>
        </div>
        <div className="col-xs-12">
          <form
            className="form-horizontal"
            id="form_info"
            onSubmit={this.submitForm}
          >
            <div className="space-2"></div>
            <div
              className={
                this.validator.message(
                  "brand_name",
                  this.state.brand_name,
                  "required"
                ) !== undefined
                  ? "form-group has-error"
                  : "form-group"
              }
            >
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="brand_name"
              >
                Brand: <span className="required">*</span>
              </label>

              <div className="col-sm-3">
                <select
                  className="chosen-select form-control"
                  id="brand_name"
                  name="brand_name"
                  data-placeholder="Click to choose..."
                  onChange={this.onChangeHandler}
                >
                  <option></option>
                  <option>6566422691971072 </option>
                  <option>Carl Account</option>
                  <option>Mobilemo Inc.</option>
                </select>
                {this.validator.message(
                  "brand_name",
                  this.state.brand_name,
                  "required"
                )}
              </div>
            </div>
            <div
              className={
                this.validator.message(
                  "product_name",
                  this.state.product_name,
                  "required|min:2"
                ) !== undefined
                  ? "form-group has-error"
                  : "form-group"
              }
            >
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="product_name"
              >
                Product Name: <span className="required">*</span>
              </label>
              <div className="col-xs-12 col-sm-6">
                <div className="clearfix">
                  <input
                    type="text"
                    name="product_name"
                    id="product_name"
                    className="col-xs-12 col-sm-8"
                    maxlength="50"
                    onChange={this.onChangeHandler}
                  />
                </div>
                {this.validator.message(
                  "product_name",
                  this.state.product_name,
                  "required|min:2"
                )}
              </div>
            </div>
            <div
              className={
                this.validator.message(
                  "category_name",
                  this.state.category_name,
                  "required"
                ) !== undefined
                  ? "form-group has-error"
                  : "form-group"
              }
            >
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="category_name"
              >
                Category: <span className="required">*</span>
              </label>

              <div className="col-sm-3">
                <select
                  className="chosen-select form-control"
                  id="category_name"
                  name="category_name"
                  data-placeholder="Click to choose..."
                  onChange={this.onChangeHandler}
                >
                  <option></option>
                  <option>Category 1 </option>
                  <option>Carl Account</option>
                  <option>Mobilemo Inc.</option>
                </select>
                {this.validator.message(
                  "category_name",
                  this.state.category_name,
                  "required"
                )}
              </div>
            </div>
            <div class="form-group">
              <label
                class="col-sm-3 control-label no-padding-right"
                for="form-field-1"
              >
                <small>Unit of Measurement</small>
                <span class="required">*</span>
              </label>
              <div class="col-sm-6">
                <a
                  type="button"
                  id="btnAddUnit"
                  class="btn btn-sm btn-success"
                  onClick={this.addUnit}
                >
                  Add Unit
                </a>
                <br />
                <div class="input_fields_wrap">
                  <div id="unit_header" class="bg-info">
                    <h6 id="header_id" style={{ padding: "6px" }}>
                      {" "}
                      Unit
                    </h6>
                  </div>
                  <div id="listview-body" class="list-group col-sm-12">
                    <ul>
                      {this.state.units.map(e => {
                        return e.checked ? (
                          <a
                            className="delete list-group-item"
                            style={{
                              borderBottom: "none",
                              borderLeft: "none",
                              borderRight: "none"
                            }}
                          >
                            <label>
                              <small>{e.text}</small>
                            </label>{" "}
                            <i
                              class="ace-icon fa fa-times bigger-150 text-danger pull-right pointer"
                              aria-hidden="true"
                              onClick={() => this.delUnit(e)}
                            ></i>
                          </a>
                        ) : null;
                      })}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="hr hr-dotted"></div>

            <h3 className="lighter block green">Custom Fields:</h3>
            <div className="space-2"></div>

            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="5140133502255104"
              >
                Manufacturer:
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="input-group col-sm-3">
                  <input
                    type="text"
                    name="manufacturer"
                    onChange={this.onChangeHandler}
                    value={this.state.manufacturer}
                  />
                </div>
              </div>
            </div>

            <div className="form-group">
              <label
                className="control-label col-xs-12 col-sm-3 no-padding-right"
                for="5692552476884992"
              >
                Test CustomField:
              </label>

              <div className="col-xs-12 col-sm-9">
                <div className="input-group col-sm-3">
                  <input
                    type="text"
                    name="customfield"
                    onChange={this.onChangeHandler}
                    value={this.state.customfield}
                  />
                </div>
              </div>
            </div>

            <div className="space-4"></div>
            <div className="clearfix form-actions">
              <div className="col-md-offset-3 col-md-9">
                <button
                  id="btn_save"
                  className="btn btn-info margin-bottom-10"
                  type="submit"
                >
                  <i className="ace-icon fa fa-check bigger-110"></i> Submit
                </button>{" "}
                <Link
                  id="reset"
                  to="/productlist"
                  className="btn margin-bottom-10"
                >
                  {" "}
                  <i className="ace-icon fa fa-times bigger-110"></i> Cancel
                </Link>
              </div>
            </div>
          </form>
        </div>

        <Modal
          open={open}
          onClose={this.onCloseModal}
          closeOnOverlayClick={false}
        >
          <div class="modal-header">
            <h4 class="blue bigger">Please fill the following form fields</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="dropdown-container">
                <div class="dropdown-button noselect">
                  <div class="dropdown-label">Unit</div>
                  <i class="fa fa-filter"></i>
                </div>
                <div class="dropdown-list">
                  <input
                    type="search"
                    placeholder="Search "
                    class="dropdown-search"
                  />

                  <ul>
                    <li style={{ display: "list-item" }}>
                      <label>
                        <input
                          id="select_all"
                          name="list_item"
                          type="checkbox"
                          value=""
                          onClick={this.selectAll}
                        />
                        Select All
                      </label>
                    </li>
                    {this.state.units.map((item, i) => (
                      <li key={i}>
                        <label>
                          <input
                            className="list-item getRoles"
                            type="checkbox"
                            onChange={this.onToggle.bind(this, i)}
                            checked={item.checked}
                          />{" "}
                          {item.text}
                        </label>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div>
              <div class="space-10"></div>
              <button
                id="submitUOM"
                type="submit"
                class="btn btn-sm btn-success"
                onClick={this.onCloseModal}
              >
                <i class="ace-icon fa fa-check"></i> Done
              </button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default NewProduct;
