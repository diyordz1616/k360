import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { Link } from "react-router-dom";
import nextId from "react-id-generator";
// import "./settings.css";
import { ReactSortable } from "react-sortablejs";

const styles = {
  fontFamily: "sans-serif"
};

class ProductSettings extends Component {
  state = {
    open: false,
    target: [
      { id: "id0", value: "Text", tool: { id: 1, question: "Manufacturer" } },
      {
        id: "id1",
        value: "Text",
        tool: { id: 2, question: "Test CustomField" }
      }
    ],
    toolType: "",
    source: [
      { id: 1, value: "Text" },
      { id: 2, value: "Number" },
      { id: 3, value: "DateTime" }
    ]
  };
  deleteMe = val => {
    //delete state or children state
    this.setState({
      target: this.state.target.filter(function(tool) {
        return tool.id.toString() !== val.id.toString();
      })
    });
  };
  editMe = val => {
    let sets = [];
    let contents = [];
    let c = val.hasOwnProperty("tool") ? val.tool : [];

    contents.push([<input type="hidden" name="id" value={val.id} />]);
    this.setState({ toolType: val.value });
    contents.push(this.formItemContent(c));
    this.setState({ modalContent: contents });
    let iF = val.hasOwnProperty("tool")
      ? val.tool.hasOwnProperty("isFilterable")
        ? val.tool.isFilterable
        : ""
      : "";
    sets.push(this.filterContent(iF));

    this.setState({
      modalContentSettings: sets
    });

    this.onOpenModal(1);
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  closeEdit = () => {
    //when cancel is clicked on modal, delete from state if empty

    this.onCloseModal();
  };
  formSubmit = ev => {
    ev.preventDefault();
    const formData = new FormData(ev.target);
    // console.log(ev);
    let formSend = Object.fromEntries(formData);
    // console.log(formSend);
    this.setTool(formSend); //send to state
    this.onCloseModal();
  };
  setTool = formdata => {
    //console.log(formdata);
    //set tool according to value

    this.state.target.forEach(target => {
      if (target.id === formdata.id) {
        target["tool"] = formdata;
      }
    });
    //console.log(this.state.cloneControlledTarget);
  };
  submitForm = () => {
    console.log(this.state.target);
  };
  render() {
    const { open } = this.state;
    return (
      <div style={styles}>
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>Product Settings</h1>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="row">
            <div className="col-xs-12 col-sm-9 widget-container-col">
              <div class="widget-box">
                <div class="widget-header">
                  <h4 class="widget-title">Custom Fields</h4>
                </div>
                <div class="widget-body">
                  <div class="widget-main">
                    <ReactSortable
                      list={this.state.target}
                      setList={newState => this.setState({ target: newState })}
                      className="block-list"
                      animation={150}
                      group={{ name: "cloning-group-name", pull: "clone" }}
                      clone={item => ({ ...item, id: nextId() })}
                      onAdd={e => {
                        this.editMe(e.item.dataset); //show modal on Add
                      }}
                    >
                      {this.state.target.map(item => (
                        <div
                          key={item.id}
                          data-value={item.value}
                          data-all={JSON.stringify(item)}
                        >
                          <i
                            className={this.getIcon(item.value)}
                            style={{ paddingRight: "10px" }}
                          ></i>
                          {item.hasOwnProperty("tool")
                            ? item.tool.question
                            : item.value}{" "}
                          <i
                            className="ace-icon red fa fa-trash-o bigger-130 pull-right pointer"
                            onClick={() => this.deleteMe(item)}
                          ></i>
                          <i
                            className="ace-icon blue fa fa-pencil bigger-130 pull-right editMe pointer"
                            onClick={() => this.editMe(item)}
                          ></i>
                        </div>
                      ))}
                    </ReactSortable>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-3 widget-container-colol-xs-4">
              <div class="widget-box">
                <div class="widget-header">
                  <h5 class="widget-title smaller">Tools</h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main padding-6">
                    <ReactSortable
                      list={this.state.source}
                      setList={newState => this.setState({ source: newState })}
                      animation={150}
                      sort={false}
                      group={{
                        name: "cloning-group-name",
                        pull: "clone",
                        put: false
                      }}
                      clone={item => ({ ...item, id: nextId() })}
                      className="block-list"
                      style={{ border: "none", fontSize: "13px" }}
                    >
                      {this.state.source.map(item => (
                        <div
                          key={item.id}
                          data-value={item.value}
                          data-all={JSON.stringify(item)}
                        >
                          <i
                            className={this.getIcon(item.value)}
                            style={{ paddingRight: "10px" }}
                          ></i>
                          {item.value}
                        </div>
                      ))}
                    </ReactSortable>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Modal
            open={open}
            closeOnEsc={false}
            closeOnOverlayClick={true}
            showCloseIcon={false}
            onClose={this.closeEdit}
            closeOnOverlayClick={false}
            key={nextId()}
          >
            <div className="modal-dialog">
              <form
                id="add_survey_form"
                method="get"
                data-validate="parsley"
                style={{ minWidth: "350px" }}
                onSubmit={this.formSubmit}
              >
                <fieldset>
                  <div className="modal-header">
                    <span>
                      <h4 className="modal-title" id="myModalLabel">
                        Form Item Details
                      </h4>
                      (
                      <label id="question_type_label">
                        Tool Type : {this.state.toolType}
                      </label>
                      )
                    </span>
                    <br />
                  </div>
                  <div className="modal-body" key={nextId()}>
                    {this.state.modalContent}
                  </div>
                </fieldset>

                {this.state.modalContentSettings !== "" ? (
                  <div className="control-group" id="settings_div">
                    <label className="control-label">Settings</label>
                    <div className="controls form-group">
                      {this.state.modalContentSettings}
                    </div>
                  </div>
                ) : (
                  " "
                )}
                <div className="modal-footer">
                  <button
                    id="close_question_dialog"
                    type="button"
                    name="close"
                    value="close"
                    className="btn btn-default"
                    data-dismiss="modal"
                    onClick={this.closeEdit}
                  >
                    Cancel
                  </button>
                  <button
                    id="submit_question_dialog"
                    type="submit"
                    name="edit"
                    value="edit"
                    className="btn btn-primary"
                  >
                    Done
                  </button>
                </div>
              </form>
            </div>
          </Modal>
          <div id="actions_btns" class="clearfix form-actions">
            <div class="col-md-9">
              <button
                id="btn_save"
                class="btn btn-info margin-bottom-10"
                type="button"
                onClick={this.submitForm}
              >
                <i class="ace-icon fa fa-check bigger-110"></i> Submit
              </button>{" "}
              <Link id="reset" class="btn margin-bottom-10" to="/productlist">
                <i class="ace-icon fa fa-arrow-left"></i> Cancel
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
  formItemContent = e => (
    <div id="div_items_details" className="control-group" key={nextId()}>
      <label id="question_label" className="control-label">
        Form Item
      </label>
      <span id="question_count" className="pull-right">
        0 of 500
      </span>
      <div className="controls form-group">
        <textarea
          id="question"
          className="form-control"
          name="question"
          rows="3"
          required="required"
          maxLength="500"
          defaultValue={e.question}
        ></textarea>
      </div>
    </div>
  );
  filterContent = w => (
    <div className="checkbox" key={nextId()}>
      <label>
        <input
          id="isFilterable"
          name="isFilterable"
          className="ace ace-checkbox-2"
          type="checkbox"
          defaultChecked={w}
        />
        <span className="lbl"> Include as Filter </span>
      </label>
    </div>
  );
  formGroupContent = e => (
    <div id="div_question_details" className="control-group" key={nextId()}>
      <label id="question_label" className="control-label">
        Form Group Name
      </label>
      <span id="question_count" className="pull-right">
        0 of 500
      </span>
      <div className="controls form-group">
        <textarea
          id="question_g"
          className="form-control"
          name="question"
          rows="3"
          required="required"
          maxLength="500"
          // onBlur={e => this.groupHandler(e)}
          defaultValue={e.question}
        ></textarea>
      </div>{" "}
    </div>
  );
  getIcon = value => {
    switch (value) {
      case "Question Group":
        return "fa fa-question-circle";
        break;
      case "Text":
        return "fa fa-font";
        break;
      case "Multiline":
        return "fa fa-align-justify";
        break;
      case "Yes No":
        return "fa fa-dot-circle-o";
        break;
      case "Rating":
        return "fa fa-star-o";
        break;
      case "Drop Down":
        return "fa fa-caret-down";
        break;
      case "Multiple Choice":
        return "fa fa-th-large";
        break;
      case "Number":
        return "fa fa-hashtag";
        break;
      case "Date":
        return "fa fa-calendar";
        break;
      case "DateTime":
        return "fa fa-calendar";
        break;
      case "Email":
        return "fa fa-envelope-o";
        break;
      case "Time":
        return "fa fa-clock-o";
        break;
      case "Product":
        return "fa fa-product-hunt";
        break;
      case "Formula":
        return "fa fa-calculator";
        break;
      case "UOM":
        return "fa fa-caret-down";
        break;
      case "QUANTITY":
        return "fa fa-hashtag";
        break;
      case "DISCOUNT":
        return "fa fa-th-large";
        break;
      default:
        return "fa fa-tasks";
    }
  };
}

export default ProductSettings;
