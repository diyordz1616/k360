import React, { Component } from "react";
import DateRangePicker from "react-bootstrap-daterangepicker";
import "bootstrap3/dist/css/bootstrap.css";
import "bootstrap-daterangepicker/daterangepicker.css";
import DataTable from "react-data-table-component";
import moment from "moment";
import axios from "axios";

// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

class Reports extends Component {
  componentDidMount() {}

  state = {
    toDashboard: false,
    filterText: "",
    date: "Jan 2019",
    showReports: false,
    reportPromise: [],
    reportType: "Sales Entry",
    ranges: {
      Today: [moment(), moment()],
      Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
      "This Month": [moment().startOf("month"), moment().endOf("month")],
      "Last Month": [
        moment()
          .subtract(1, "month")
          .startOf("month"),
        moment()
          .subtract(1, "month")
          .endOf("month")
      ]
    },
    totalUsers: 1,
    totalVisits: 4
  };

  onChange = date => this.setState({ date: date });
  reportHandler = event => {
    this.setState({ reportType: event.target.value });
    this.retrieveData();
  };
  handlePickerSubmit = (event, picker) => {
    const momentString =
      picker.startDate.format("MMM D, YYYY") +
      " to " +
      picker.endDate.format("MMM D, YYYY");
    this.setState({ date: momentString });
    this.retrieveData();
  };

  datatableHandler = key => {
    // console.log(key.id);
    this.props.history.push("/reportdetails");
  };
  retrieveData = () => {
    this.setState({ showReports: true });
    let tU = this.generateRand() + 2;
    this.setState({ totalUsers: tU });
    let tV = this.generateRand() + 4;
    this.setState({ totalVisits: tV });
    let num = this.generateRand();
    axios.get("https://reqres.in/api/users?page=" + num).then(response => {
      this.setState({ reportPromise: response.data });
    });
  };
  generateRand = () => {
    return Math.floor(Math.random() * (3 - 1 + 1)) + 1;
  };
  render() {
    const reportData = this.state.reportPromise.data;
    const columns = [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "First Name",
        selector: "first_name",
        sortable: true
      },
      {
        name: "Last_name",
        selector: "last_name",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      }
    ];
    let reportsDiv = null;

    if (this.state.showReports) {
      reportsDiv = (
        <div>
          <div class="col-sm-12 infobox-container">
            <div class="space-12"></div>
            <div class="infobox infobox-blue">
              <div class="infobox-icon">
                <i class="ace-icon fa fa-users"></i>
              </div>

              <div class="infobox-data">
                <span class="infobox-text">Total Users</span>

                <div class="infobox-content">{this.state.totalUsers}</div>
              </div>
            </div>

            <div class="infobox infobox-green2">
              <div class="infobox-icon">
                <i class="ace-icon fa fa-building-o"></i>
              </div>

              <div class="infobox-data">
                <span class="infobox-text">Total Visits</span>

                <div class="infobox-content">{this.state.totalVisits}</div>
              </div>
            </div>
          </div>
          <DataTable
            title="Reports"
            columns={columns}
            data={reportData}
            onRowClicked={this.datatableHandler}
            pointerOnHover
            striped
            highlightOnHover
            pagination
          />
        </div>
      );
    }

    return (
      <div class="row">
        <div class="page-header col-xs-12">
          <div class="pull-left">
            <h1>Reports</h1>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="widget-box transparent" id="recent-box">
            <div class="widget-header">
              <div class="col-sm-5">
                Report :{" "}
                <select
                  value={this.state.reportType}
                  onChange={this.reportHandler}
                >
                  <option>Accomplishments</option>
                  <option>Sales Entry</option>
                </select>
              </div>
              <div class="col-sm-6 pull-right">
                <DateRangePicker
                  ranges={this.state.ranges}
                  startDate="1/1/2014"
                  endDate="3/1/2014"
                  onApply={this.handlePickerSubmit}
                >
                  <div
                    id="reportrange"
                    class="pull-right"
                    style={{
                      color: "black",
                      backGround: " #fff",
                      cursor: "pointer",
                      padding: " 4px 10px",
                      border: " 1px solid #ccc",
                      width: "100%"
                    }}
                  >
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span> {this.state.date}</span> <b class="caret"></b>
                  </div>
                </DateRangePicker>
              </div>
            </div>
          </div>
        </div>
        {reportsDiv}
      </div>
    );
  }
}
export default Reports;
