import React, { Component } from "react";
import { Link } from "react-router-dom";
import DateRangePicker from "react-bootstrap-daterangepicker";
import "bootstrap-daterangepicker/daterangepicker.css";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
// import moment from "moment";
import CheckBox from "./CheckBox";
import CalendarEvent from "./CalendarEvent";
import axios from "axios";
import arrayMove from "array-move";
import SimpleReactValidator from "simple-react-validator";
// import Moment from "react-moment";

class NewItinerary extends Component {
  constructor() {
    super();
    this.validator = new SimpleReactValidator();
  }
  state = {
    date: new Date().toString().substr(4, 12),
    generated: false,
    isEdit: false,
    isEditClicked: false,
    calendarEvents: [],
    title: "",
    content: "",
    dayChange: "Jan 01, 2019",
    days: [
      { id: 1, value: "MON", isChecked: true },
      { id: 2, value: "TUE", isChecked: true },
      { id: 3, value: "WED", isChecked: true },
      { id: 4, value: "THU", isChecked: true },
      { id: 5, value: "FRI", isChecked: true },
      { id: 6, value: "SAT", isChecked: true },
      { id: 7, value: "SUN", isChecked: true }
    ],
    selectedBranch: "",
    selectedUser: "",
    accountlist: [
      { id: 1, value: "Acoount1" },
      { id: 2, value: "Acoount B2" },
      { id: 3, value: "Acoount 133" }
    ],
    users: [
      { id: 1, value: "User 1" },
      { id: 2, value: "2" },
      { id: 3, value: "User 3" }
    ],
    branchlist: [],
    chosenBranches: []
  };
  submitPage = e => {
    let finalObj = [];
    finalObj.push(this.state.calendarEvents);
    console.log(finalObj);
  };
  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState(({ chosenBranches }) => ({
      chosenBranches: arrayMove(chosenBranches, oldIndex, newIndex)
    }));
  };
  handlePickerSubmit = (event, picker) => {
    const momentString = picker.startDate.format("MMM D, YYYY");
    this.setState({ date: momentString });
  };
  handleCheckChieldElement = event => {
    let days = this.state.days;
    days.forEach(day => {
      if (day.value === event.target.value)
        day.isChecked = event.target.checked;
    });
    this.setState({ days: days });
  };
  generateItinerary = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      this.generate();
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      // you can use the autoForceUpdate option to do this automatically`
      this.forceUpdate();
    }
  };
  generate = () => {
    // let dayOne = [
    //   {
    //     title: "DAY 1",
    //     start: new Date(this.state.date),
    //     allDay: true,
    //     ordering: 0
    //   }
    // ];
    // this.setState({
    //   calendarEvents: [...dayOne, ...this.state.calendarEvents]
    // });
    // if (newItinerary.length > 0) {
    //   //if there is itinerary and it's Edit Page
    //   let fixedDates = this._fixedDays(newItinerary);
    //   this.setState({ calendarEvents: fixedDates, generated: true });
    // } else {
    this.setState({ generated: true });
    // }
  };
  addBranch = (e, clickedDate) => {
    //copy selectedrows to calendarevent
    let newlist = [];
    let newdate = clickedDate; //selected date
    e.forEach(element => {
      //prepare data
      newlist.push({
        title: element.hasOwnProperty("employee_name")
          ? element.employee_name
          : element.title,
        start: newdate,
        content: element.hasOwnProperty("employee_salary")
          ? element.employee_salary
          : element.content,
        allDay: true,
        id: element.id,
        ordering: this.getLastEventOfDate(clickedDate)
      });
    });
    // ADD DAY EVENT
    // let dayLevel = this._getDay(clickedDate);
    // if (newlist.length > 0 || dayLevel == 1) {
    //   newlist.unshift({
    //     // DAY event first
    //     title: "DAY " + dayLevel,
    //     start: newdate,
    //     content: " ",
    //     allDay: true,
    //     ordering: this.getLastEventOfDate(clickedDate)
    //   });
    // }

    let cleanEvents = this.state.calendarEvents.filter(ev => {
      //clean calendarevents with this date
      return ev.start.valueOf() !== newdate.valueOf();
    });
    // console.log(newlist);
    let newEvents = cleanEvents.concat(newlist);
    this.setState({
      calendarEvents: newEvents
    });

    this.setState({ title: "", content: "", chosenBranches: [] });
  };
  saveBranches = clickedDate => {
    // save items from selected in datatable to chosen branches
    let newdate = clickedDate.subDays(1);
    let newlist = [];
    this.state.calendarEvents.forEach(element => {
      newlist.push({
        title: element.hasOwnProperty("employee_name")
          ? element.employee_name
          : element.title,
        start: newdate,
        content: element.hasOwnProperty("employee_salary")
          ? element.employee_salary
          : element.content,
        allDay: true,
        id: element.id,
        ordering: this.getLastEventOfDate(clickedDate)
      });
    });
    this.setState({
      chosenBranches: newlist
    });
  };

  delBranch = (itemStart, itemTitle) => e => {
    this.setState({
      calendarEvents: this.state.calendarEvents.filter(function(e, i) {
        return (
          //choosing whc state to remove
          JSON.stringify(e.start) != JSON.stringify(itemStart) ||
          JSON.stringify(e.title) !== JSON.stringify(itemTitle)
        );
      })
    });
  };
  removeBranch = (id, clickedDate) => {
    //delete a branch

    this.setState({
      chosenBranches: this.state.chosenBranches.filter(function(e, i) {
        // console.log(id + "--" + e.id);
        return (
          e.id !== id
          //choosing whc state to remove
          //JSON.stringify(e.start) != JSON.stringify(itemStart) ||
          //JSON.stringify(e.id) !== id
        );
      })
    });
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  updateBranches = br => {
    this.setState({
      chosenBranches: br
    });
  };
  resetBranchList = e => {
    this.setState({ branchlist: [] });
  };
  onSelect = (val, clickedDate) => e => {
    //on click account list
    axios.get(`http://dummy.restapiexample.com/api/v1/employees`).then(res => {
      let branchlist = res.data;
      let readyList = [];
      readyList = branchlist.data.filter(
        row => !this._isFound(val, row.id, clickedDate)
      );
      this.setState({ branchlist: readyList });
    });

    this.setState({
      selectedBranch: e.target.value
    });
  };
  onSelectUser = () => e => {
    this.setState({
      selectedUser: e.target.value
    });
  };
  getLastEventOfDate = date => {
    let w = 0;
    this.state.calendarEvents.map(function(x) {
      if (x.start.valueOf() === date.valueOf()) {
        w = x.ordering;
      }
    });
    return w + 1;
  };

  sortHandle = clickedDate => (order, sortable, evt) => {
    let events = this.state.calendarEvents;
    let x = events.length;
    events.forEach(event => {
      // if (
      //   event.start.valueOf() ==
      //   new Date(clickedDate)
      //     .setDate(new Date(clickedDate).getDate() - 1)
      //     .valueOf()
      // ) {
      event.ordering = x;
      // }
      x--;
    });
    this.setState({ calendarEvents: events });
  };
  componentDidMount = () => {
    // console.log(this.props.location.state.detail);
    const state = this.props.location.state;
    if (typeof state !== "undefined") {
      axios
        .get(`http://dummy.restapiexample.com/api/v1/employees`)
        .then(res => {
          let branchlist = res.data;
          let readyList = [
            {
              title: "Tiger Nixon",
              start: "2020-02-06T16:00:00.000Z",
              content: "320800",
              allDay: true,
              id: "1",
              ordering: 1
            },
            {
              title: "Garrett Winters",
              start: "2020-02-07T16:00:00.000Z",
              content: "170750",
              allDay: true,
              id: "2",
              ordering: 2
            },
            {
              title: "Ashton Cox",
              start: "2020-02-07T16:00:00.000Z",
              content: "86000",
              allDay: true,
              id: "3",
              ordering: 2
            }
          ];
          // readyList = branchlist.data;
          let fixedDates = this._fixedDays(readyList);
          this.setState({
            calendarEvents: fixedDates,
            generated: true,
            isEdit: true
          });
        });
    }
  };
  render() {
    const newButtons = (
      <div className="col-md-9" style={{ paddingTop: "10px" }}>
        <Link
          to="/itineraries"
          id="btn_save"
          className="btn btn-info margin-bottom-10"
          onClick={this.submitPage}
        >
          <i className="ace-icon fa fa-floppy-o bigger-110"></i>
          <span>Save</span>
        </Link>
        {"  "}
        <Link to="/itineraries" className="btn margin-bottom-10">
          <i className="ace-icon fa fa-arrow-left"></i> Cancel
        </Link>
      </div>
    );
    const editButtons = (
      <div className="col-md-9" style={{ paddingTop: "10px" }}>
        <button
          id="btn_edit"
          className="btn btn-info margin-bottom-10"
          onClick={this.editPage}
        >
          <i className="ace-icon fa fa-floppy-o bigger-110"></i>
          <span>Edit</span>
        </button>
        {"  "}
        <Link to="/itineraries" className="btn margin-bottom-10">
          <i className="ace-icon fa fa-arrow-left"></i> Cancel
        </Link>
      </div>
    );
    const calendarview = (
      <div className="col-xs-12 col-sm-12">
        <CalendarEvent
          date={this.state.date}
          accountlist={this.state.accountlist}
          isEditClicked={this.state.isEditClicked}
          branchlist={this.state.branchlist}
          chosenBranches={this.state.chosenBranches}
          calendarEvents={this.state.calendarEvents}
          isEdit={this.state.isEdit}
          onSortEnd={this.onSortEnd}
          addBranch={this.addBranch}
          removeBranch={this.removeBranch}
          resetBranchList={this.resetBranchList}
          onChange={this.onChange}
          onSelect={this.onSelect}
          delBranch={this.delBranch}
          sortHandle={this.sortHandle}
          // saveBranches={this.saveBranches}
          updateBranches={this.updateBranches}
        />
        {this.state.isEdit ? editButtons : newButtons}
      </div>
    );
    return (
      <div className="row">
        <div className="col-12-xs" id="event-list">
          <div className="widget-box transparent">
            <div className="widget-header">
              <h4 className="widget-title lighter smaller">
                <i className="ace-icon fa fa-user orange"></i> USER SELECTION:
              </h4>
            </div>
            <div className="space-6"></div>
            <div className="form-group">
              <label className="control-label col-xs-12 col-sm-3 no-padding-right">
                User: <span className="required">*</span>
              </label>
              <div className="col-xs-12 col-sm-9">
                <div className="input-group col-sm-5">
                  <select
                    className="chosen-select form-control"
                    value={this.state.selectUser}
                    onChange={this.onSelectUser()}
                    name="user"
                  >
                    {" "}
                    <option value="" key="0" disabled selected>
                      Select a User
                    </option>
                    {this.state.users.map(user => (
                      <option
                        key={user.id}
                        value={user.value}
                        disabled={this.state.generated ? true : null}
                      >
                        {user.value}
                      </option>
                    ))}
                  </select>
                  {this.validator.message(
                    "selectedUser",
                    this.state.selectedUser,
                    "required"
                  )}
                </div>
              </div>
              <div className="col-12-xs" id="setting-schedule">
                <div className="widget-box transparent">
                  <div className="widget-header">
                    <h4 className="widget-title lighter smaller">
                      <i className="ace-icon fa fa-cogs orange"></i> SCHEDULE
                      SETTINGS
                    </h4>
                  </div>
                  <div className="widget-body">
                    <div className="widget-main padding-1">
                      <div className="form-group">
                        <label className="control-label col-xs-12 col-sm-3 no-padding-right">
                          Start Date: <span className="required">*</span>
                        </label>
                        <div
                          className="col-xs-12 col-sm-9"
                          disabled={this.state.generated}
                        >
                          <DateRangePicker
                            singleDatePicker
                            onApply={this.handlePickerSubmit}
                            // disabled={true}
                          >
                            <div className="input-group col-sm-10">
                              <input
                                readOnly
                                className="form-control date-picker"
                                style={{ border: "1px solid #ccc" }}
                                value={this.state.date}
                                name="date"
                                disabled={this.state.generated ? true : null}
                              />
                              {this.validator.message(
                                "date",
                                this.state.date,
                                "required"
                              )}
                              <span className="input-group-addon">
                                <i className="fa fa-calendar bigger-110"></i>
                              </span>
                            </div>
                          </DateRangePicker>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="control-label col-xs-12 col-sm-3 no-padding-right">
                          Working Days: <span className="required">*</span>
                        </label>

                        <div className="col-xs-12 col-sm-9">
                          <div className="clearfix">
                            <span
                              id="working_days_selection"
                              className="inline"
                            >
                              <span className="btn-toolbar inline middle no-margin">
                                <span
                                  id="span_working_days"
                                  data-toggle="buttons"
                                  className="btn-group no-margin"
                                >
                                  {this.state.days.map(day => {
                                    return (
                                      <CheckBox
                                        handleCheckChieldElement={
                                          this.handleCheckChieldElement
                                        }
                                        {...day}
                                      />
                                    );
                                  })}
                                </span>
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>

                      <div
                        className="form-group"
                        style={{ paddingTop: "60px" }}
                      >
                        <label className="control-label col-xs-12 col-sm-3 no-padding-right">
                          Repeat Schedule:{" "}
                        </label>

                        <div className="col-xs-12 col-sm-1">
                          <div className="clearfix">
                            <label className="inline">
                              {" "}
                              <input
                                type="checkbox"
                                id="chk_repeat_schedule"
                                name="chk_repeat_schedule"
                                className="ace input-lg"
                              />{" "}
                              <span className="lbl"></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="space-12"></div>
                      <div className="form-group">
                        <label className="control-label col-xs-12 col-sm-3 no-padding-right"></label>
                        <div className="col-xs-12 col-sm-4">
                          <div className="clearfix">
                            <button
                              ref={this.simulateClick}
                              id="btn_generate_itinerary"
                              className="btn btn-info margin-bottom-10 pull-right"
                              type="button"
                              onClick={this.generateItinerary}
                              disabled={this.state.generated}
                            >
                              <i className="ace-icon fa fa-floppy-o bigger-110"></i>
                              Generate Itinerary
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12-xs" id="event-list">
          {this.state.generated ? calendarview : null}
        </div>
      </div>
    );
  }
  editPage = () => {
    //activate buttons
    const allButtons = document.querySelectorAll(".activeDay");
    allButtons.forEach(e => e.classList.remove("hideButton"));
    this.setState({ isEditClicked: true });
    // console.log(allButtons);
  };
  _fixedDays = e => {
    let fd = e.map(d => {
      return {
        title: d.title,
        start: new Date(d.start),
        content: d.content,
        allDay: d.allDay,
        id: d.id,
        ordering: d.ordering
      };
    });
    return fd;
  };
  _getDay(cd) {
    const startDate = new Date(this.state.date + " 00:00"); //start date
    const diffTime = Math.abs(startDate - cd);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays + 1;
  }
  _isFound(json, id, cd) {
    //if id is found
    let obj = [];
    obj = json.filter(
      //check existence of id with this date if it exists
      row => row.id === id && row.start.valueOf() === cd.valueOf()
    );
    if (Object.keys(obj).length === 0) {
      return false;
    } else {
      return true;
    }
  }
}
Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};
Date.prototype.subDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() - days);
  return date;
};
export default NewItinerary;
