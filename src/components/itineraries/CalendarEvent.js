import React, { Component } from "react";
// import ReactDOM from "react-dom";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
// import Event, { eventNewDiv, tester } from "./EventHandle";
// import moment from "react-moment";
import Modal from "react-responsive-modal";
import uniqueId from "lodash/uniqueId";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import "./list.sccs";

import "@fullcalendar/core/main.css";
import "@fullcalendar/daygrid/main.css";

const columns = [
  {
    dataField: "id",
    text: "Product ID"
  },
  {
    dataField: "employee_name",
    text: "Branch Name"
  }
];
const SortableItem = SortableElement(({ value, note, id }) => (
  <li className="list-group-item" style={{ zIndex: "1010" }}>
    <b style={{ marginRight: "5px" }}>{value}</b>
    <i style={{ color: "silver" }}>{note}</i>
    <div className="pull-right action-buttons">
      {/* <a
        id="editBranch"
        className="blue"
        style={{ cursor: "pointer" }}
     
      >
        <i className="ace-icon fa fa-pencil bigger-130"></i>
      </a> */}
      <a
        className="red"
        style={{ cursor: "pointer" }}
        onClick={() => {
          //window.theComponent.removebranch(id);
          window.theComponent.props.removeBranch(
            id,
            window.theComponent.state.clickedDate
          );
        }}
      >
        <i className="ace-icon fa fa-trash-o bigger-130"></i>
      </a>
    </div>
  </li>
));

const SortableList = SortableContainer(({ items }) => {
  return (
    <ul>
      {items.map((value, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          id={value.id}
          note={value.content}
          value={value.title}
        />
      ))}
    </ul>
  );
});
class CalendarEvent extends Component {
  constructor() {
    super();
    window.theComponent = this;
  }
  state = {
    calendarWeekends: false,
    open: false,
    dateSelected: "Select Date",
    calendarEvents: [],
    clickedDate: "",
    content: "",
    contentSelect: 0,
    selectedRows: []
  };

  componentDidMount() {}
  render() {
    const selectRow = {
      mode: "checkbox",
      clickToSelect: true,
      onSelect: this.handleOnSelect,
      onSelectAll: this.handleOnSelectAll
    };
    let branchlist = this.props.chosenBranches.filter(
      (value, index) =>
        value.start.valueOf() ===
        new Date(this.state.clickedDate)
          .setDate(new Date(this.state.clickedDate).getDate() - 1)
          .valueOf()
          ? value
          : null
      //value
    );
    let savedBranchList = branchlist.map(val => {
      if (val.title.includes("DAY")) {
        //dont include events with 'day'
        return null;
      } else {
        return (
          <li
            key={uniqueId()}
            data-id={val.ordering}
            className="item-blue clearfix ui-sortable-handle"
          >
            <label className="inline">
              <i className="normal-icon ace-icon fa fa-bars blue bigger-130"></i>
              {"  "}
              <span className="lbl">
                <b>{val.title}</b>{" "}
                <small style={{ color: "dimgrey" }}>conten_there</small>
              </span>
            </label>
          </li>
        );
      }
    });
    let modalContent = (
      <div className="">
        <div className="modal-header">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-hidden="true"
            onClick={this.onCloseModal}
          >
            &times;
          </button>
          <h4 className="modal-title" id="myModalLabel">
            Check Branch to Assign
          </h4>
        </div>
        <form
          style={{ marginTop: "10px", minWidth: "520px" }}
          autoComplete="off"
          className="form-inline"
          // onSubmit={this.props.addBranch(this.state.clickedDate)}
          // onSubmit={this.saveBranchlist}
          onSubmit={e => e.preventDefault()}
        >
          <div className="row" style={{ minHeight: "200px" }}>
            <div className="col-xs-12 col-sm-3 no-padding-left ">
              <label className="col-sm-12 control-label">Account Name</label>
            </div>
            <div className="col-xs-12 col-sm-9 ">
              <select
                className="chosen-select form-control"
                value={this.props.selectedBranch}
                // onChange={e =>
                //   this.setState({
                //     selectedBranch: e.target.value,
                //     validationError:
                //       e.target.value === "" ? "You must select branch" : ""
                //   })
                // }
                onChange={this.props.onSelect(
                  this.props.calendarEvents,
                  this.state.clickedDate
                )}
              >
                {" "}
                <option value="select" key="0" disabled selected>
                  Select a branch
                </option>
                {this.props.accountlist.map(branch => (
                  <option key={branch.id} value={branch.value}>
                    {branch.value}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-xs-12 col-sm-12" style={{ paddingTop: "5px" }}>
              <BootstrapTable
                keyField="id"
                data={this.props.branchlist}
                columns={columns}
                selectRow={selectRow}
                pagination={paginationFactory()}
              />
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-default"
              // onClick={() => this.changeModalContent(0)}
              onClick={this.onCloseModal}
            >
              Back
            </button>{" "}
            <button
              id="sort_submit"
              data-dismiss="modal"
              className="btn btn-primary"
              onClick={() => this.changeModalContent(2)}
            >
              Add Note
            </button>
            <button
              id="sort_submit"
              data-dismiss="modal"
              className="btn btn-primary"
              // type="submit"
              onClick={this.saveBranchlist}
            >
              Save
            </button>
          </div>{" "}
        </form>
      </div>
    );
    let modalContent0 = (
      <div style={{ minWidth: "550px" }}>
        <div className="modal-header">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-hidden="true"
            onClick={this.onCloseModal}
          >
            &times;
          </button>
          <h4 className="modal-title" id="myModalLabel">
            Arrange Branch
          </h4>
        </div>{" "}
        {/* <button
          type="add"
          className="btn btn-info btn-sm"
          style={{
            marginTop: "10px",
            marginBottom: "10px",
            width: "550px",
            marginLeft: "auto",
            marginRight: "auto",
            display: "block"
          }}
          onClick={() => {
            this.props.resetBranchList();
            this.updateSelectedRows();
            this.changeModalContent(1);
          }}
        >
          <i className="ace-icon fa fa-plus bigger-110"></i>ADD
        </button> */}
        {this.props.chosenBranches.length < 1 ? "No items found." : " "}
        <SortableList
          items={this.props.chosenBranches}
          onSortEnd={this.props.onSortEnd}
          distance={1}
          lockAxis="y"
        />
        <div className="modal-footer">
          <button
            id="submit_branch_selection_btn2"
            type="submit"
            data-dismiss="modal"
            className="btn btn-primary"
            onClick={this.saveEvents}
            // onClick={this.onCloseModal}
          >
            <i></i>
            <span>Done</span>
          </button>
        </div>
      </div>
    );
    let modalContent1 = (
      <div>
        <div className="modal-header">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-hidden="true"
            onClick={this.onCloseModal}
          >
            &times;
          </button>
          <h4 className="modal-title" id="myModalLabel">
            Add Note
          </h4>
        </div>{" "}
        <form
          style={{ marginTop: "10px", minWidth: "520px" }}
          autoComplete="off"
          className="form-inline"
          onSubmit={this.saveBranchlistNote}
          id="addNotes"
        >
          <ul>
            {this.state.selectedRows.map(function(row, i) {
              return (
                <li
                  key={i}
                  className="branchNotes"
                  style={{ listStyleType: "none" }}
                >
                  <label>
                    <b>
                      <small>
                        {row.hasOwnProperty("employee_name")
                          ? row.employee_name
                          : row.title}
                      </small>
                    </b>
                  </label>
                  <input
                    type="hidden"
                    className="id"
                    name="id"
                    value={row.id}
                    readOnly
                  />
                  <input
                    type="hidden"
                    className="title"
                    name="title"
                    value={
                      row.hasOwnProperty("employee_name")
                        ? row.employee_name
                        : row.title
                    }
                    readOnly
                  />
                  <br />
                  <textarea
                    name="content"
                    className="notes form-control"
                    rows="1"
                    cols="50"
                    maxlength="5000"
                  >
                    {row.hasOwnProperty("employee_salary")
                      ? row.employee_salary
                      : row.content}
                  </textarea>
                </li>
              );
            })}
          </ul>
          <div className="modal-footer" style={{ marginTop: "10px" }}>
            <button
              type="button"
              className="btn btn-default"
              onClick={() => this.changeModalContent(1)}
            >
              Back
            </button>
            {"  "}
            <button
              type="submit"
              data-dismiss="modal"
              className="btn btn-primary"
              //onClick={this.saveBranchlist}
            >
              <i></i>
              <span>Save</span>
            </button>
          </div>
        </form>
      </div>
    );
    const { open } = this.state;
    return (
      <div>
        <FullCalendar
          events={this.props.calendarEvents}
          //addBranch={this.addBranch}
          // dateClick={this.handleDayClick} //day is clicked/add event
          eventRender={this.eventNewDiv} //each event style
          contentHeight={"auto"}
          eventOrder="ordering"
          defaultView="dayGridMonth"
          plugins={[dayGridPlugin, interactionPlugin]}
          // eventLimit={3}
          dayRender={this.dayRender} //add custom button
          eventPositioned={this.eventPositionedHandler}
        />
        <Modal
          open={open}
          onClose={this.onCloseModal}
          center
          showCloseIcon={false}
          className="modal-content"
        >
          {/* {this.state.contentSelect == 1 ? modalContent : modalContent0} */}
          {this.state.contentSelect == 1
            ? modalContent
            : this.state.contentSelect == 0
            ? modalContent0
            : modalContent1}
        </Modal>
      </div>
    );
  }
  eventNewDiv = info => {
    //change the event style (each event)
    //console.log(info);
    const eventDiv = document.createElement("div");
    const classes = Array.from(info.el.classList);
    eventDiv.classList.add(...classes);
    let contentDiv = document.createElement("div");
    if (!info.event.title.includes("DAY")) {
      contentDiv.style.cssText = "background-color: white";
    }
    contentDiv.innerText = info.event.title;
    let btn = document.createElement("i");
    btn.setAttribute(
      "class",
      "fa fa-times red bigger-150 pull-right activeDay hideButton"
    );
    if (!this.props.isEdit) {
      //if its edit button hide it first
      btn.classList.remove("hideButton");
    }
    btn.onclick = this.props.delBranch(info.event.start, info.event.title);
    btn.style.cssText = "float:right;margin-right:2px;cursor:pointer";
    if (!info.event.title.includes("DAY")) {
      contentDiv.appendChild(btn);
    }
    let extended = document.createElement("div");
    extended.style.cssText = "color:dimgrey;white-space:normal;max-height:32px";
    extended.setAttribute("class", "text-left");
    extended.innerText = info.event.extendedProps.content;
    if (info.event.extendedProps.content !== undefined) {
      contentDiv.appendChild(extended);
    }
    eventDiv.appendChild(contentDiv);
    return eventDiv;
  };
  handleDayClick = arg => {
    //check if date click is allowed
    let startDate = new Date(this.props.date + " 00:00"); //start date
    this.setState({ clickedDate: new Date(arg.date) }); //save chosen date
    let yesterday = this.state.clickedDate.subDays(1);
    if (startDate <= this.state.clickedDate) {
      //if click allowed
      //check if clickeddate's yesterday has events inside | ONLY DATES WITH EVENTS PREVIEWS DAY ARE CLICKABLE
      if (
        this._dateHasEvents(yesterday) ||
        startDate.valueOf() === this.state.clickedDate.valueOf()
      ) {
        //copy from calendarEvents to ChosenBranches
        this.toChosenBranches(new Date(arg.date));
        this.setState({ open: true }); //open modal
        this.state.clickedDate.setDate(this.state.clickedDate.getDate() + 1); //choose correct date
        let dateFormatted = this.state.clickedDate
          .toUTCString()
          .substring(0, 17)
          .split(" "); //format date for view
        this.setState({
          //save date for show in title
          dateSelected:
            dateFormatted[2] + " " + dateFormatted[1] + ", " + dateFormatted[3]
        });
      }
    } else {
      console.log(arg);
    }
  };
  dayRender = info => {
    //render of every day
    // console.log(info);
    const startDate = new Date(this.props.date + " 00:00"); //start date
    //check if this day's yesterday has event
    if (
      // this._dateHasEvents(yesterday) ||
      info.date.valueOf() >= startDate.valueOf()
    ) {
      var add = this._buttons(info); //add buttons to pertinent days
      var dayLevel = this._dayLevel(this._getDay(info.date)); //add buttons to pertinent days
      if (
        info.date.valueOf() === startDate.valueOf() ||
        this._eventExists(info.date)
      ) {
        //if first day, or if this day has event(calenderEvents) SHOW BUTTON
        if (this.props.isEdit) {
          //IF edit MODE hide all buttons first
          if (this.props.isEditClicked) {
            //if already clicked edit show the buttons
            add.classList.remove("hideButton");
          }
        } else {
          add.classList.remove("hideButton");
        }

        dayLevel.classList.remove("hideButton");

        //add class for hide/show of date on EDIT mode
        add.classList.add("activeDay");
      }
      // ADD DAYLEVEL

      info.el.prepend(dayLevel);
      info.el.prepend(add);
    }
  };
  _eventExists = gd => {
    let ef = this.props.calendarEvents.filter(ev => {
      return (
        // new Date(ev.start.toISOString().slice(0, 10) + " 00:00").valueOf() ===
        ev.start.valueOf() === gd.valueOf() && !ev.title.includes("DAY")
      );
    });
    if (ef.length > 0) return true;
    else return false;
  };

  _buttons = info => {
    let add = document.createElement("i");
    add.style.cssText = "padding-right:10px";
    add.setAttribute("class", "ace-icon fa fa-plus blue bigger-10 pull-left ");
    add.onclick = () => this.addClick(info.date);
    let span = document.createElement("span");
    span.innerHTML = " ADD";
    span.style.cssText = "font-family:'verdana', serif;";
    add.appendChild(span);
    //arrange button
    let arr = document.createElement("i");
    arr.setAttribute("class", "ace-icon fa fa-sort blue bigger-10 pull-right ");
    arr.onclick = () => this.arrangeClick(info.date);
    let span2 = document.createElement("span");
    span2.innerHTML = " ARRANGE";
    span2.style.cssText = "font-family:'verdana', serif;";
    arr.appendChild(span2);

    let mainDiv = document.createElement("div");
    mainDiv.style.cssText =
      "position:absolute; margin-left: 5px; margin-top:5px;cursor:pointer !important;z-index:5; font-size:10px;";
    mainDiv.setAttribute("class", "hideButton");

    mainDiv.appendChild(add);
    mainDiv.appendChild(arr);
    return mainDiv;
  };
  _dayLevel = e => {
    let dayLevel = document.createElement("div");
    let innertxt = document.createElement("div");
    innertxt.innerText = "DAY " + e;
    dayLevel.style.cssText = "margin-top:20px;";
    dayLevel.setAttribute(
      "class",
      "fc-day-grid-event fc-h-event fc-event fc-start fc-end hideButton"
    );
    dayLevel.appendChild(innertxt);
    return dayLevel;
  };
  arrangeClick = el => {
    //button add is clicked
    const givenDay = new Date(el);

    this.setState({ clickedDate: givenDay }); //save chosen date
    const startDate = new Date(this.props.date + " 00:00"); //start date

    //copy from calendarEvents to ChosenBranches
    this.toChosenBranches(givenDay);
    this.changeModalContent(0);
    this.setState({ open: true }); //open modal
    //this.state.clickedDate.setDate(givenDay); //choose correct date
    let dateFormatted = givenDay
      .toUTCString()
      .substring(0, 17)
      .split(" "); //format date for view
    this.setState({
      //save date for show in title
      dateSelected:
        dateFormatted[2] + " " + dateFormatted[1] + ", " + dateFormatted[3]
    });
  };
  addClick = el => {
    //button add is clicked
    const givenDay = new Date(el);

    this.setState({ clickedDate: givenDay }); //save chosen date
    const startDate = new Date(this.props.date + " 00:00"); //start date

    this.props.resetBranchList();

    this.toSelectedRows(givenDay);
    this.changeModalContent(1);
    //copy from calendarEvents to ChosenBranches
    // this.toChosenBranches(givenDay);
    this.setState({ open: true }); //open modal
    //this.state.clickedDate.setDate(givenDay); //choose correct date
    let dateFormatted = givenDay
      .toUTCString()
      .substring(0, 17)
      .split(" "); //format date for view
    this.setState({
      //save date for show in title
      dateSelected:
        dateFormatted[2] + " " + dateFormatted[1] + ", " + dateFormatted[3]
    });
  };
  selectedEvents = () => {
    // let curEvents = this.props.calendarEvents.filter(
    //   ev => ev.start === this.state.valueOf
    // );
    let curEvents = [];
    curEvents = this.props.calendarEvents.filter(ev => {
      return (
        !ev.title.includes("DAY") &&
        new Date(ev.start.toISOString().slice(0, 10) + " 00:00").valueOf() ===
          this.state.clickedDate.valueOf()
      );
    });
    return curEvents;
  };
  toSelectedRows = clickedDate => {
    let newrows = [];
    this.props.calendarEvents.forEach(element => {
      if (
        !element.title.includes("DAY") &&
        new Date(element.start.toISOString().slice(0, 10) + " 00:00")
          .addDays(1)
          .valueOf() === clickedDate.valueOf()
      ) {
        newrows.push({
          title: element.hasOwnProperty("employee_name")
            ? element.employee_name
            : element.title,
          start: clickedDate,
          content: element.hasOwnProperty("employee_salary")
            ? element.employee_salary
            : element.content,
          allDay: true,
          id: element.id,
          ordering: element.ordering
        });
      }
    });

    this.setState({ selectedRows: newrows });
  };
  toChosenBranches = clickedDate => {
    let newChosenBranches = [];
    this.props.calendarEvents.forEach(element => {
      if (
        !element.title.includes("DAY") &&
        new Date(element.start.toISOString().slice(0, 10) + " 00:00")
          .addDays(1)
          .valueOf() === clickedDate.valueOf()
      ) {
        newChosenBranches.push({
          title: element.hasOwnProperty("employee_name")
            ? element.employee_name
            : element.title,
          start: clickedDate,
          content: element.hasOwnProperty("employee_salary")
            ? element.employee_salary
            : element.content,
          allDay: true,
          id: element.id,
          ordering: element.ordering
        });
      }
    });
    // console.log(newChosenBranches);
    // console.log(this.props.calendarEvents);
    this.props.updateBranches(newChosenBranches);
  };
  changeModalContent = arg => {
    this.setState({ contentSelect: arg });
  };
  handleOnSelect = (row, isSelect) => {
    if (isSelect) {
      let tempRows = this.state.selectedRows.filter(r => r.id === row.id);
      if (tempRows === undefined || tempRows.length == 0) {
        //make sure no dupes
        this.setState({ selectedRows: [...this.state.selectedRows, row] });
      }
    } else {
      let tempRows = this.state.selectedRows.filter(r => r.id !== row.id);
      this.setState({ selectedRows: tempRows });
    }
    //console.log(this.state.selectedRows);

    return true; // return true or dont return to approve current select action
  };
  removebranch = e => {
    this.props.removebranch(e);
  };
  addNote = () => {
    this.setState({ contentSelect: 1 });
  };
  resetSelectedRows = cd => {
    //get all events on this date except day
    // let cleanEvents = this.state.calendarEvents.filter(ev => {
    //   //clean calendarevents with this date
    //   return ev.start.valueOf() === cd.valueOf() && ev.title.includes("DAY");
    // });
    this.setState({ selectedRows: [] });
  };
  saveEvents = e => {
    //on arrangebranch save
    //chosenbranches add to calendarEvents
    this.props.addBranch(this.props.chosenBranches, this.state.clickedDate);
    this.onCloseModal();
    if (this._dateHasEvents(this.state.clickedDate)) {
      this._showButtons(this.state.clickedDate);
    }
    this.props.resetBranchList();
  };
  saveBranchlist = () => {
    //save check branch assign
    // this.setState({ contentSelect: 0 });
    this.props.addBranch(this.state.selectedRows, this.state.clickedDate);
    this.onCloseModal();
    if (this._dateHasEvents(this.state.clickedDate)) {
      this._showButtons(this.state.clickedDate);
    }
    // console.log(this.props.calendarEvents);
    this.props.resetBranchList();
    //update height here
  };

  saveBranchlistNote = e => {
    e.preventDefault();
    let branchNotes = [];
    // this.setState({ contentSelect: 0 });
    let lis = document.getElementById("addNotes").getElementsByTagName("li");
    for (let i = 0; i < lis.length; i++) {
      branchNotes.push({
        id: lis[i].querySelector(".id").value,
        title: lis[i].querySelector(".title").value,
        content: lis[i].querySelector("textarea").value
      });
    }
    this.props.addBranch(branchNotes, this.state.clickedDate);
    this.onCloseModal();
    if (this._dateHasEvents(this.state.clickedDate)) {
      this._showButtons(this.state.clickedDate);
    }
  };
  handleOnSelectAll = (isSelect, rows) => {
    const ids = rows.map(r => r);
    if (isSelect) {
      this.setState(() => ({
        selectedRows: [...this.state.selectedRows, ...ids]
      }));
    } else {
      this.setState(() => ({
        selectedRows: []
      }));
    }
  };

  _showButtons = cd => {
    //show add/arrange, Daylevel buttons to given date
    const dateFormat = cd
      .addDays(2)
      .toISOString()
      .slice(0, 10);
    const dateFormatDayLevel = cd
      .addDays(1)
      .toISOString()
      .slice(0, 10);
    let eleme = document.querySelectorAll('[data-date="' + dateFormat + '"]');
    let elemeDay = document.querySelectorAll(
      '[data-date="' + dateFormatDayLevel + '"]'
    );
    let btn = eleme[0].childNodes;
    let day = elemeDay[0].childNodes;
    btn[0].classList.remove("hideButton");
    day[1].classList.remove("hideButton");
    // console.log(day);
  };
  onCloseModal = () => {
    this.setState({ open: false });
  };
  _getDay(cd) {
    // console.log(cd);
    const startDate = new Date(this.props.date + " 00:00"); //start date
    const diffTime = Math.abs(startDate - cd);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays + 1;
  }
  _dateHasEvents(yd) {
    // let datesHere = this.props.calendarEvents.filter(d => {
    //   // console.log(d.start + "--" + yd);
    //   return (
    //     d.start.valueOf() === yd.subDays(1).valueOf() &&
    //     !d.title.includes("DAY")
    //   );
    // });
    // console.log(this.props.chosenBranches);
    if (this.state.selectedRows.length > 0) return true;
    else return false;
  }
  _isContains(json, value) {
    let contains = false;
    Object.keys(json).some(key => {
      contains =
        typeof json[key] === "object"
          ? this._isContains(json[key], value)
          : json[key] === value;
      return contains;
    });
    return contains;
  }
}
Date.prototype.subDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() - days);
  return date;
};
Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};
export default CalendarEvent;
