import React, { Component } from "react";
import { Link } from "react-router-dom";
import DataTable from "react-data-table-component";
import axios from "axios";
import Modal from "react-responsive-modal";

class List extends Component {
  componentDidMount() {
    axios.get("https://reqres.in/api/users?page=1").then(response => {
      this.setState({ announcementPromise: response.data });
    });
  }
  state = {
    open: false,
    announcementPromise: [],
    isGeofencing: false,
    geofencingRange: 1,
    isAllowCheckInOutsideFence: false,
    isAllowCheckOutOutsideFence: false
  };
  datatableHandler = key => {
    // this.props.history.push("/newitinerary/" + key);
    // this.props.history.push({
    //   pathname: "/newitinerary",
    //   search: "?isEdit=abc",
    //   state: { detail: key }
    // });
    this.props.history.push("/newitinerary", { response: key });
  };
  render() {
    const columns = [
      {
        name: "Id",
        selector: "id",
        sortable: true
      },
      {
        name: "First Name",
        selector: "first_name",
        sortable: true
      },
      {
        name: "Last_name",
        selector: "last_name",
        sortable: true
      },
      {
        name: "Email",
        selector: "email",
        sortable: true
      }
    ];
    const announcementData = this.state.announcementPromise.data;
    const { open } = this.state;
    return (
      <div className="row">
        <div className="page-header col-xs-12">
          <div className="pull-left">
            <h1>Itineraries</h1>
          </div>
          <div className="pull-right">
            <div className="btn-group">
              <button
                id="btn_settings"
                className="btn btn-white btn-sm btn-info btn-round"
                onClick={this.settingsHandler}
              >
                {" "}
                <i className="ace-icon fa fa-cog "></i> Settings
              </button>
            </div>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="widget-box transparent" id="recent-box">
            <div className="widget-body">
              <div className="widget-main padding-4">
                <div className="tab-content padding-8">
                  <div id="task-tab" className="tab-pane active">
                    <div className="row">
                      <div id="div_filter" className="col-md-12"></div>
                      <div className="col-xs-12 col-md-12 col-sm-12 align-right">
                        <div className="btn-group">
                          <Link
                            to="/newitinerary"
                            className="btn btn-white btn-sm btn-info btn-round"
                          >
                            <span className="ace-icon fa fa-plus "></span>
                            {"   "}
                            Add New Itinerary
                          </Link>
                        </div>

                        {"   "}
                        <Link
                          to="/ExportItinerary"
                          className="btn btn-white btn-sm btn-info btn-round"
                        >
                          <span className="glyphicon glyphicon-download"></span>
                          {"   "}
                          Export
                        </Link>
                      </div>

                      <div className="space-20"></div>
                      <div className="col-xs-12 content">
                        <div className="space-6"></div>
                        <div className="table-responsive">
                          <DataTable
                            columns={columns}
                            data={announcementData}
                            onRowClicked={this.datatableHandler}
                            pointerOnHover
                            striped
                            highlightOnHover
                            pagination
                          />
                        </div>
                      </div>
                      <Modal
                        open={open}
                        onClose={this.onCloseModal}
                        center
                        className="modal-content"
                      >
                        <form onSubmit={this.submitForm}>
                          <div className="modal-header">
                            <h4 className="modal-title" id="modalSettingsTitle">
                              Itinerary Settings
                            </h4>
                          </div>
                          <div className="modal-body">
                            <div className="control-group" id="settings_div">
                              <div className="controls form-group">
                                <div className="checkbox">
                                  <label>
                                    <input
                                      id="isGeofencing"
                                      name="isGeofencing"
                                      className="ace ace-checkbox-2"
                                      type="checkbox"
                                      checked={this.state.isGeofencing}
                                      onChange={this.handleIsGeo}
                                    />
                                    <span className="lbl">
                                      {"  "} Allow Geofencing{" "}
                                    </span>
                                  </label>
                                </div>
                                <div
                                  className="checkbox disabled"
                                  id="divRange"
                                >
                                  <label>Range: </label>
                                </div>
                                <div className="ace-spinner middle touch-spinner">
                                  <div className="input-group">
                                    <input
                                      type="text"
                                      id="geofencingRange"
                                      className="spinbox-input form-control text-center"
                                      value={this.state.geofencingRange}
                                      onChange={event =>
                                        this.setState({
                                          geofencingRange: event.target.value.replace(
                                            /\D/,
                                            ""
                                          )
                                        })
                                      }
                                    />
                                    <div className="spinbox-buttons input-group-btn">
                                      <button
                                        type="button"
                                        className="btn spinbox-up btn-sm btn-success"
                                        onClick={() => this.addHandler(10)}
                                      >
                                        <i className="ace-icon ace-icon fa fa-plus"></i>
                                      </button>
                                      <button
                                        type="button"
                                        className="btn spinbox-down btn-sm btn-danger"
                                        onClick={() => this.subHandler(10)}
                                      >
                                        <i className="ace-icon ace-icon fa fa-minus"></i>
                                      </button>
                                    </div>
                                  </div>
                                  <small>in meters (m).</small>
                                  <div className="checkbox">
                                    <label>
                                      <input
                                        id="isAllowCheckInOutsideFence"
                                        name="isAllowCheckInOutsideFence"
                                        className="ace ace-checkbox-2"
                                        type="checkbox"
                                        checked={
                                          this.state.isAllowCheckInOutsideFence
                                        }
                                        onChange={this.handleisAllowCheckInOut}
                                      />
                                      <span className="lbl">
                                        {" "}
                                        Allow check in outside fence.{" "}
                                      </span>
                                    </label>
                                  </div>
                                  <div className="checkbox">
                                    <label>
                                      <input
                                        id="isAllowCheckOutOutsideFence"
                                        name="isAllowCheckOutOutsideFence"
                                        className="ace ace-checkbox-2"
                                        type="checkbox"
                                        checked={
                                          this.state.isAllowCheckOutOutsideFence
                                        }
                                        onChange={this.handleisAllowCheckOut}
                                      />
                                      <span className="lbl">
                                        {" "}
                                        Allow check out outside fence.{" "}
                                      </span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="alert alert-warning" role="alert">
                              <span className="fa fa-warning"></span> Notes:
                              <p>
                                1. Geofencing consumes more power and may drain
                                mobile device batteries faster.
                              </p>
                              <p>
                                2. Locations may be inaccurate at times
                                depending on the area, surrounding structures,
                                and signal interference.
                              </p>
                              <p>3. Range is how big is the geo-fence.</p>
                            </div>
                          </div>
                          <div className="modal-footer">
                            <button
                              type="button"
                              className="btn btn-secondary"
                              onClick={this.onCloseModal}
                            >
                              Close
                            </button>
                            <button type="submit" className="btn btn-primary">
                              Save changes
                            </button>
                          </div>
                        </form>
                      </Modal>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  addHandler = val => {
    let a = parseInt(this.state.geofencingRange) + val;
    this.setState({ geofencingRange: a });
  };
  subHandler = val => {
    let a = this.state.geofencingRange - val;
    if (!(a < 1)) this.setState({ geofencingRange: a });
  };
  handleIsGeo = event => this.setState({ isGeofencing: event.target.checked });
  handleisAllowCheckInOut = event =>
    this.setState({ isAllowCheckInOutsideFence: event.target.checked });
  handleisAllowCheckOut = event =>
    this.setState({ isAllowCheckOutOutsideFence: event.target.checked });
  submitForm = e => {
    e.preventDefault();
    this.onCloseModal();
  };
  settingsHandler = () => {
    this.onOpenModal();
  };
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
}
export default List;
