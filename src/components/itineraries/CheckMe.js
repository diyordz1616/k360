import React, { Component } from "react";
import CheckBox from "./CheckBox";

class CheckMe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fruits: [
        { id: 1, value: "MON", isChecked: false },
        { id: 2, value: "TUE", isChecked: false },
        { id: 3, value: "WED", isChecked: false },
        { id: 4, value: "THU", isChecked: false }
      ]
    };
  }

  handleAllChecked = event => {
    let fruits = this.state.fruits;
    fruits.forEach(fruit => (fruit.isChecked = event.target.checked));
    this.setState({ fruits: fruits });
    console.log(fruits);
  };

  handleCheckChieldElement = event => {
    let fruits = this.state.fruits;
    fruits.forEach(fruit => {
      if (fruit.value === event.target.value)
        fruit.isChecked = event.target.checked;
    });
    this.setState({ fruits: fruits });
    console.log(fruits);
  };

  render() {
    return (
      <div className="App">
        <h1> Check and Uncheck All Example </h1>
        <input
          type="checkbox"
          onClick={this.handleAllChecked}
          value="checkedall"
        />{" "}
        Check / Uncheck All
        <ul>
          {this.state.fruits.map(fruit => {
            return (
              <span
                id="span_working_days"
                data-toggle="buttons"
                class="btn-group no-margin"
              >
                <CheckBox
                  handleCheckChieldElement={this.handleCheckChieldElement}
                  {...fruit}
                />
              </span>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default CheckMe;
