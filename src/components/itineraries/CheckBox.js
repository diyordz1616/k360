import React from "react";

export const CheckBox = props => {
  return (
    <label
      key={props.id}
      className={
        "col-sm-offset-1 btn btn-sm btn-yellow " +
        (props.isChecked ? "active" : " ")
      }
    >
      {props.value}
      <input
        key={props.id}
        onClick={props.handleCheckChieldElement}
        type="checkbox"
        checked={props.isChecked}
        value={props.value}
        readOnly
      />
    </label>
  );
};
export default CheckBox;
