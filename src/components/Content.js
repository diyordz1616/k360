import React, { useState } from "react";
import About from "./About";
import Dashboard from "./Dashboard";
import Reports from "./Reports";
import ReportDetails from "./ReportDetails";
import Announcements from "./announcements/Announcements";
import NewAnnouncement from "./announcements/NewAnnouncement";
import NewItinerary from "./itineraries/NewItinerary";
import CheckMe from "./itineraries/CheckMe";
import List from "./itineraries/List";
import FormList from "./forms/FormList";
import Api from "./settings/Api";
import Branches from "./branches/list";
import AccountsList from "./accounts/list";
import UomList from "./uom/list";
import GroupsList from "./groups/list";
import CategoriesList from "./categories/list";
import BrandsList from "./brands/list";
import RolesList from "./roles/list";
import NewUom from "./uom/NewUom";
import NewGroups from "./groups/NewGroups";
import NewRoles from "./roles/NewRoles";
import NewCategories from "./categories/NewCategories";
import NewBrands from "./brands/NewBrands";
import PricezoneList from "./pricezone/list";
import NewProduct from "./product/NewProduct";
import ProductSettings from "./product/Settings";
import ProductList from "./product/list";
import NewAccount from "./accounts/NewAccount";
import NewBranch from "./branches/New";
import BranchSettings from "./branches/Settings";
// import NewForm from "./forms/NewForm";
import FormSorter from "./forms/FormSorter";
import FormSort from "./forms/FormSort";
import Choices from "./forms/Choices";
import Header from "./Header";
import Sidebar from "./Sidebar";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import MultipleBranches from "./branches/Multiple";
import MultipleProducts from "./product/Multiple";

function Content() {
  const [page, setPage] = useState("Dashboard");
  const routes = [
    {
      path: "/dashboard",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-icon"></i>
            Home
          </li>
          <li className="active">Dashboard</li>
        </ul>
      ),
      exact: true
    },
    {
      path: "/reports",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-icon"></i>
            Home
          </li>
          <li className="active">Reports</li>
        </ul>
      )
    },
    {
      path: "/reportdetails",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-icon"></i>
            Home
          </li>
          <li className="active">
            <Link to="/reports">Reports</Link>
          </li>
          <li>Report Details</li>
        </ul>
      )
    },
    {
      path: "/about",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-icon"></i>
            Home
          </li>
          <li className="active">About</li>
        </ul>
      )
    },
    {
      path: "/announcements",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li className="active">Announcements</li>
        </ul>
      )
    },
    {
      path: "/newannouncement",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/announcements">Announcements</Link>
          </li>
          <li className="active">New Announcements</li>
        </ul>
      )
    },
    {
      path: "/newaccount",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/accountslist">Accounts</Link>
          </li>
          <li className="active">New Account</li>
        </ul>
      )
    },
    {
      path: "/itineraries",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li className="active">Itineraries</li>
        </ul>
      )
    },
    {
      path: "/newitinerary",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li className="">
            <Link to="/itineraries">Itineraries</Link>
          </li>
          <li className="active">New</li>
        </ul>
      )
    },
    {
      path: "/checkme",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li className="">
            <Link to="/itineraries">Itineraries</Link>
          </li>
          <li className="active">New</li>
        </ul>
      )
    },
    {
      path: "/forms",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li className="active">Form</li>
        </ul>
      )
    },
    {
      path: "/Api",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li className="active">Settings</li>
          <li className="active">API Keys</li>
        </ul>
      )
    },
    {
      path: "/accountslist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/accountslist">Accounts</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/brandslist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/brandslist">Brands</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/uomlist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/uomlist">UOM</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/groupslist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/groupslist">Groups</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/categorieslist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/categorieslist">Categories</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/roleslist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/roleslist">Roles</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/newuom",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/uomlist">Uom</Link>
          </li>
          <li className="active">New UOM</li>
        </ul>
      )
    },
    {
      path: "/newgroups",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/groupslist">Groups</Link>
          </li>
          <li className="active">New Group</li>
        </ul>
      )
    },
    {
      path: "/newroles",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/roleslist">Roles</Link>
          </li>
          <li className="active">New Roles</li>
        </ul>
      )
    },
    {
      path: "/newbrands",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/brandslist">Brands</Link>
          </li>
          <li className="active">New Brand</li>
        </ul>
      )
    },
    {
      path: "/newcategories",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            {" "}
            <Link to="/categorieslist">Categories</Link>
          </li>
          <li className="active">New Categories</li>
        </ul>
      )
    },
    {
      path: "/formsort",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/forms">Forms</Link>
          </li>
          <li className="active">New Form Sort</li>
        </ul>
      )
    },
    {
      path: "/pricezonelist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/pricezonelist">Price Zone</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/productlist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/productlist">Product</Link>
          </li>
          <li className="active">View</li>
        </ul>
      )
    },
    {
      path: "/brancheslist",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/brancheslist">Branches</Link>
          </li>
          <li className="active">List</li>
        </ul>
      )
    },
    {
      path: "/newBranch",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/brancheslist">Branches</Link>
          </li>
          <li className="active">New</li>
        </ul>
      )
    },
    {
      path: "/newproduct",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/productlist">Products</Link>
          </li>
          <li className="active">New</li>
        </ul>
      )
    },
    {
      path: "/productsettings",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/productlist">Product</Link>
          </li>
          <li className="active">Settings</li>
        </ul>
      )
    },
    {
      path: "/branchsettings",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/brancheslist">Branches</Link>
          </li>
          <li className="active">Settings</li>
        </ul>
      )
    },
    {
      path: "/multiplebranches",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/brancheslist">Branches</Link>
          </li>
          <li className="active">Multiple</li>
        </ul>
      )
    },
    {
      path: "/multipleproducts",
      sidebar: () => (
        <ul className="breadcrumb">
          <li>
            <i className="ace-icon fa fa-home home-bullhorn"> </i> Home
          </li>
          <li>
            <Link to="/productlist">Products</Link>
          </li>
          <li className="active">Multiple</li>
        </ul>
      )
    }
  ];
  return (
    <Router>
      <div className="App">
        <Header />
        <div className="main-container ace-save-state" id="main-container">
          <Sidebar />
          <div className="main-content">
            <div className="main-content-inner">
              <div className="breadcrumbs ace-save-state" id="breadcrumbs">
                <Switch>
                  {routes.map((route, index) => (
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      component={route.sidebar}
                    />
                  ))}
                </Switch>
              </div>
              <div className="page-content">
                <Switch>
                  <Route path="/" exact component={Dashboard} />
                  <Route path="/about" component={About} />
                  <Route path="/dashboard" component={Dashboard} />
                  <Route path="/announcements" component={Announcements} />
                  <Route path="/newannouncement" component={NewAnnouncement} />
                  <Route path="/reports" page={page} component={Reports} />
                  <Route path="/itineraries" page={page} component={List} />
                  <Route path="/forms" page={page} component={FormList} />
                  <Route path="/Api" page={page} component={Api} />
                  <Route path="/brancheslist" component={Branches} />
                  <Route path="/accountslist" component={AccountsList} />
                  <Route path="/uomlist" component={UomList} />
                  <Route path="/groupslist" component={GroupsList} />
                  <Route path="/brandslist" component={BrandsList} />
                  <Route path="/categorieslist" component={CategoriesList} />
                  <Route path="/roleslist" component={RolesList} />
                  <Route path="/newuom" component={NewUom} />
                  <Route path="/newbrands" component={NewBrands} />
                  <Route path="/newgroups" component={NewGroups} />
                  <Route path="/newroles" component={NewRoles} />
                  <Route path="/newcategories" component={NewCategories} />
                  <Route path="/pricezonelist" component={PricezoneList} />
                  <Route path="/productlist" component={ProductList} />
                  <Route path="/productsettings" component={ProductSettings} />
                  <Route path="/newproduct" component={NewProduct} />
                  <Route path="/newaccount" component={NewAccount} />
                  <Route path="/newBranch" component={NewBranch} />
                  <Route path="/branchsettings" component={BranchSettings} />
                  <Route
                    path="/multiplebranches"
                    component={MultipleBranches}
                  />
                  <Route
                    path="/multipleproducts"
                    component={MultipleProducts}
                  />
                  <Route path="/choices" page={page} component={Choices} />
                  <Route
                    path="/formsorter"
                    page={page}
                    component={FormSorter}
                  />
                  <Route path="/formsort" page={page} component={FormSort} />
                  <Route path="/checkme" page={page} component={CheckMe} />
                  <Route
                    path="/newitinerary"
                    page={page}
                    component={NewItinerary}
                  />

                  <Route
                    path="/reportdetails"
                    page={page}
                    component={ReportDetails}
                  />
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Router>
  );
}
// const Home = () => <div>Home Page.</div>;
export default Content;
