import React from "react";
import "bootstrap/dist/css/bootstrap-custom.css";
import "./App.css";

import Content from "./components/Content";

function App() {
  return (
    <div className="App">
      {/* <div>sidebar here</div> */}
      <Content />
    </div>
  );
}

export default App;
